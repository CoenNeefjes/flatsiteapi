# Flatsite API

This application serves as the flatsite API for Residentie Dirty-A. 

## Requirements

**Development**
* Java 8
* PostgreSQL
* Any IDEA (IntelliJ is preferred)
* Git

**Deployment with Docker**
* Git (Optional: For pulling the repo)
* Java (Optional: For building the jar)
* Docker
* Docker-Compose

## Development Setup
**Application Local**

Spring boot makes use of .yml files for loading application properties.
The standard properties are defined in the application-default.yml file.
To override these properties so they match your needs for running locally,
you can create an application-local.yml file in the same directory.
Any property you define here will override the same property in the application-default file.
This will probably be necessary for at least the datasource url property.

Example application-local.yml:
```
spring:
  datasource:
    url: jdbc:postgresql://localhost:5432/dirtya?currentSchema=public
    username: postgres
    password: admin

app:
  directories:
    images: C:/Users/Coen Neefjes/Documents/test/
```

##Deployment
### With Docker
* Clone the repository or copy the files from another source
* Make sure a jar file is present in the target folder. Otherwise build the jar file: `./mvnw package -DskipTests`
* Build the docker image: `sudo docker-compose build`
* Run the docker images: `sudo docker-compose up`

The application will now be available on `localhost:8080/api`

### Without Docker
This is for Linux. Install `postgresql`.
#### Database

    $ sudo -u postgres psql
    postgres=# create role dirtya with createdb login password 'password';

    $ # edit pg_hba.conf, usually in /var/lib/pgsql/data/
      # Add `local all dirtya scram-sha-256`
      # and `host all dirtya 127.0.0.1/32 scram-sha-256
      # so that you can login with password

    $ sudo -u postgres createdb dirtya -O dirtya -U dirtya -W

#### Java
Install `java-17`. In IntelliJ add this SDK in `Project Structure` > `SDK`.

##Testing
**Using maven**

Tests can be run locally using the provided maven wrapper: `./mvnw test` (This requires Java 8 installed on the device)

**Gitlab-ci**

This project also contains a gitlab-ci file. This file makes sure all tests run automatically after each push to the repository.
When the tests or the build fail, the branch cannot be merged into the master branch.

##Documentation
**Swagger**

Swagger documentation is available locally using: `http://localhost:8080/api/swagger-ui/index.html?configUrl=/api/v3/api-docs/swagger-config`

This documentation is also available once deployed, just change `localhost:8080` to the deployment host address.
