package nl.dirtya.flatsiteapi

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import javax.transaction.Transactional

@ActiveProfiles("default","test")
@SpringBootTest
@Transactional
class BaseTest {

	@Test
	fun contextLoads() {
	}

}
