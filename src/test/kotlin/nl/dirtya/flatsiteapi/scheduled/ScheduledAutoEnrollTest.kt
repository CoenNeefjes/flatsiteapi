package nl.dirtya.flatsiteapi.scheduled

import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.user.api.AutoEnroll
import nl.dirtya.flatsiteapi.model.user.api.User
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import java.time.LocalDate
import javax.persistence.EntityManager

class ScheduledAutoEnrollTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val scheduledAutoEnroll: ScheduledAutoEnroll,
        private val cookingEventService: CookingEventService,
): BaseTest() {

    private lateinit var user1: User
    private lateinit var user2: User

    @BeforeEach
    fun beforeEach() {
        val user1 = User("user1", "0000")
        user1.autoEnroll = AutoEnroll(monday = true, tuesday = true, wednesday = true, thursday = true, friday = true, saturday = true, sunday = true)

        val user2 = User("user2", "0000")
        user2.autoEnroll = AutoEnroll(monday = false, tuesday = false, wednesday = false, thursday = false, friday = false, saturday = false, sunday = false)

        entityManager.persist(user1)
        entityManager.persist(user2)

        this.user1 = user1
        this.user2 = user2
    }

    @Test
    fun whenAutoEnrollingCorrectBehaviour() {
        val now = LocalDate.now()
        scheduledAutoEnroll.autoEnroll()

        entityManager.flush()
        entityManager.clear()

        val cookingEvent = cookingEventService.findByDateOrCreate(now)

        assertEquals(2, cookingEvent.enrollments.size)
        assertEquals(1, cookingEvent.getTotalEnrollments())
    }

    @Test
    fun whenAutoEnrollingAlreadyEnrolledCorrectBehaviour() {
        val now = LocalDate.now()

        val cookingEvent = cookingEventService.findByDateOrCreate(now)
        cookingEventService.enroll(cookingEvent, 2, user1)
        cookingEventService.enroll(cookingEvent, 2, user2)

        entityManager.flush()
        entityManager.clear()

        scheduledAutoEnroll.autoEnroll()

        val updated = cookingEventService.findByDateOrCreate(now)
        assertEquals(2, updated.enrollments.size)
        assertEquals(4, updated.getTotalEnrollments())
    }

}