package nl.dirtya.flatsiteapi

import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.authentication.AuthenticationService
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

class IntegrationTest: BaseTest() {

    @Autowired lateinit var webApplicationContext: WebApplicationContext
    @Autowired lateinit var authenticationService: AuthenticationService
    @Autowired lateinit var userService: UserService

    lateinit var mockMvc: MockMvc

    lateinit var admin: User
    lateinit var adminAuthHeader: String

    lateinit var user: User
    lateinit var userAuthHeader: String

    @BeforeEach
    fun baseIntegrationSetup() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build()

        admin = userService.findByUsername("Admin")
        adminAuthHeader = authenticationService.generateJwt(admin.username, "password")

        val userWriteDto = UserFactory().createValid1()
        user = userService.create(userWriteDto)
        userAuthHeader = authenticationService.generateJwt(user.username, userWriteDto.password!!)
    }

}