package nl.dirtya.flatsiteapi.model.image

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.album.AlbumFactory
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.image.service.ImageFileService
import nl.dirtya.flatsiteapi.model.image.service.ImageLocationResolver
import nl.dirtya.flatsiteapi.model.image.service.ImageUploadService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import java.io.File
import javax.persistence.EntityManager

class ImageUploadServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val imageUploadService: ImageUploadService,
        private val albumService: AlbumService,
        private val userService: UserService,
        private val imageFileService: ImageFileService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService
    @MockBean lateinit var imageLocationResolver: ImageLocationResolver
    @TempDir lateinit var tempFolder: File

    lateinit var user: User

    @BeforeEach
    fun setup() {
        whenever(imageLocationResolver.resolve()).thenReturn(tempFolder.absolutePath)

        user = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    private fun createMultipart(): MockMultipartFile {
        assertTrue(tempFolder.isDirectory)
        val resource = ClassPathResource("files/landscape2.jpg")
        assertTrue(resource.exists())

        val resourceBytes = resource.file.readBytes()

        return MockMultipartFile("file", "image_with_name.jpg", MediaType.IMAGE_JPEG_VALUE, resourceBytes)
    }

    @Test
    fun whenUploadingCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())
        val amount = album.images.size
        val multipart = createMultipart()

        val image = imageUploadService.upload(multipart, album.id)

        val foundAlbum = albumService.findById(album.id)
        assertEquals(amount+1, foundAlbum.images.size)

        entityManager.flush()
        entityManager.clear()

        // Make sure we can find the file again
        val foundImage = imageFileService.findById(image.id)
        assertNotEquals(0, foundImage.size)
    }

    @Test
    fun whenUploadingAlbumNotFoundThrowsException() {
        assertThrows<NotFoundException> { imageUploadService.upload(createMultipart(), -1) }
    }

    @Test
    fun whenUploadingAlbumNotEditableThrowsException() {
        val album = albumService.findByName("bier inkoop bonnetjes")
        assertThrows<ForbiddenException> { imageUploadService.upload(createMultipart(), album.id) }
    }

}