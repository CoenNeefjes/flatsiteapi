package nl.dirtya.flatsiteapi.model.image

import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.config.AppProperties
import nl.dirtya.flatsiteapi.model.image.service.ImageLocationResolver
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

class ImageLocationResolverTest
@Autowired constructor(
        private val imageLocationResolver: ImageLocationResolver,
        private val appProperties: AppProperties
): BaseTest() {

    @Test
    fun whenResolvingCorrectBehaviour() {
        assertEquals(appProperties.directories.images, imageLocationResolver.resolve())
    }

}