package nl.dirtya.flatsiteapi.model.image

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.album.AlbumFactory
import nl.dirtya.flatsiteapi.model.album.api.Album
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.image.service.ImageService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class ImageServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val imageService: ImageService,
        private val userService: UserService,
        private val albumService: AlbumService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User
    private lateinit var user2: User
    private lateinit var album: Album

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        album = albumService.create(AlbumFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val amount = imageService.findAll().size
        imageService.create(album)

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = imageService.findAll().size
        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val image = imageService.create(album)

        entityManager.flush()
        entityManager.clear()

        val foundImage = imageService.findById(image.id)
        assertEquals(image, foundImage)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { imageService.findById(-1) }
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val image = imageService.create(album)

        assertEquals(user, image.createdBy)
        assertEquals(LocalDate.now(), image.createdOn)
        assertEquals(album, image.album)
        assertNull(image.beerInput)
        assertNull(image.user)
    }

    @Test
    fun whenCreatingImageIsAddedToAlbum() {
        val image = imageService.create(album)

        entityManager.flush()
        entityManager.clear()

        val foundAlbum = albumService.findById(album.id)
        assertTrue(foundAlbum.images.contains(image))
    }

    @Test
    fun whenDeletingCorrectBehaviour() {
        val image = imageService.create(album)

        assertTrue(imageService.findAll().contains(image))

        imageService.delete(image)

        assertFalse(imageService.findAll().contains(image))
    }

    @Test
    fun whenDeletingImageOfAnotherUserThrowsException() {
        val image = imageService.create(album)

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)

        assertThrows<ForbiddenException> { imageService.delete(image) }
    }

    @Test
    fun whenDeletingImageDangerouslyNoIdentityCheckIsPerformed() {
        val amount = imageService.findAll().size

        val image = imageService.create(album)

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(-1L)

        imageService.deleteDangerously(image)

        assertEquals(amount, imageService.findAll().size)
    }

}