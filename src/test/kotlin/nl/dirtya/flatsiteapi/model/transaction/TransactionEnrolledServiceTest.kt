package nl.dirtya.flatsiteapi.model.transaction

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionEnrolledService
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.BigDecimal
import javax.persistence.EntityManager

class TransactionEnrolledServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val transactionEnrolledService: TransactionEnrolledService,
        private val transactionService: TransactionService,
        private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val transaction = transactionService.create(TransactionFactory().createValid2(user1))
        val enrollment = transactionEnrolledService.createAll(listOf(TransactionEnrolledWriteDto(user2.id, 1)), transaction)[0]

        entityManager.flush()
        entityManager.clear()

        val foundEnrollment = transactionEnrolledService.findById(enrollment.id)

        assertEquals(enrollment, foundEnrollment)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { transactionEnrolledService.findById(-1) }
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val transaction = transactionService.create(TransactionFactory().createValid2(user1))
        transaction.enrollments.clear()

        val amountEnrolled = 1
        val writeDto1 = TransactionEnrolledWriteDto(user1.id, amountEnrolled)
        val writeDto2 = TransactionEnrolledWriteDto(user2.id, amountEnrolled)
        val enrollment = transactionEnrolledService.createAll(listOf(writeDto1, writeDto2), transaction)[0]

        assertEquals(user2, enrollment.user)
        assertEquals(amountEnrolled, enrollment.amount)
        assertEquals(transaction, enrollment.transaction)
    }

    @Test
    fun whenCalculatingAmountSpendCorrectBehaviour() {
        val initBalance = transactionEnrolledService.countAmountSpend(user1.id)
        assertEquals(BigDecimal.ZERO, initBalance)

        val writeDto = TransactionFactory().createValid2(user1)
        transactionService.create(writeDto)

        val newBalance = transactionEnrolledService.countAmountSpend(user1.id)

        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).times(BigDecimal.valueOf(-1)).compareTo(newBalance) == 0)
    }

}