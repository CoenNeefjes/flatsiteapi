package nl.dirtya.flatsiteapi.model.transaction

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionBalanceService
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.BigDecimal

class TransactionBalanceServiceTest
@Autowired constructor(
        private val userService: UserService,
        private val transactionBalanceService: TransactionBalanceService,
        private val transactionService: TransactionService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenCalculatingBalanceForUserCorrectBehaviour() {
        val initBalanceUser1 = transactionBalanceService.calculateBalanceForUser(user1.id)
        val initBalanceUser2 = transactionBalanceService.calculateBalanceForUser(user2.id)
        assertEquals(BigDecimal.ZERO, initBalanceUser1)
        assertEquals(BigDecimal.ZERO, initBalanceUser2)

        val writeDto = TransactionFactory().createValid1(user2)
        transactionService.create(writeDto)

        val newBalanceUser1 = transactionBalanceService.calculateBalanceForUser(user1.id)
        val newBalanceUser2 = transactionBalanceService.calculateBalanceForUser(user2.id)
        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).compareTo(newBalanceUser1) == 0)
        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).times(BigDecimal.valueOf(-1)).compareTo(newBalanceUser2) == 0)
    }

    @Test
    fun whenCalculatingBalanceForMeCorrectBehaviour() {
        val initBalance = transactionBalanceService.calculateBalanceForMe()
        assertEquals(BigDecimal.ZERO, initBalance)

        val writeDto = TransactionFactory().createValid1(user2)
        transactionService.create(writeDto)

        val newBalance = transactionBalanceService.calculateBalanceForMe()
        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).compareTo(newBalance) == 0)
    }

    @Test
    fun whenCalculatingBalanceForUsersCorrectBehaviour() {
        val initBalances = transactionBalanceService.calculateBalanceForUsers(false)
        for (x in initBalances) {
            assertEquals(BigDecimal.ZERO, x.value)
        }

        val writeDto = TransactionFactory().createValid1(user2)
        transactionService.create(writeDto)

        val newBalances = transactionBalanceService.calculateBalanceForUsers(false)
        val newBalanceUser1 = newBalances[user1]
        val newBalanceUser2 = newBalances[user2]
        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).compareTo(newBalanceUser1) == 0)
        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).times(BigDecimal.valueOf(-1)).compareTo(newBalanceUser2) == 0)
    }
}