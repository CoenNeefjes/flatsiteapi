package nl.dirtya.flatsiteapi.model.transaction

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionEnrolledService
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Pageable
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.EntityManager

class TransactionServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val transactionService: TransactionService,
        private val transactionEnrolledService: TransactionEnrolledService,
        private val userService: UserService,
        private val cookingEventService: CookingEventService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User
    private lateinit var user3: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        user3 = userService.create(UserFactory().createValid3())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val foundAmount = transactionService.findAllPaged(Pageable.unpaged()).totalElements

        transactionService.create(TransactionFactory().createValid1(user1))

        assertEquals(foundAmount+1, transactionService.findAllPaged(Pageable.unpaged()).totalElements)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1))

        entityManager.flush()
        entityManager.clear()

        val foundTransaction = transactionService.findById(transaction.id)

        assertEquals(transaction, foundTransaction)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { transactionService.findById(-1) }
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val writeDto = TransactionFactory().createValid1(user1, user2)
        val transaction = transactionService.create(writeDto)

        assertEquals(LocalDate.now(), transaction.date)
        assertEquals(writeDto.description, transaction.description)
        assertEquals(writeDto.price, transaction.price)
        assertEquals(writeDto.enrolled!!.size, transaction.enrollments.size)
    }

    @Test
    fun whenCreatingNotSelfEnrolledCorrectBehaviour() {
        val writeDto = TransactionFactory().createValid1(BigDecimal.valueOf(13.76), user2, user3)
        val transaction = transactionService.create(writeDto)

        assertEquals(LocalDate.now(), transaction.date)
        assertEquals(writeDto.description, transaction.description)
        assertEquals(writeDto.price, transaction.price)
        assertEquals(writeDto.enrolled!!.size, transaction.enrollments.size)
    }

    @Test
    fun whenCreatingIndivisibleAmountCorrectBehaviour() {
        val writeDto = TransactionFactory().createValid1(BigDecimal.TEN, user1, user2, user3)
        val transaction = transactionService.create(writeDto)

        assertEquals(LocalDate.now(), transaction.date)
        assertEquals(writeDto.description, transaction.description)
        assertEquals(writeDto.price, transaction.price)
        assertEquals(writeDto.enrolled!!.size, transaction.enrollments.size)
    }

    @Test
    fun whenCreatingDescriptionIsNullThrowsException() {
        assertThrows<NotValidException> { transactionService.create(TransactionFactory().descriptionNull(user1, user2)) }
    }

    @Test
    fun whenCreatingPriceIsNullThrowsException() {
        assertThrows<NotValidException> { transactionService.create(TransactionFactory().priceNull(user1, user2)) }
    }

    @Test
    fun whenCreatingEnrollmentsIsNullThrowsException() {
        assertThrows<NotValidException> { transactionService.create(TransactionFactory().enrollmentsNull()) }
    }

    @Test
    fun whenCreatingEnrollmentsIsEmptyThrowsException() {
        assertThrows<NotValidException> { transactionService.create(TransactionFactory().enrollmentsEmpty()) }
    }

    @Test
    fun whenCreatingEnrollmentsContainsNullThrowsException() {
        assertThrows<NotValidException> { transactionService.create(TransactionFactory().enrollmentsContainsNull(user1, user2)) }
    }

    @Test
    fun whenCreatingForCookingEventDateIsSameAsCookingEvent() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now().minusDays(4))
        cookingEventService.enrollAsCook(cookingEvent.id)

        val transaction = transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id)

        assertEquals(cookingEvent.date, transaction.date)
    }

    @Test
    fun whenCreatingForCookingEventTransactionIsAddedToCookingEvent() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now().minusDays(4))
        cookingEventService.enrollAsCook(cookingEvent.id)

        val transaction = transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id)

        entityManager.flush()
        entityManager.clear()

        val foundCookingEvent = cookingEventService.findById(cookingEvent.id)
        assertEquals(transaction, foundCookingEvent.transaction)
    }

    @Test
    fun whenCreatingForCookingEventPriceIsNullThrowsException() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now())
        assertThrows<NotValidException> { transactionService.createForCookingEvent(TransactionFactory().cookingEventPriceNull(), cookingEvent.id) }
    }

    @Test
    fun whenCreatingForCookingEventUserIsNotCookThrowsException() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now())
        assertThrows<ForbiddenException> { transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id) }
    }

    @Test
    fun whenCreatingForCookingEventTransactionAlreadyExistsThrowsException() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(cookingEvent.id)

        transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id)
        assertThrows<NotValidException> { transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id) }
    }

    @Test
    fun whenUpdatingCorrectBehaviour() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))

        entityManager.flush()
        entityManager.clear()

        val updateWriteDto = TransactionFactory().updateValid1(user1, user2, user3)
        val updatedTransaction = transactionService.update(transaction.id, updateWriteDto)

        assertEquals(transaction.date, updatedTransaction.date)
        assertEquals(transaction.owner, updatedTransaction.owner)
        assertEquals(updateWriteDto.description, updatedTransaction.description)
        assertEquals(updateWriteDto.price, updatedTransaction.price)
        assertEquals(updateWriteDto.enrolled!!.size, updatedTransaction.enrollments.size)
    }

    @Test
    fun whenUpdatingRemovingEnrollmentThrowsException() {
        val totalEnrollments = transactionEnrolledService.findAll().size
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))
        assertEquals(totalEnrollments+2, transactionEnrolledService.findAll().size)
        assertEquals(2, transaction.enrollments.size)

        entityManager.flush()
        entityManager.clear()

        val updateWriteDto = TransactionFactory().updateValid1(user1)

        // Try to remove enrollment of user2
        assertThrows<NotValidException> { transactionService.update(transaction.id, updateWriteDto) }
    }

    @Test
    fun whenUpdatingCookingEventEnrollmentsAreUpdatedCorrectly() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(cookingEvent.id)

        val transaction = transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id)

        entityManager.flush()
        entityManager.clear()

        val cookingEventAfterTransactionCreation = cookingEventService.findById(cookingEvent.id)
        assertEquals(transaction.enrollments.sumOf { it.amount }, cookingEventAfterTransactionCreation.enrollments.sumOf { it.amount })

        val updatedTransaction = transactionService.update(transaction.id, TransactionFactory().updateValid1(user1, user2))

        entityManager.flush()
        entityManager.clear()

        val cookingEventAfterTransactionUpdate = cookingEventService.findById(cookingEvent.id)
        assertEquals(updatedTransaction.enrollments.sumOf { it.amount }, cookingEventAfterTransactionUpdate.enrollments.sumOf { it.amount })
    }

    @Test
    fun whenUpdatingDescriptionIsNullThrowsException() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))
        assertThrows<NotValidException> { transactionService.update(transaction.id, TransactionFactory().descriptionNull()) }
    }

    @Test
    fun whenUpdatingEnrollmentsIsNullThrowsException() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))
        assertThrows<NotValidException> { transactionService.update(transaction.id, TransactionFactory().enrollmentsNull()) }
    }

    @Test
    fun whenUpdatingEnrollmentsIsEmptyThrowsException() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))
        assertThrows<NotValidException> { transactionService.update(transaction.id, TransactionFactory().enrollmentsEmpty()) }
    }

    @Test
    fun whenUpdatingEnrollmentsContainsNullThrowsException() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))
        assertThrows<NotValidException> { transactionService.update(transaction.id, TransactionFactory().enrollmentsContainsNull()) }
    }

    @Test
    fun whenCountingAmountPaidCorrectBehaviour() {
        val initBalance = transactionService.countAmountPaid(user1.id)
        assertEquals(BigDecimal.ZERO, initBalance)

        val writeDto = TransactionFactory().createValid1(user1, user2)
        transactionService.create(writeDto)
        val newBalance = transactionService.countAmountPaid(user1.id)

        assertTrue(BigDecimal.valueOf(writeDto.price!!.toLong()).compareTo(newBalance) == 0)
    }

    @Test
    fun whenDeletingCorrectBehaviour() {
        val amount = transactionService.findAll().size

        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))

        transactionService.delete(transaction.id)

        entityManager.flush()
        entityManager.clear()

        val newAmount = transactionService.findAll().size

        assertEquals(amount, newAmount)
    }


    @Test
    fun whenDeletingTransactionIsAlsoRemovedFromCookingEvent() {
        val cookingEvent = cookingEventService.findByDateOrCreate(LocalDate.now().minusDays(4))
        cookingEventService.enrollAsCook(cookingEvent.id)

        val transaction = transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), cookingEvent.id)

        transactionService.delete(transaction.id)

        entityManager.flush()
        entityManager.clear()

        val foundCookingEvent = cookingEventService.findById(cookingEvent.id)

        assertNull(foundCookingEvent.transaction)
    }

    @Test
    fun whenDeletingTransactionFromAnotherUserThrowsException() {
        val transaction = transactionService.create(TransactionFactory().createValid1(user1, user2))

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)

        assertThrows<ForbiddenException> { transactionService.delete(transaction.id) }
    }
}