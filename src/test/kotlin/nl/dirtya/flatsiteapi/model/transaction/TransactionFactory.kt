package nl.dirtya.flatsiteapi.model.transaction

import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionForCookingWriteDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionWriteDto
import nl.dirtya.flatsiteapi.model.user.api.User
import java.math.BigDecimal

class TransactionFactory {

    fun createValid1(vararg users: User): TransactionWriteDto {
        return TransactionWriteDto("descr", BigDecimal.ONE, users.map { TransactionEnrolledWriteDto(it.id, 1) })
    }

    fun createValid1(amount: BigDecimal, vararg users: User): TransactionWriteDto {
        return TransactionWriteDto("descr", amount, users.map { TransactionEnrolledWriteDto(it.id, 1) })
    }

    fun createValidForCookingEvent1(price: BigDecimal): TransactionForCookingWriteDto {
        return TransactionForCookingWriteDto(price)
    }

    fun createValid2(vararg users: User): TransactionWriteDto {
        return TransactionWriteDto("descr2", BigDecimal.valueOf(4), users.map { TransactionEnrolledWriteDto(it.id, 1) })
    }

    fun descriptionNull(vararg users: User): TransactionWriteDto {
        return TransactionWriteDto(null, BigDecimal.ONE, users.map { TransactionEnrolledWriteDto(it.id, 1) })
    }

    fun priceNull(vararg users: User): TransactionWriteDto {
        return TransactionWriteDto("descr", null, users.map { TransactionEnrolledWriteDto(it.id, 1) })
    }

    fun enrollmentsNull(): TransactionWriteDto {
        return TransactionWriteDto("descr", BigDecimal.ONE, null)
    }

    fun enrollmentsEmpty(): TransactionWriteDto {
        return TransactionWriteDto("descr", BigDecimal.ONE, emptyList())
    }

    fun enrollmentsContainsNull(vararg users: User): TransactionWriteDto {
        return TransactionWriteDto("descr", BigDecimal.ONE, users.map { null })
    }

    fun cookingEventPriceNull(): TransactionForCookingWriteDto {
        return TransactionForCookingWriteDto(null)
    }

    fun updateValid1(vararg users: User): TransactionWriteDto {
        return TransactionWriteDto("descr2", BigDecimal.TEN, users.map { TransactionEnrolledWriteDto(it.id, 2) })
    }
}