package nl.dirtya.flatsiteapi.model.transaction

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.BigDecimal

class CorrectTransactionHandlingTest
@Autowired constructor(
    private val transactionService: TransactionService,
    private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User
    private lateinit var user3: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        user3 = userService.create(UserFactory().createValid3())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenCreatingIndivisibleAmountCorrectBehaviour() {
        createTransaction(2, BigDecimal.valueOf(3.33))
        createTransaction(3, BigDecimal.valueOf(3.33))
        createTransaction(4, BigDecimal.valueOf(3.33))
        createTransaction(5, BigDecimal.valueOf(3.33))
        createTransaction(6, BigDecimal.valueOf(3.33))

        createTransaction(2, BigDecimal.valueOf(15.15))
        createTransaction(3, BigDecimal.valueOf(15.15))
        createTransaction(4, BigDecimal.valueOf(15.15))

        createTransaction(3, BigDecimal.valueOf(20))
        createTransaction(4, BigDecimal.valueOf(20))
        createTransaction(5, BigDecimal.valueOf(20))
    }

    /**
     * Function that creates transaction with 2 persons enrolled.
     * User1 is always enrolled 1 time
     * User2 is enrolled (amountOfEnrollments-1) times
     * @param amountOfEnrollments
     * @param totalPrice
     */
    fun createTransaction(amountOfEnrollments: Int, totalPrice: BigDecimal) {
        transactionService.create(TransactionWriteDto("test", totalPrice, listOf(
            TransactionEnrolledWriteDto(user1.id, 1),
            TransactionEnrolledWriteDto(user2.id, amountOfEnrollments-1)
        )))
    }

}