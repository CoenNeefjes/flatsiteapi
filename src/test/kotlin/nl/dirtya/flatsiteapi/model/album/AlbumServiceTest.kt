package nl.dirtya.flatsiteapi.model.album

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.image.service.ImageService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class AlbumServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val albumService: AlbumService,
        private val imageService: ImageService,
        private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val amount = albumService.findAll().size

        albumService.create(AlbumFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val newAmount = albumService.findAll().size

        assertEquals(amount+1, newAmount)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundAlbum = albumService.findById(album.id)

        assertEquals(album, foundAlbum)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { albumService.findById(-1) }
    }

    @Test
    fun whenFindingByNameCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundAlbum = albumService.findByName(album.name)

        assertEquals(album, foundAlbum)
    }

    @Test
    fun whenFindingByNameNotFoundThrowsException() {
        assertThrows<NotFoundException> { albumService.findByName("NotFound") }
    }

    @Test
    fun whenFindingByNameOptionalCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundAlbum = albumService.findByNameOptional(album.name)

        assertEquals(album, foundAlbum)
    }

    @Test
    fun whenFindingByNameOptionalNotFoundReturnsNull() {
        val foundAlbum = albumService.findByNameOptional("NotFound")
        assertNull(foundAlbum)
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val writeDto = AlbumFactory().createValid1()
        val album = albumService.create(writeDto)

        assertEquals(LocalDate.now(), album.createdOn)
        assertEquals(LocalDate.now(), album.lastEdited)
        assertEquals(writeDto.name, album.name)
        assertTrue(album.editable)
        assertTrue(album.images.isEmpty())
    }

    @Test
    fun whenCreatingNameIsNullThrowsException() {
        assertThrows<NotValidException> { albumService.create(AlbumFactory().nameNull()) }
    }

    @Test
    fun whenCreatingNameIsEmptyThrowsException() {
        assertThrows<NotValidException> { albumService.create(AlbumFactory().nameEmpty()) }
    }

    @Test
    fun whenCreatingNameIsAlreadyUsedThrowsException() {
        albumService.create(AlbumFactory().createValid1())
        assertThrows<NotValidException> { albumService.create(AlbumFactory().createValid1()) }
    }

    @Test
    fun whenUpdatingCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val updateWriteDto = AlbumFactory().updateValid1()
        val updatedAlbum = albumService.update(album.id, updateWriteDto)

        assertEquals(updateWriteDto.name, updatedAlbum.name)
        assertEquals(LocalDate.now(), updatedAlbum.lastEdited)
    }

    @Test
    fun whenDeletingCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        albumService.delete(album.id)

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotFoundException> { albumService.findById(album.id) }
    }

    // This test expects a non-editable album to be in the database at startup
    @Test
    fun whenDeletingNotEditableThrowsException() {
        assertFalse(albumService.findAll().isEmpty())

        val album = albumService.findAll()[0]

        assertFalse(album.editable)

        assertThrows<NotValidException> { albumService.delete(album.id) }
    }

    @Test
    fun whenDeletingNotEmptyThrowsException() {
        val album = albumService.create(AlbumFactory().createValid1())
        imageService.create(album)

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotValidException> { albumService.delete(album.id) }
    }

}