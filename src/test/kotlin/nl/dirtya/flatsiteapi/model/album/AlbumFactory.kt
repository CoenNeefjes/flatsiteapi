package nl.dirtya.flatsiteapi.model.album

import nl.dirtya.flatsiteapi.model.album.controller.dto.AlbumWriteDto

class AlbumFactory {

    fun createValid1(): AlbumWriteDto {
        return AlbumWriteDto("name1")
    }

    fun createValid2(): AlbumWriteDto {
        return AlbumWriteDto("name2")
    }

    fun nameNull(): AlbumWriteDto {
        return AlbumWriteDto(null)
    }

    fun nameEmpty(): AlbumWriteDto {
        return AlbumWriteDto("")
    }

    fun updateValid1(): AlbumWriteDto {
        return AlbumWriteDto("nameUpdated1")
    }

}