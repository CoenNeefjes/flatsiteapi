package nl.dirtya.flatsiteapi.model.shuttedrunktest

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.shuttledrunktest.service.ShuttleDrunkTestResultService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Pageable
import javax.persistence.EntityManager

class ShuttleDrunkTestResultServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val shuttleDrunkTestResultService: ShuttleDrunkTestResultService,
        private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val shuttleDrunkTestResult = shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user1.id))

        entityManager.flush()
        entityManager.clear()

        val found = shuttleDrunkTestResultService.findById(shuttleDrunkTestResult.id)

        assertEquals(shuttleDrunkTestResult, found)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { shuttleDrunkTestResultService.findById(-1) }
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val total = shuttleDrunkTestResultService.findAll(Pageable.unpaged()).totalElements

        val shuttleDrunkTestResult = shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user1.id))

        entityManager.flush()
        entityManager.clear()

        val found = shuttleDrunkTestResultService.findAll(Pageable.unpaged())

        assertEquals(total+1, found.totalElements)
        assertTrue(found.contains(shuttleDrunkTestResult))
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val user = user1
        val writeDto = ShuttleDrunkTestResultFactory().createValid1(user.id)
        val shuttleDrunkTestResult = shuttleDrunkTestResultService.create(writeDto)

        assertNotNull(shuttleDrunkTestResult.id)
        assertNotNull(shuttleDrunkTestResult.uuid)
        assertEquals(writeDto.date, shuttleDrunkTestResult.date)
        assertEquals(writeDto.score, shuttleDrunkTestResult.score)
        assertEquals(user, shuttleDrunkTestResult.user)
    }

    @Test
    fun whenCreatingDateIsNullThrowsException() {
        assertThrows<NotValidException> { shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().dateNull(user1.id)) }
    }

    @Test
    fun whenCreatingScoreIsNullThrowsException() {
        assertThrows<NotValidException> { shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().scoreNull(user1.id)) }
    }

    @Test
    fun whenCreatingScoreIsNegativeThrowsException() {
        assertThrows<NotValidException> { shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().scoreNegative(user1.id)) }
    }

    @Test
    fun whenCreatingUserIsNullThrowsException() {
        assertThrows<NotValidException> { shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().userNull()) }
    }

    @Test
    fun whenCreatingUserIsNotFoundThrowsException() {
        assertThrows<NotFoundException> { shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().userNotFound()) }
    }

    @Test
    fun whenCreatingForOtherUserThrowsException() {
        assertThrows<ForbiddenException> { shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user2.id)) }
    }

    @Test
    fun whenDeletingCorrectBehaviour() {
        val shuttleDrunkTestResult = shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user1.id))

        entityManager.flush()
        entityManager.clear()

        shuttleDrunkTestResultService.delete(shuttleDrunkTestResult.id)

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotFoundException> { shuttleDrunkTestResultService.findById(shuttleDrunkTestResult.id) }
    }

    @Test
    fun whenDeletingNotFoundThrowsException() {
        assertThrows<NotFoundException> { shuttleDrunkTestResultService.findById(-1L) }
    }

    @Test
    fun whenDeletingFromOtherUserThrowsException() {
        val shuttleDrunkTestResult = shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user1.id))

        entityManager.flush()
        entityManager.clear()

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)

        assertThrows<ForbiddenException> { shuttleDrunkTestResultService.delete(shuttleDrunkTestResult.id) }
    }

}