package nl.dirtya.flatsiteapi.model.shuttedrunktest

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.shuttledrunktest.service.ShuttleDrunkTestOverviewService
import nl.dirtya.flatsiteapi.model.shuttledrunktest.service.ShuttleDrunkTestResultService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import javax.persistence.EntityManager

class ShuttleDrunkTestOverviewServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val shuttleDrunkTestResultService: ShuttleDrunkTestResultService,
        private val shuttleDrunkTestOverviewService: ShuttleDrunkTestOverviewService,
        private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User
    private lateinit var user3: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        user3 = userService.create(UserFactory().createValid3())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenGettingOverviewCorrectBehaviour() {
        val user1HighScore = 4.5
        val user2HighScore = 2.5

        shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user1.id, 0.5))
        shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user1.id, user1HighScore))

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)
        shuttleDrunkTestResultService.create(ShuttleDrunkTestResultFactory().createValid1(user2.id, user2HighScore))

        entityManager.flush()
        entityManager.clear()

        val overview = shuttleDrunkTestOverviewService.getOverview(true);

        assertEquals(user1HighScore, overview[user1])
        assertEquals(user2HighScore, overview[user2])
        assertEquals(0.0, overview[user3])
    }

}