package nl.dirtya.flatsiteapi.model.shuttedrunktest

import nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto.ShuttleDrunkTestResultWriteDto
import java.time.LocalDate

class ShuttleDrunkTestResultFactory {

    fun createValid1(userId: Long): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(LocalDate.now(), 2.5, userId)
    }

    fun createValid1(userId: Long, score: Double): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(LocalDate.now(), score, userId)
    }

    fun dateNull(userId: Long): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(null, 2.5, userId)
    }

    fun scoreNull(userId: Long): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(LocalDate.now(), null, userId)
    }

    fun scoreNegative(userId: Long): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(LocalDate.now(), -2.5, userId)
    }

    fun userNull(): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(LocalDate.now(), 2.5, null)
    }

    fun userNotFound(): ShuttleDrunkTestResultWriteDto {
        return ShuttleDrunkTestResultWriteDto(LocalDate.now(), 2.5, -1L)
    }

}