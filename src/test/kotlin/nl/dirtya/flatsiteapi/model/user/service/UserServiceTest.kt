package nl.dirtya.flatsiteapi.model.user.service

import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.user.api.AutoEnroll
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserWriteDto
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.security.utils.PasswordEncoder
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import javax.persistence.EntityManager

class UserServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val userService: UserService
): BaseTest() {

     fun assertValid(writeDto: UserWriteDto, user: User) {
         assertEquals(writeDto.username, user.username)
         assertTrue(PasswordEncoder.instance.matches(writeDto.password, user.password))

         if (writeDto.autoEnroll == null) {
             assertEquals(AutoEnroll(), user.autoEnroll)
         } else {
             assertEquals(writeDto.autoEnroll, user.autoEnroll)
         }
     }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val amount = userService.findAll().size

        userService.create(UserFactory().createValid1())
        userService.create(UserFactory().createValid2())

        entityManager.flush()
        entityManager.clear()

        assertEquals(amount+2, userService.findAll().size)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val user = userService.create(UserFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundUser = userService.findById(user.id)

        assertEquals(user, foundUser)
    }

    @Test
    fun whenFindingByUsernameCorrectBehaviour() {
        val user = userService.create(UserFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundUser = userService.findByUsername(user.username)

        assertEquals(user, foundUser)
    }

    @Test
    fun whenFindingByActiveCorrectBehaviour() {
        val amount = userService.findByActive(true).size
        userService.create(UserFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        assertEquals(amount+1, userService.findByActive(true).size)
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val writeDto = UserFactory().createValid1()
        val user = userService.create(writeDto)
        assertValid(writeDto, user)
    }

    @Test
    fun whenCreatingUsernameIsNullThrowsException() {
        val writeDto = UserFactory().usernameIsNull()
        assertThrows<NotValidException> { userService.create(writeDto) }
    }

    @Test
    fun whenCreatingPasswordIsNullThrowsException() {
        val writeDto = UserFactory().passwordIsNull()
        assertThrows<NotValidException> { userService.create(writeDto) }
    }

    @Test
    fun whenCreatingPasswordRepeatIsNullThrowsException() {
        val writeDto = UserFactory().passwordRepeatIsNull()
        assertThrows<NotValidException> { userService.create(writeDto) }
    }

    @Test
    fun whenCreatingPasswordsDontMatchThrowsException() {
        val writeDto = UserFactory().passwordsDontMatch()
        assertThrows<NotValidException> { userService.create(writeDto) }
    }

    @Test
    fun whenCreatingUsernameAlreadyExistsThrowsException() {
        val writeDto = UserFactory().createValid1()
        userService.create(writeDto)
        assertThrows<NotValidException> { userService.create(writeDto) }
    }

    @Test
    fun whenUpdatingCorrectBehaviour() {
        val user = userService.create(UserFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val writeDto = UserFactory().updateValid1()
        val updatedUser = userService.update(user.id, writeDto)

        assertEquals(writeDto.username, updatedUser.username)
        assertEquals(writeDto.autoEnroll, updatedUser.autoEnroll)
    }

    @Test
    fun whenUpdatingUsernameIsNullThrowsException() {
        val user = userService.create(UserFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val writeDto = UserFactory().usernameIsNull()
        assertThrows<NotValidException> { userService.update(user.id, writeDto) }
    }

}