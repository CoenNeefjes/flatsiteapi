package nl.dirtya.flatsiteapi.model.user.factory

import nl.dirtya.flatsiteapi.model.user.api.AutoEnroll
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserWriteDto

class UserFactory {

    fun createValid1(): UserWriteDto {
        return UserWriteDto("username", "password", "password", null, null)
    }

    fun createValid2(): UserWriteDto {
        return UserWriteDto("username2", "password2", "password2", null, null)
    }

    fun createValid3(): UserWriteDto {
        return UserWriteDto("username3", "password3", "password3", null, null)
    }

    fun usernameIsNull(): UserWriteDto {
        return UserWriteDto(null, "password", "password", null, null)
    }

    fun passwordIsNull(): UserWriteDto {
        return UserWriteDto("username", null, "password", null, null)
    }

    fun passwordRepeatIsNull(): UserWriteDto {
        return UserWriteDto("username", "password", null, null, null)
    }

    fun passwordsDontMatch(): UserWriteDto {
        return UserWriteDto("username", "password", "password2", null, null)
    }

    fun updateValid1(): UserWriteDto {
        return UserWriteDto("usernameUpdated", null, null, null, AutoEnroll(monday = true, tuesday = false))
    }

}