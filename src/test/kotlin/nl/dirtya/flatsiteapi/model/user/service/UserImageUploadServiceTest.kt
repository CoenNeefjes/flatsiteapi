package nl.dirtya.flatsiteapi.model.user.service

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.image.service.ImageFileService
import nl.dirtya.flatsiteapi.model.image.service.ImageLocationResolver
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import java.io.File
import java.time.LocalDate

class UserImageUploadServiceTest
@Autowired constructor(
        private val userImageUploadService: UserImageUploadService,
        private val userService: UserService,
        private val imageFileService: ImageFileService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService
    @MockBean lateinit var imageLocationResolver: ImageLocationResolver
    @TempDir lateinit var tempFolder: File

    lateinit var user: User

    @BeforeEach
    fun setup() {
        whenever(imageLocationResolver.resolve()).thenReturn(tempFolder.absolutePath)

        user = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    private fun createMultipart(): MockMultipartFile {
        assertTrue(tempFolder.isDirectory)
        val resource = ClassPathResource("files/landscape2.jpg")
        assertTrue(resource.exists())

        val resourceBytes = resource.file.readBytes()

        return MockMultipartFile("file", "image_with_name.jpg", MediaType.IMAGE_JPEG_VALUE, resourceBytes)
    }

    @Test
    fun whenUploadingCorrectBehaviour() {
        userImageUploadService.upload(createMultipart())

        val foundUser = userService.getCurrentUser()

        assertNotNull(foundUser.image)
        assertEquals(foundUser, foundUser.image!!.createdBy)
        assertEquals(LocalDate.now(), foundUser.image!!.createdOn)
    }

    @Test
    fun whenUploadingNewImageCorrectBehaviour() {
        userImageUploadService.upload(createMultipart())

        val firstImage = userService.getCurrentUser().image!!

        userImageUploadService.upload(createMultipart())

        val secondImage = userService.getCurrentUser().image!!

        assertNotEquals(firstImage, secondImage)
    }

    @Test
    fun whenUploadingOldImageIsDeleted() {
        userImageUploadService.upload(createMultipart())
        val foundUser = userService.getCurrentUser()

        val image = foundUser.image!!

        userImageUploadService.upload(createMultipart())

        assertThrows<NotFoundException> { imageFileService.findById(image.id) }
    }

}