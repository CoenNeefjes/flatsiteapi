package nl.dirtya.flatsiteapi.model.user.controller

import nl.dirtya.flatsiteapi.IntegrationTest
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content

class UserControllerTest: IntegrationTest() {

    @Test
    fun whenFindingActiveCorrectBehaviour() {
        mockMvc.perform(MockMvcRequestBuilders.get("/users/active")
                .header("Authorization", userAuthHeader)
        )
                .andExpect(MockMvcResultMatchers.status().isOk)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
    }

}