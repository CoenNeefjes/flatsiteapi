package nl.dirtya.flatsiteapi.model.beerinput

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputWriteDto
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputBalanceService
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputService
import nl.dirtya.flatsiteapi.model.beertype.BeerTypeFactory
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate

class BeerInputBalanceServiceTest
@Autowired constructor(
        private val beerInputBalanceService: BeerInputBalanceService,
        private val userService: UserService,
        private val beerTypeService: BeerTypeService,
        private val beerInputService: BeerInputService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User
    private lateinit var user2: User
    private lateinit var admin: User
    private lateinit var beerType: BeerType

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        admin = userService.findAll().find { it.getRoles().contains(Roles.ADMIN) }!!
        beerType = beerTypeService.create(BeerTypeFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)
    }

    @Test
    fun whenCalculatingBalanceForUsersCorrectBehaviour() {
        val plusUser1 = 5
        val plusUser2 = 3

        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        BeerInputType.RECEIPT,
                        beerType.id,
                        null,
                        listOf(
                                BeerInputEnrolledWriteDto(user.id, plusUser1),
                                BeerInputEnrolledWriteDto(user2.id, plusUser2)
                        )
                )
        )

        val balances = beerInputBalanceService.calculateBalanceForUsers(beerType.id, false)
        assertEquals(userService.findAll().size, balances.size)
        assertEquals(plusUser1, balances[user])
        assertEquals(plusUser2, balances[user2])

        val minusUser1 = 44
        val minusUser2 = 3
        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        BeerInputType.LIST,
                        beerType.id,
                        null,
                        listOf(
                                BeerInputEnrolledWriteDto(user.id, minusUser1),
                                BeerInputEnrolledWriteDto(user2.id, minusUser2)
                        )
                )
        )

        val updatedBalances = beerInputBalanceService.calculateBalanceForUsers(beerType.id, false)
        assertEquals(userService.findAll().size, updatedBalances.size)
        assertEquals(plusUser1-minusUser1, updatedBalances[user])
        assertEquals(plusUser2-minusUser2, updatedBalances[user2])
    }

    @Test
    fun whenCalculatingBalanceForUserCorrectBehaviour() {
        val plusUser1 = 5
        val plusUser2 = 3
        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        BeerInputType.RECEIPT,
                        beerType.id,
                        null,
                        listOf(
                                BeerInputEnrolledWriteDto(user.id, plusUser1),
                                BeerInputEnrolledWriteDto(user2.id, plusUser2)
                        )
                )
        )

        val balanceUser1 = beerInputBalanceService.calculateBalanceForUser(user, beerType)
        assertEquals(plusUser1, balanceUser1)
        val balanceUser2 = beerInputBalanceService.calculateBalanceForUser(user2, beerType)
        assertEquals(plusUser2, balanceUser2)

        val minusUser1 = 44
        val minusUser2 = 3
        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        BeerInputType.LIST,
                        beerType.id,
                        null,
                        listOf(
                                BeerInputEnrolledWriteDto(user.id, minusUser1),
                                BeerInputEnrolledWriteDto(user2.id, minusUser2)
                        )
                )
        )

        val updatedBalanceUser1 = beerInputBalanceService.calculateBalanceForUser(user, beerType)
        assertEquals(plusUser1-minusUser1, updatedBalanceUser1)
        val updatedBalanceUser2 = beerInputBalanceService.calculateBalanceForUser(user2, beerType)
        assertEquals(plusUser2-minusUser2, updatedBalanceUser2)
    }

    @Test
    fun whenCalculatingForMeCorrectBehaviour() {
        val plusUser1 = 5
        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        BeerInputType.RECEIPT,
                        beerType.id,
                        null,
                        listOf(
                                BeerInputEnrolledWriteDto(admin.id, plusUser1)
                        )
                )
        )

        assertEquals(plusUser1, beerInputBalanceService.calculateBalanceForMe(beerType.id))
    }

}