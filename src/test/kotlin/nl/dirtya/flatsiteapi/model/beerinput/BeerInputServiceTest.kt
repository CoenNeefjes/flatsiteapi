package nl.dirtya.flatsiteapi.model.beerinput

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.album.AlbumFactory
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputService
import nl.dirtya.flatsiteapi.model.beertype.BeerTypeFactory
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import nl.dirtya.flatsiteapi.model.image.service.ImageService
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Pageable
import javax.persistence.EntityManager

class BeerInputServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerInputService: BeerInputService,
        private val userService: UserService,
        private val beerTypeService: BeerTypeService,
        private val imageService: ImageService,
        private val albumService: AlbumService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User
    private lateinit var user2: User
    private lateinit var admin: User
    private lateinit var beerType: BeerType

    private fun flush() {
        entityManager.flush()
        entityManager.clear()
    }

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        admin = userService.findAll().find { it.getRoles().contains(Roles.ADMIN) }!!
        beerType = beerTypeService.create(BeerTypeFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val amount = beerInputService.findAll().size
        beerInputService.create(BeerInputFactory().createValid1(beerType, user, user2))

        flush()

        val updatedAmount = beerInputService.findAll().size
        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user, user2))

        flush()

        val foundBeerInput = beerInputService.findById(beerInput.id)
        assertEquals(beerInput, foundBeerInput)
    }

    @Test
    fun whenSearchingBeforeCorrectBehaviour() {
        val writeDto = BeerInputFactory().createValid1(beerType, user, user2)
        val beerInput = beerInputService.create(writeDto)

        flush()

        val foundBeerInputs = beerInputService.search(writeDto.date!!.plusDays(1), null, null, null, null, Pageable.unpaged())

        assertEquals(1, foundBeerInputs.size)
        assertTrue(foundBeerInputs.content.contains(beerInput))
    }

    @Test
    fun whenSearchingAfterCorrectBehaviour() {
        val writeDto = BeerInputFactory().createValid1(beerType, user, user2)
        val beerInput = beerInputService.create(writeDto)

        flush()

        val foundBeerInputs = beerInputService.search(null, writeDto.date!!.minusDays(1), null, null, null, Pageable.unpaged())

        assertEquals(1, foundBeerInputs.size)
        assertTrue(foundBeerInputs.content.contains(beerInput))
    }

    @Test
    fun whenSearchingBeerInputTypeCorrectBehaviour() {
        val writeDto = BeerInputFactory().createValid1(beerType, user, user2)
        val beerInput = beerInputService.create(writeDto)

        flush()

        val foundBeerInputs = beerInputService.search(null, null, writeDto.type, null, null, Pageable.unpaged())

        assertEquals(1, foundBeerInputs.size)
        assertTrue(foundBeerInputs.content.contains(beerInput))
    }

    @Test
    fun whenSearchingCreatedByCorrectBehaviour() {
        val writeDto = BeerInputFactory().createValid1(beerType, admin, user2)
        val beerInput = beerInputService.create(writeDto)

        flush()

        val foundBeerInputs = beerInputService.search(null, null, null, admin.id, null, Pageable.unpaged())

        assertEquals(1, foundBeerInputs.size)
        assertTrue(foundBeerInputs.content.contains(beerInput))
    }

    @Test
    fun whenSearchingBeerTypeCorrectBehaviour() {
        val writeDto = BeerInputFactory().createValid1(beerType, admin, user2)
        val beerInput = beerInputService.create(writeDto)

        flush()

        val foundBeerInputs = beerInputService.search(null, null, null, null, writeDto.beerType, Pageable.unpaged())

        assertEquals(1, foundBeerInputs.size)
        assertTrue(foundBeerInputs.content.contains(beerInput))
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val writeDto = BeerInputFactory().createValid1(beerType, admin, user2)
        val beerInput = beerInputService.create(writeDto)

        assertEquals(writeDto.date, beerInput.date)
        assertEquals(writeDto.beerType, beerInput.beerType.id)
        assertEquals(writeDto.type, beerInput.type)
        assertEquals(writeDto.enrollments!!.size, beerInput.enrollments.size)
    }

    @Test
    fun whenCreatingAsNonAdminThrowsException() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
        assertThrows<ForbiddenException> { beerInputService.create(BeerInputFactory().createValid1(beerType, user, user2)) }
    }

    @Test
    fun whenCreatingDateIsNullThrowsException() {
        assertThrows<NotValidException> { beerInputService.create(BeerInputFactory().dateIsNull(beerType, user, user2)) }
    }

    @Test
    fun whenCreatingBeerInputTypeIsNullThrowsException() {
        assertThrows<NotValidException> { beerInputService.create(BeerInputFactory().beerInputTypeIsNull(beerType, user, user2)) }
    }

    @Test
    fun whenCreatingBeerTypeIsNullThrowsException() {
        assertThrows<NotValidException> { beerInputService.create(BeerInputFactory().beerTypeIsNull(user, user2)) }
    }

    @Test
    fun whenCreatingBeerTypeNotFoundThrowsException() {
        assertThrows<NotFoundException> { beerInputService.create(BeerInputFactory().beerTypeNotFound(user, user2)) }
    }

    @Test
    fun whenCreatingUnitsIsNullThrowsException() {
        assertThrows<NotValidException> { beerInputService.create(BeerInputFactory().unitsIsNull(beerType)) }
    }

    @Test
    fun whenCreatingUnitsIsEmptyThrowsException() {
        assertThrows<NotValidException> { beerInputService.create(BeerInputFactory().unitsIsEmpty(beerType)) }
    }

    @Test
    fun whenCreatingUnitsContainsNullThrowsException() {
        assertThrows<NotValidException> { beerInputService.create(BeerInputFactory().unitsContainsNull(beerType, user, user2)) }
    }

    @Test
    fun whenUpdatingImageCorrectBehaviour() {
        val album = albumService.create(AlbumFactory().createValid1())
        val image = imageService.create(album)

        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user, user2))

        assertNull(beerInput.image)

        val updatedBeerInput = beerInputService.updateImage(beerInput, image)

        assertEquals(image, updatedBeerInput.image)
    }

    @Test
    fun whenUpdatingImageByOtherUserThrowsException() {
        val album = albumService.create(AlbumFactory().createValid1())
        val image = imageService.create(album)
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user, user2))

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)

        assertThrows<ForbiddenException> { beerInputService.updateImage(beerInput, image) }
    }

    @Test
    fun whenUpdatingBeerInputCorrectBehaviour() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, admin, user2))
        val beerType2 = beerTypeService.create(BeerTypeFactory().createValid2())

        flush()

        val updateWriteDto = BeerInputFactory().updateValid1(beerType2, admin, user, user2)
        val updatedBeerInput = beerInputService.update(beerInput.id, updateWriteDto)

        assertEquals(beerInput.date, updatedBeerInput.date)
        assertEquals(updateWriteDto.beerType, updatedBeerInput.beerType.id)
        assertEquals(updateWriteDto.note, updatedBeerInput.note)
        assertEquals(updateWriteDto.enrollments!!.size, updatedBeerInput.enrollments.size)
    }

    @Test
    fun whenUpdatingBeerInputOfOtherUserThrowsForbiddenException() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, admin, user2))
        val updateWriteDto = BeerInputFactory().updateValid1(beerType, admin, user, user2)

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)

        assertThrows<ForbiddenException> { beerInputService.update(beerInput.id, updateWriteDto) }
    }

    @Test
    fun whenUpdatingBeerInputEmptyNoteCorrectBehaviour() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, admin, user2))
        val beerType2 = beerTypeService.create(BeerTypeFactory().createValid2())

        flush()

        val updateWriteDto = BeerInputFactory().updateValid2(beerType2, admin, user, user2)
        val updatedBeerInput = beerInputService.update(beerInput.id, updateWriteDto)

        assertEquals(beerInput.date, updatedBeerInput.date)
        assertEquals(updateWriteDto.beerType, updatedBeerInput.beerType.id)
        assertEquals(null, updatedBeerInput.note)
        assertEquals(updateWriteDto.enrollments!!.size, updatedBeerInput.enrollments.size)
    }
}