package nl.dirtya.flatsiteapi.model.beerinput

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputWriteDto
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputService
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputEnrolledService
import nl.dirtya.flatsiteapi.model.beertype.BeerTypeFactory
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class BeerInputEnrolledServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerInputEnrolledService: BeerInputEnrolledService,
        private val beerInputService: BeerInputService,
        private val userService: UserService,
        private val beerTypeService: BeerTypeService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User
    private lateinit var user2: User
    private lateinit var admin: User
    private lateinit var beerType: BeerType

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        admin = userService.findAll().find { it.getRoles().contains(Roles.ADMIN) }!!
        beerType = beerTypeService.create(BeerTypeFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))

        entityManager.flush()
        entityManager.clear()

        val amount = beerInputEnrolledService.findAll().size
        beerInputEnrolledService.create(BeerInputEnrollmentFactory().createValid1(user2)!!, beerInput)

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = beerInputEnrolledService.findAll().size

        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))
        val beerInputUnit = beerInputEnrolledService.create(BeerInputEnrollmentFactory().createValid1(user2)!!, beerInput)

        entityManager.flush()
        entityManager.clear()

        val foundBeerInputUnit = beerInputEnrolledService.findById(beerInputUnit.id)

        assertEquals(beerInputUnit, foundBeerInputUnit)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { beerInputEnrolledService.findById(-1) }
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))

        entityManager.flush()
        entityManager.clear()

        val amountOfUnits = beerInputEnrolledService.findAll().size

        val writeDto = BeerInputEnrollmentFactory().createValid1(user2)!!
        val beerInputUnit = beerInputEnrolledService.create(writeDto, beerInput)

        assertEquals(amountOfUnits+1, beerInputEnrolledService.findAll().size)
        assertEquals(beerInput, beerInputUnit.beerInput)
        assertEquals(writeDto.amount, beerInputUnit.amount)
        assertEquals(writeDto.user, beerInputUnit.user.id)
    }

    @Test
    fun whenCreatingAmountIsNullThrowsException() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))
        assertThrows<NotValidException> { beerInputEnrolledService.create(BeerInputEnrollmentFactory().amountIsNull(user2), beerInput) }
    }

    @Test
    fun whenCreatingAmountIsZeroThrowsException() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))
        assertThrows<NotValidException> { beerInputEnrolledService.create(BeerInputEnrollmentFactory().amountIsZero(user2), beerInput) }
    }

    @Test
    fun whenCreatingUserIsNullThrowsException() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))
        assertThrows<NotValidException> { beerInputEnrolledService.create(BeerInputEnrollmentFactory().userIsNull(), beerInput) }
    }

    @Test
    fun whenCreatingUserNotFoundThrowsException() {
        val beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, user))
        assertThrows<NotFoundException> { beerInputEnrolledService.create(BeerInputEnrollmentFactory().userNotFound(), beerInput) }
    }

    @Test
    fun whenCountingReceiptCorrectBehaviour() {
        val plus = 3
        val type = BeerInputType.RECEIPT
        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        type,
                        beerType.id,
                        null,
                        listOf(BeerInputEnrolledWriteDto(user.id, plus))
                )
        )

        assertEquals(plus, beerInputEnrolledService.count(user.id, type, beerType.id))
    }

    @Test
    fun whenCountingListCorrectBehaviour() {
        val minus = 3
        val type = BeerInputType.LIST
        beerInputService.create(
                BeerInputWriteDto(LocalDate.now(),
                        type,
                        beerType.id,
                        null,
                        listOf(BeerInputEnrolledWriteDto(user.id, minus))
                )
        )

        assertEquals(minus, beerInputEnrolledService.count(user.id, type, beerType.id))
    }

}