package nl.dirtya.flatsiteapi.model.beerinput

import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputWriteDto
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.user.api.User
import java.time.LocalDate
import java.util.*

class BeerInputFactory {

    fun createValid1(beerType: BeerType, vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.LIST, beerType.id, null, users.map { BeerInputEnrollmentFactory().createValid1(it) })
    }

    fun updateValid1(beerType: BeerType, vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.RECEIPT, beerType.id, "note", users.map { BeerInputEnrollmentFactory().createValid2(it) })
    }

    fun updateValid2(beerType: BeerType, vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.RECEIPT, beerType.id, null, users.map { BeerInputEnrollmentFactory().createValid2(it) })
    }

    fun dateIsNull(beerType: BeerType, vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(null, BeerInputType.LIST, beerType.id, null, users.map { BeerInputEnrollmentFactory().createValid1(it) })
    }

    fun beerInputTypeIsNull(beerType: BeerType, vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), null, beerType.id, null, users.map { BeerInputEnrollmentFactory().createValid1(it) })
    }

    fun beerTypeIsNull(vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.LIST, null, null, users.map { BeerInputEnrollmentFactory().createValid1(it) })
    }

    fun beerTypeNotFound(vararg users: User): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.LIST, -1, null, users.map { BeerInputEnrollmentFactory().createValid1(it) })
    }

    fun unitsIsNull(beerType: BeerType): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.LIST, beerType.id, null, null)
    }

    fun unitsIsEmpty(beerType: BeerType): BeerInputWriteDto {
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.LIST, beerType.id, null, Collections.emptyList())
    }

    fun unitsContainsNull(beerType: BeerType, vararg users: User): BeerInputWriteDto {
        val units = users.map { BeerInputEnrollmentFactory().createValid1(it) }.toMutableList()
        units.add(null)
        return BeerInputWriteDto(LocalDate.now(), BeerInputType.LIST, beerType.id, null, units)
    }
}