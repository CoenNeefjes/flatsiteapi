package nl.dirtya.flatsiteapi.model.beerinput

import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.user.api.User

class BeerInputEnrollmentFactory {

    fun createValid1(user: User): BeerInputEnrolledWriteDto? {
        return BeerInputEnrolledWriteDto(user.id, 1)
    }

    fun createValid2(user: User): BeerInputEnrolledWriteDto {
        return BeerInputEnrolledWriteDto(user.id, 2)
    }

    fun userIsNull(): BeerInputEnrolledWriteDto {
        return BeerInputEnrolledWriteDto(null, 1)
    }

    fun userNotFound(): BeerInputEnrolledWriteDto {
        return BeerInputEnrolledWriteDto(-1, 1)
    }

    fun amountIsNull(user: User): BeerInputEnrolledWriteDto {
        return BeerInputEnrolledWriteDto(user.id, null)
    }

    fun amountIsZero(user: User): BeerInputEnrolledWriteDto {
        return BeerInputEnrolledWriteDto(user.id, 0)
    }
}