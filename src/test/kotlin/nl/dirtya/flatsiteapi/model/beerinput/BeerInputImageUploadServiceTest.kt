package nl.dirtya.flatsiteapi.model.beerinput

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputImageUploadService
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputService
import nl.dirtya.flatsiteapi.model.beertype.BeerTypeFactory
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import nl.dirtya.flatsiteapi.model.image.service.ImageLocationResolver
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.io.TempDir
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.core.io.ClassPathResource
import org.springframework.http.MediaType
import org.springframework.mock.web.MockMultipartFile
import java.io.File
import javax.persistence.EntityManager

class BeerInputImageUploadServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerInputImageUploadService: BeerInputImageUploadService,
        private val beerInputService: BeerInputService,
        private val beerTypeService: BeerTypeService,
        private val userService: UserService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService
    @MockBean lateinit var imageLocationResolver: ImageLocationResolver
    @TempDir lateinit var tempFolder: File

    lateinit var beerInput: BeerInput
    lateinit var admin: User
    lateinit var beerType: BeerType

    @BeforeEach
    fun setup() {
        whenever(imageLocationResolver.resolve()).thenReturn(tempFolder.absolutePath)

        admin = userService.findAll().find { it.getRoles().contains(Roles.ADMIN) }!!
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)

        beerType = beerTypeService.create(BeerTypeFactory().createValid1())
        beerInput = beerInputService.create(BeerInputFactory().createValid1(beerType, admin))
    }

    private fun createMultipart(): MockMultipartFile {
        assertTrue(tempFolder.isDirectory)
        val resource = ClassPathResource("files/landscape2.jpg")
        assertTrue(resource.exists())

        val resourceBytes = resource.file.readBytes()

        return MockMultipartFile("file", "image_with_name.jpg", MediaType.IMAGE_JPEG_VALUE, resourceBytes)
    }

    @Test
    fun whenUploadingCorrectBehaviour() {
        val multipart = createMultipart()

        val updatedBeerInput = beerInputImageUploadService.upload(multipart, beerInput.id)

        assertNotNull(updatedBeerInput.image)
    }

    @Test
    fun whenUploadingBeerInputNotFoundThrowsException() {
        assertThrows<NotFoundException> { beerInputImageUploadService.upload(createMultipart(), -1) }
    }

}