package nl.dirtya.flatsiteapi.model.cooking

import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDryersWriteDto
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEventWriteDto
import nl.dirtya.flatsiteapi.model.user.api.User

class CookingEventFactory {

    fun createValid1(vararg users: User): CookingEventWriteDto {
        return CookingEventWriteDto(users.map { CookingEnrolledWriteDto(it.id, 2) })
    }

    fun createValid1(vararg userIds: Long): CookingEventWriteDto {
        return CookingEventWriteDto(userIds.map { CookingEnrolledWriteDto(it, 2) })
    }

    fun updateCookingDryersValid(dryer1: Long?, dryer2: Long?): CookingDryersWriteDto {
        return CookingDryersWriteDto(dryer1, dryer2)
    }

}