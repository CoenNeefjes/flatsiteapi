package nl.dirtya.flatsiteapi.model.cooking

import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDonationWriteDto
import nl.dirtya.flatsiteapi.model.user.api.User

class CookingDonationFactory {

    fun createValid1(recipient: User): CookingDonationWriteDto {
        return CookingDonationWriteDto(3, recipient.id)
    }

    fun createValid2(recipient: User): CookingDonationWriteDto {
        return CookingDonationWriteDto(6, recipient.id)
    }

    fun amountIsNull(recipient: User): CookingDonationWriteDto {
        return CookingDonationWriteDto(null, recipient.id)
    }

    fun amountIsZero(recipient: User): CookingDonationWriteDto {
        return CookingDonationWriteDto(0, recipient.id)
    }

    fun userIsNull(): CookingDonationWriteDto {
        return CookingDonationWriteDto(3, null)
    }

    fun userNotFound(): CookingDonationWriteDto {
        return CookingDonationWriteDto(3, -1)
    }

}