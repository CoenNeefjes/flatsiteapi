package nl.dirtya.flatsiteapi.model.cooking

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEnrolledService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class CookingEnrolledServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val cookingEnrolledService: CookingEnrolledService,
        private val cookingEventService: CookingEventService,
        private val userService: UserService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var cookingEvent: CookingEvent
    private lateinit var user: User
    private lateinit var user2: User

    @BeforeEach
    fun beforeEach() {
        cookingEvent  = cookingEventService.findByDateOrCreate(LocalDate.now())
        user = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val enrollment = cookingEnrolledService.create(user, 2, cookingEvent)

        entityManager.flush()
        entityManager.clear()

        val foundEnrollment = cookingEnrolledService.findById(enrollment.id)

        assertEquals(enrollment, foundEnrollment)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingEnrolledService.findById(-1) }
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val amountToEnroll = 1
        val enrollment = cookingEnrolledService.create(user, amountToEnroll, cookingEvent)

        assertEquals(user, enrollment.user)
        assertEquals(amountToEnroll, enrollment.amount)
        assertEquals(cookingEvent, enrollment.cookingEvent)
    }

    @Test
    fun whenCreatingAmountIsBelowZeroThrowsException() {
        assertThrows<NotValidException> { cookingEnrolledService.create(user, -1, cookingEvent) }
    }

    @Test
    fun whenUpdatingCorrectBehaviour() {
        val enrollment = cookingEnrolledService.create(user, 1, cookingEvent)
        val updatedEnrollment = cookingEnrolledService.update(enrollment, 3)

        assertEquals(3, updatedEnrollment.amount)
    }

    @Test
    fun whenUpdatingAmountIsBelowZeroThrowsException() {
        val enrollment = cookingEnrolledService.create(user, 1, cookingEvent)
        assertThrows<NotValidException> { cookingEnrolledService.update(enrollment, -1) }
    }

    @Test
    fun whenDeletingAllCorrectBehaviour() {
        val amount = cookingEnrolledService.findAll().size

        val enrollment1 = cookingEnrolledService.create(user, 1, cookingEvent)
        val enrollment2 = cookingEnrolledService.create(user2, 3, cookingEvent)

        val newAmount = cookingEnrolledService.findAll().size
        assertEquals(amount+2, newAmount)

        cookingEnrolledService.deleteAll(setOf(enrollment1, enrollment2))

        val finalAmount = cookingEnrolledService.findAll().size
        assertEquals(amount, finalAmount)
    }

    @Test
    fun whenSummingEnrollmentsCorrectBehaviour() {
        val amountToEnroll = 1
        cookingEventService.enroll(cookingEvent.id, amountToEnroll)
        assertEquals(amountToEnroll - 1, cookingEnrolledService.sumEnrollmentsWhereCookWasPresent(user.id))

        cookingEventService.enrollAsCook(cookingEvent.id)
        assertEquals(amountToEnroll, cookingEnrolledService.sumEnrollmentsWhereCookWasPresent(user.id))
    }

}