package nl.dirtya.flatsiteapi.model.cooking

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.service.CookingActionService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate

class CookingActionServiceTest
@Autowired constructor(
    private val cookingActionService: CookingActionService,
    private val cookingEventService: CookingEventService,
    private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val message = "test";

        val event = cookingEventService.create(LocalDate.now())
        val action = cookingActionService.create(event, user1, message)

        assertNotNull(action.id)
        assertNotNull(action.uuid)
        assertEquals(event, action.cookingEvent)
        assertEquals(user1, action.user)
        assertEquals(message, action.message)
        assertNotNull(action.timestamp)
    }

}