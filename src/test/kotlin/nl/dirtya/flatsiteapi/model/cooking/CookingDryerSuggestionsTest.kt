package nl.dirtya.flatsiteapi.model.cooking

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate

/**
 * This test case has its own test class since it has a lot of setup work
 */

class CookingDryerSuggestionsTest
@Autowired constructor(
        private val cookingEventService: CookingEventService,
        private val userService: UserService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User
    private lateinit var user3: User
    private lateinit var user4: User

    private lateinit var mondayEvent: CookingEvent
    private lateinit var tuesdayEvent: CookingEvent
    private lateinit var wednesdayEvent: CookingEvent

    @BeforeEach
    fun setup() {
        user1 = userService.findAll()[0]
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
        user2 = userService.create(UserFactory().createValid1())
        user3 = userService.create(UserFactory().createValid2())
        user4 = userService.create(UserFactory().createValid3())

        mondayEvent = cookingEventService.findByDateOrCreate(LocalDate.parse("2021-01-04"))
        tuesdayEvent = cookingEventService.findByDateOrCreate(LocalDate.parse("2021-01-05"))
        wednesdayEvent = cookingEventService.findByDateOrCreate(LocalDate.parse("2021-01-06"))
    }

    @Test
    fun whenGettingDryerSuggestionsCorrectBehaviour() {
        // Enroll user 1 as cook on monday
        cookingEventService.enrollAsCook(mondayEvent.id)
        // Enroll all other users on monday
        cookingEventService.update(mondayEvent.id, CookingEventFactory().createValid1(user1, user2, user3, user4))
        // Get dryer suggestions for monday
        val mondayDryerSuggestions = cookingEventService.getDryerSuggestions(mondayEvent.id)
        // User 3 and 4 should be dryers since they are lowest in hierarchy
        assertTrue(mondayDryerSuggestions.containsAll(listOf(user3, user4)))
        // Set dryers for monday
        cookingEventService.updateDryers(mondayEvent.id, CookingEventFactory().updateCookingDryersValid(user3.id, user4.id))
        // This should not have effect on the suggestion
        assertTrue(cookingEventService.getDryerSuggestions(mondayEvent.id).containsAll(listOf(user3, user4)))

        // Tuesday
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)
        cookingEventService.enrollAsCook(tuesdayEvent.id)
        cookingEventService.update(tuesdayEvent.id, CookingEventFactory().createValid1(user1, user2, user3, user4))
        val tuesdayDryerSuggestions = cookingEventService.getDryerSuggestions(tuesdayEvent.id)
        assertTrue(tuesdayDryerSuggestions.containsAll(listOf(user1, user4)))
        cookingEventService.updateDryers(tuesdayEvent.id, CookingEventFactory().updateCookingDryersValid(user1.id, user4.id))
        assertTrue(cookingEventService.getDryerSuggestions(tuesdayEvent.id).containsAll(listOf(user1, user4)))

        // Wednesday
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user3.id)
        cookingEventService.enrollAsCook(wednesdayEvent.id)
        cookingEventService.update(wednesdayEvent.id, CookingEventFactory().createValid1(user1, user2, user3, user4))
        val wednesdayDryerSuggestions = cookingEventService.getDryerSuggestions(wednesdayEvent.id)
        assertTrue(wednesdayDryerSuggestions.containsAll(listOf(user1, user2)))
        cookingEventService.updateDryers(wednesdayEvent.id, CookingEventFactory().updateCookingDryersValid(user1.id, user4.id))
        assertTrue(cookingEventService.getDryerSuggestions(wednesdayEvent.id).containsAll(listOf(user1, user2)))
    }

}