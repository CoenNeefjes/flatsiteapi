package nl.dirtya.flatsiteapi.model.cooking

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDonationWriteDto
import nl.dirtya.flatsiteapi.model.cooking.service.CookingBalanceService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingDonationService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Pageable
import java.time.LocalDate
import javax.persistence.EntityManager

class CookingDonationServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val cookingDonationService: CookingDonationService,
        private val userService: UserService,
        private val cookingBalanceService: CookingBalanceService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User
    private lateinit var user2: User

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val amount = cookingDonationService.findAll().size
        cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = cookingDonationService.findAll().size

        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenFindingAllPagedCorrectBehaviour() {
        val amount = cookingDonationService.findAllPaged(Pageable.unpaged()).size
        cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = cookingDonationService.findAllPaged(Pageable.unpaged()).size

        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val cookingDonation = cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val foundCookingDonation = cookingDonationService.findById(cookingDonation.id)

        assertEquals(cookingDonation, foundCookingDonation)
    }

    @Test
    fun whenSearchingNoCriteriaCorrectBehaviour() {
        cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val found = cookingDonationService.search(null, null, null, null, Pageable.unpaged())

        assertEquals(1, found.size)
    }

    @Test
    fun whenSearchingBeforeCorrectBehaviour() {
        val cookingDonation = cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val found = cookingDonationService.search(cookingDonation.createdOn.plusDays(1), null, null, null, Pageable.unpaged())

        assertEquals(1, found.size)
    }

    @Test
    fun whenSearchingAfterCorrectBehaviour() {
        val cookingDonation = cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val found = cookingDonationService.search(null, cookingDonation.createdOn.minusDays(1), null, null, Pageable.unpaged())

        assertEquals(1, found.size)
    }

    @Test
    fun whenSearchingCreatedByCorrectBehaviour() {
        val cookingDonation = cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val found = cookingDonationService.search(null, null, cookingDonation.createdBy.id, null, Pageable.unpaged())

        assertEquals(1, found.size)
    }

    @Test
    fun whenSearchingRecipientCorrectBehaviour() {
        val cookingDonation = cookingDonationService.create(CookingDonationFactory().createValid1(user2))

        entityManager.flush()
        entityManager.clear()

        val found = cookingDonationService.search(null, null, null, cookingDonation.recipient.id, Pageable.unpaged())

        assertEquals(1, found.size)
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val writeDto = CookingDonationFactory().createValid1(user2)
        val cookingDonation = cookingDonationService.create(writeDto)

        assertEquals(user, cookingDonation.createdBy)
        assertEquals(writeDto.amount, cookingDonation.amount)
        assertEquals(writeDto.recipient, cookingDonation.recipient.id)
        assertEquals(LocalDate.now(), cookingDonation.createdOn)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenCreatingAmountIsNullThrowsException() {
        assertThrows<NotValidException> { cookingDonationService.create(CookingDonationFactory().amountIsNull(user2)) }
    }

    @Test
    fun whenCreatingAmountIsZeroThrowsException() {
        assertThrows<NotValidException> { cookingDonationService.create(CookingDonationFactory().amountIsZero(user2)) }
    }

    @Test
    fun whenCreatingRecipientIsNullThrowsException() {
        assertThrows<NotValidException> { cookingDonationService.create(CookingDonationFactory().userIsNull()) }
    }

    @Test
    fun whenCreatingRecipientNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingDonationService.create(CookingDonationFactory().userNotFound()) }
    }

    @Test
    fun whenCreatingDonationToSelfThrowsException() {
        val writeDto = CookingDonationWriteDto(5, user.id)
        assertThrows<NotValidException> { cookingDonationService.create(writeDto) }
    }

    @Test
    fun whenCountingAmountGivenCorrectBehaviour() {
        val amount = cookingDonationService.countAmountGiven(user.id)

        val writeDto = CookingDonationFactory().createValid1(user2)
        cookingDonationService.create(writeDto)

        val updatedAmount = cookingDonationService.countAmountGiven(user.id)

        assertEquals(amount + writeDto.amount!!, updatedAmount)
    }

    @Test
    fun whenCountingAmountReceivedCorrectBehaviour() {
        val amount = cookingDonationService.countAmountReceived(user2.id)

        val writeDto = CookingDonationFactory().createValid1(user2)
        cookingDonationService.create(writeDto)

        val updatedAmount = cookingDonationService.countAmountReceived(user2.id)

        assertEquals(amount + writeDto.amount!!, updatedAmount)
    }

}