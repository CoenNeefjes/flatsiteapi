package nl.dirtya.flatsiteapi.model.cooking

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.service.CookingBalanceService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingDonationService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate

class CookingBalanceServiceTest
@Autowired constructor(
        private val cookingBalanceService: CookingBalanceService,
        private val cookingEventService: CookingEventService,
        private val cookingDonationService: CookingDonationService,
        private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun whenCalculatingBalanceForUsersCorrectBehaviour() {
        val initBalances = cookingBalanceService.calculateBalanceForUsers(false)
        initBalances.forEach { assertEquals(0, it.value) }

        val eventWriteDto = CookingEventFactory().createValid1(user1, user2)
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(event.id)
        cookingEventService.update(event.id, eventWriteDto)

        val newBalances = cookingBalanceService.calculateBalanceForUsers(false)
        assertEquals(eventWriteDto.enrollments!![0]!!.amount, newBalances[user1])
        assertEquals(eventWriteDto.enrollments!![0]!!.amount!!.times(-1), newBalances[user2])

        val donationWriteDto = CookingDonationFactory().createValid1(user2)
        cookingDonationService.create(donationWriteDto)
        val afterDonationBalances = cookingBalanceService.calculateBalanceForUsers(false)
        assertEquals(newBalances[user1]!!.minus(donationWriteDto.amount!!), afterDonationBalances[user1])
        assertEquals(newBalances[user2]!!.plus(donationWriteDto.amount!!), afterDonationBalances[user2])

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenCalculatingBalanceForUserCorrectBehaviour() {
        val initBalance = cookingBalanceService.calculateBalanceForUser(user1.id)
        assertEquals(0, initBalance)

        val eventWriteDto = CookingEventFactory().createValid1(user1, user2)
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(event.id)
        cookingEventService.update(event.id, eventWriteDto)

        val newBalance = cookingBalanceService.calculateBalanceForUser(user1.id)
        assertEquals(eventWriteDto.enrollments!![0]!!.amount, newBalance)

        val donationWriteDto = CookingDonationFactory().createValid1(user2)
        cookingDonationService.create(donationWriteDto)
        val afterDonationBalance = cookingBalanceService.calculateBalanceForUser(user1.id)
        assertEquals(newBalance - donationWriteDto.amount!!, afterDonationBalance)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenCalculatingBalanceForMeCorrectBehaviour() {
        val initBalance = cookingBalanceService.calculateBalanceForMe()
        assertEquals(0, initBalance)

        val eventWriteDto = CookingEventFactory().createValid1(user1, user2)
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(event.id)
        cookingEventService.update(event.id, eventWriteDto)

        val newBalance = cookingBalanceService.calculateBalanceForMe()
        assertEquals(eventWriteDto.enrollments!![0]!!.amount, newBalance)

        val donationWriteDto = CookingDonationFactory().createValid1(user2)
        cookingDonationService.create(donationWriteDto)
        val afterDonationBalance = cookingBalanceService.calculateBalanceForMe()
        assertEquals(newBalance - donationWriteDto.amount!!, afterDonationBalance)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

}