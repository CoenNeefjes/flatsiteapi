package nl.dirtya.flatsiteapi.model.cooking

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.cooking.service.CookingBalanceService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEnrolledService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.transaction.TransactionFactory
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionForCookingWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionBalanceService
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.EntityManager

class CookingEventServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val cookingEventService: CookingEventService,
        private val userService: UserService,
        private val cookingEnrolledService: CookingEnrolledService,
        private val transactionService: TransactionService,
        private val cookingBalanceService: CookingBalanceService,
        private val transactionBalanceService: TransactionBalanceService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val event = cookingEventService.create(LocalDate.now())

        entityManager.flush()
        entityManager.clear()

        val foundEvent = cookingEventService.findById(event.id)

        assertEquals(event.id, foundEvent.id)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingEventService.findById(-1) }
    }

    @Test
    fun whenFindingByIdOptionalCorrectBehaviour() {
        val event = cookingEventService.create(LocalDate.now())

        entityManager.flush()
        entityManager.clear()

        val foundEvent = cookingEventService.findByIdOptional(event.id)

        assertEquals(event.id, foundEvent!!.id)

        assertEquals(null, cookingEventService.findByIdOptional(-1))
    }

    @Test
    fun whenFindingByDateCorrectBehaviour() {
        val event = cookingEventService.create(LocalDate.now())

        entityManager.flush()
        entityManager.clear()

        val foundEvent = cookingEventService.findByDate(event.date)

        assertEquals(event.id, foundEvent!!.id)
    }

    @Test
    fun whenFindingByDateOrCreateCorrectBehaviour() {
        val date = LocalDate.of(1990, 1, 1)
        assertEquals(null, cookingEventService.findByDate(date))
        val event = cookingEventService.findByDateOrCreate(date)

        assertEquals(date, event.date)
    }

    @Test
    fun whenFindingInRangeCorrectBehaviour() {
        val date1 = LocalDate.now()
        val date2 = LocalDate.now().plusDays(1)
        val date3 = LocalDate.now().plusDays(3)

        val event1 = cookingEventService.create(date1)
        val event2 = cookingEventService.create(date2)
        val event3 = cookingEventService.create(date3)

        entityManager.flush()
        entityManager.clear()

        val result = cookingEventService.findAllInRange(LocalDate.now(), LocalDate.now().plusDays(4))
        assertEquals(4, result.size)

        assertEquals(event1, result[date1])
        assertEquals(event2, result[date2])
        assertNull(result[LocalDate.now().plusDays(2)])
        assertEquals(event3, result[date3])
    }

    @Test
    fun whenFindingInRangeEndDateIsBeforeStartDateThrowsException() {
        assertThrows<NotValidException> { cookingEventService.findAllInRange(LocalDate.now(), LocalDate.now().minusDays(1)) }
    }

    @Test
    fun whenFindingInRangeOrCreateCorrectBehaviour() {
        val date1 = LocalDate.now()
        val date2 = LocalDate.now().plusDays(1)
        val date3 = LocalDate.now().plusDays(3)

        cookingEventService.create(date1)
        cookingEventService.create(date2)
        cookingEventService.create(date3)

        entityManager.flush()
        entityManager.clear()

        val result = cookingEventService.findAllInRangeOrCreate(LocalDate.now(), LocalDate.now().plusDays(4))
        assertEquals(4, result.size)
    }

    @Test
    fun whenFindingInRangeOrCreateEndDateIsBeforeStartDateThrowsException() {
        assertThrows<NotValidException> { cookingEventService.findAllInRangeOrCreate(LocalDate.now(), LocalDate.now().minusDays(1)) }
    }

    @Test
    fun whenFindingWithCookAndTransactionCorrectBehaviour() {
        val date1 = LocalDate.now()
        val date2 = LocalDate.now().plusDays(1)
        val date3 = LocalDate.now().plusDays(3)

        val event = cookingEventService.create(date1)
        event.cook = user
        val event2 = cookingEventService.create(date2)
        event2.cook = user
        cookingEventService.updateEnrollments(event2, listOf(CookingEnrolledWriteDto(user.id, 1)))
        transactionService.createForCookingEvent(TransactionForCookingWriteDto(15.60.toBigDecimal()), event2.id)
        cookingEventService.create(date3)

        entityManager.flush()
        entityManager.clear()

        val results = cookingEventService.findWithCookAndTransaction()
        assertEquals(1, results.size)
        assertEquals(event2.id, results[0].id)
    }

    @Test
    fun whenFindingForgottenShowForgotten() {
        val today = LocalDate.now()
        val yesterday = today.minusDays(1)

        val event1 = cookingEventService.create(today)
        event1.cook = user
        val event2 = cookingEventService.create(yesterday)
        event2.cook = user

        entityManager.flush()
        entityManager.clear()

        val results = cookingEventService.findForgottenTransactions(today)
        assertEquals(2, results.size)
        assertEquals(event1.id, results[0].id)
        assertEquals(event2.id, results[1].id)
    }

    @Test
    fun whenFindingForgottenDontShowFilledInOrWithoutCook() {
        val yesterday = LocalDate.now().minusDays(1)

        val event = cookingEventService.create(yesterday)
        event.cook = user
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user.id, 1)))
        transactionService.createForCookingEvent(TransactionForCookingWriteDto(15.60.toBigDecimal()), event.id)
        cookingEventService.create(yesterday.minusDays(1))

        entityManager.flush()
        entityManager.clear()

        val results = cookingEventService.findForgottenTransactions(LocalDate.now())
        assertEquals(0, results.size)
    }

    @Test
    fun whenFindingForgottenDontReturnFuture() {
        val dateFuture = LocalDate.now().plusDays(3)

        val event = cookingEventService.create(dateFuture)
        event.cook = user

        entityManager.flush()
        entityManager.clear()

        val results = cookingEventService.findForgottenTransactions(LocalDate.now())
        assertEquals(0, results.size)
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val date = LocalDate.now()
        val event = cookingEventService.create(date)

        assertEquals(date, event.date)
        assertNull(event.cook)
        assertEquals(0, event.enrollments.size)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenCreatingDateAlreadyExistsThrowsException() {
        val date = LocalDate.now()
        cookingEventService.create(date)
        assertThrows<NotValidException> { cookingEventService.create(date) }
    }

    @Test
    fun whenUpdatingCorrectBehaviour() {
        val user2 = userService.create(UserFactory().createValid2())

        val event = cookingEventService.create(LocalDate.now())
        event.cook = user

        val totalEnrollments = cookingEnrolledService.findAll().size

        entityManager.persist(event)
        entityManager.flush()
        entityManager.clear()

        val updatedEvent = cookingEventService.update(event.id, CookingEventFactory().createValid1(user, user2))

        assertEquals(user, updatedEvent.cook)
        assertEquals(2, updatedEvent.enrollments.size)
        assertEquals(totalEnrollments+2, cookingEnrolledService.findAll().size)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenUpdatingActionIsCreatedCorrectly() {
        val user2 = userService.create(UserFactory().createValid2())

        val event = cookingEventService.create(LocalDate.now())
        event.cook = user

        entityManager.persist(event)
        entityManager.flush()
        entityManager.clear()

        val updatedEvent = cookingEventService.update(event.id, CookingEventFactory().createValid1(user, user2))

        assertEquals(1, updatedEvent.actions.size)
        assertEquals(user, updatedEvent.actions[0].user)
    }

    @Test
    fun whenUpdatingAndRemovingEnrollmentsThrowsException() {
        val user2 = userService.create(UserFactory().createValid2())

        val event = cookingEventService.create(LocalDate.now())
        event.cook = user

        val totalEnrollments = cookingEnrolledService.findAll().size

        cookingEnrolledService.create(user, 2, event)
        cookingEnrolledService.create(user2, 3, event)

        entityManager.flush()
        entityManager.clear()

        assertEquals(totalEnrollments+2, cookingEnrolledService.findAll().size)

        // Try to remove the enrollment of user2
        assertThrows<NotValidException> { cookingEventService.update(event.id, CookingEventFactory().createValid1(user)) }
    }

    @Test
    fun whenUpdatingNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingEventService.update(-1, CookingEventFactory().createValid1(user)) }
    }

    @Test
    fun whenUpdatingUserIsNotCookThrowsException() {
        val user2 = userService.create(UserFactory().createValid2())

        val event = cookingEventService.create(LocalDate.now())
        event.cook = user2

        entityManager.persist(event)
        entityManager.flush()
        entityManager.clear()

        assertThrows<ForbiddenException> { cookingEventService.update(event.id, CookingEventFactory().createValid1(user, user2)) }
    }

    @Test
    fun whenUpdatingDryersCorrectBehaviour() {
        val event = cookingEventService.create(LocalDate.now())

        cookingEventService.enrollAsCook(event.id)

        val user2 = userService.create(UserFactory().createValid2())
        cookingEventService.update(event.id, CookingEventFactory().createValid1(user, user2))

        entityManager.flush()
        entityManager.clear()

        val updated = cookingEventService.updateDryers(event.id, CookingEventFactory().updateCookingDryersValid(user2.id, null))

        assertEquals(updated.dryer1, user2)
        assertNull(updated.dryer2)
    }

    @Test
    fun whenUpdatingDryersUserIsNotCookThrowsException() {
        val event = cookingEventService.create(LocalDate.now())

        cookingEventService.enroll(event.id, 2)

        assertThrows<ForbiddenException> { cookingEventService.update(event.id, CookingEventFactory().createValid1(user)) }
    }

    @Test
    fun whenUpdatingDryersDryerNotFoundThrowsException() {
        val event = cookingEventService.create(LocalDate.now())

        cookingEventService.enrollAsCook(event.id)

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotFoundException> { cookingEventService.updateDryers(event.id, CookingEventFactory().updateCookingDryersValid(-1L, null)) }
    }

    @Test
    fun whenUpdatingDryersDryerIsNotEnrolledThrowsException() {
        val event = cookingEventService.create(LocalDate.now())

        cookingEventService.enrollAsCook(event.id)

        val user2 = userService.create(UserFactory().createValid2())

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotValidException> { cookingEventService.updateDryers(event.id, CookingEventFactory().updateCookingDryersValid(user2.id, null)) }
    }

    @Test
    fun whenUpdatingDryersDryerIsCookThrowsException() {
        val event = cookingEventService.create(LocalDate.now())

        cookingEventService.enrollAsCook(event.id)

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotValidException> { cookingEventService.updateDryers(event.id, CookingEventFactory().updateCookingDryersValid(user.id, null)) }
    }

    @Test
    fun whenUpdatingDryersSameUserAsDryersThrowsException() {
        val event = cookingEventService.create(LocalDate.now())

        cookingEventService.enrollAsCook(event.id)

        val user2 = userService.create(UserFactory().createValid2())
        cookingEventService.update(event.id, CookingEventFactory().createValid1(user, user2))

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotValidException> { cookingEventService.updateDryers(event.id, CookingEventFactory().updateCookingDryersValid(user2.id, user2.id)) }

    }

    @Test
    fun whenEnrollingCorrectBehaviour() {
        val enrollTimes = 4

        val event = cookingEventService.findByDateOrCreate(LocalDate.now())

        val totalEnrollments = cookingEnrolledService.findAll().size

        val updatedEvent = cookingEventService.enroll(event.id, enrollTimes)

        assertEquals(1, updatedEvent.enrollments.size)
        assertEquals(user, updatedEvent.enrollments.first().user)
        assertEquals(enrollTimes, updatedEvent.enrollments.first().amount)

        assertEquals(totalEnrollments+1, cookingEnrolledService.findAll().size)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenEnrollingActionIsCreatedCorrectly() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val updatedEvent = cookingEventService.enroll(event.id, 1)

        assertEquals(1, updatedEvent.actions.size)
        assertEquals(user, updatedEvent.actions[0].user)
    }

    @Test
    fun whenEnrollingWasAlreadyEnrolledCorrectBehaviour() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val totalEnrollments = cookingEnrolledService.findAll().size

        val afterFirstEnroll = cookingEventService.enroll(event.id, 2)
        assertEquals(2, afterFirstEnroll.enrollments.first().amount)

        val afterSecondEnroll = cookingEventService.enroll(event.id, 5)
        assertEquals(5, afterSecondEnroll.enrollments.first().amount)

        assertEquals(1, afterSecondEnroll.enrollments.size)
        assertEquals(totalEnrollments+1, cookingEnrolledService.findAll().size)
    }

    @Test
    fun whenUnEnrollingCorrectBehaviour() {
        val totalEnrollmentsBeforeEnrolling = cookingEnrolledService.findAll().size

        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enroll(event.id, 1)
        cookingEventService.enrollAsCook(event.id)

        val totalEnrollmentsAfterEnrolling = cookingEnrolledService.findAll().size
        assertEquals(totalEnrollmentsBeforeEnrolling + 1, totalEnrollmentsAfterEnrolling)

        entityManager.flush()
        entityManager.clear()

        val updatedEvent = cookingEventService.enroll(event.id, 0)

        entityManager.flush()
        entityManager.clear()

        val totalEnrollmentsAfterUnEnrolling = cookingEnrolledService.findAll().size

        // Make sure we are not cook anymore
        assertNull(updatedEvent.cook)

        // Make sure we are not enrolled, but the enrollment is still there
        assertEquals(1, updatedEvent.enrollments.size)
        assertEquals(totalEnrollmentsAfterEnrolling, totalEnrollmentsAfterUnEnrolling)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenUnEnrollingWithTransactionAddedCorrectBehaviour() {
        val currentBalance = transactionBalanceService.calculateBalanceForUser(user.id)

        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enroll(event.id, 1)

        val user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)
        cookingEventService.enrollAsCook(event.id)
        transactionService.createForCookingEvent(TransactionForCookingWriteDto(BigDecimal.TEN), event.id)

        entityManager.flush()
        entityManager.clear()

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
        cookingEventService.enroll(event.id, 0)

        entityManager.flush()
        entityManager.clear()

        val finalEvent = cookingEventService.findById(event.id)
        val finalBalance = transactionBalanceService.calculateBalanceForUser(user.id)

        assertEquals(2, finalEvent.enrollments.size)
        assertEquals(0, finalEvent.enrollments.filter { it.user == user }[0].amount)
        assertEquals(0, currentBalance.compareTo(finalBalance))
    }

    @Test
    fun whenUnEnrollingActionIsCreatedCorrectly() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enroll(event.id, 1)
        val updatedEvent = cookingEventService.enroll(event.id, 0)

        assertEquals(2, updatedEvent.actions.size)
        assertEquals(user, updatedEvent.actions[0].user)
        assertEquals(user, updatedEvent.actions[1].user)
    }

    @Test
    fun whenEnrollingNegativeAmountThrowsException() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        assertThrows<NotValidException> { cookingEventService.enroll(event.id, -1) }
    }

    @Test
    fun whenEnrollingCookingEventNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingEventService.enroll(-1, 3) }
    }

    @Test
    fun whenEnrollingInThePastThrowsException() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now().minusDays(5))
        assertThrows<ForbiddenException> { cookingEventService.enroll(event.id, 3) }
    }

    @Test
    fun whenEnrollingAsCookCorrectBehaviour() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val updatedEvent = cookingEventService.enrollAsCook(event.id)

        assertEquals(user, updatedEvent.cook)
        assertEquals(1, updatedEvent.enrollments.size)
        assertEquals(user, updatedEvent.enrollments.first().user)
        assertEquals(1, updatedEvent.enrollments.first().amount)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenEnrolledZeroTimesEnrollingAsCookCorrectBehaviour() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enroll(event.id, 0)
        val updatedEvent = cookingEventService.enrollAsCook(event.id)

        assertEquals(user, updatedEvent.cook)
        assertEquals(1, updatedEvent.enrollments.size)
        assertEquals(user, updatedEvent.enrollments.first().user)
        assertEquals(1, updatedEvent.enrollments.first().amount)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenEnrollingAsCookActionIsCreatedCorrectly() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val updatedEvent = cookingEventService.enrollAsCook(event.id)

        assertEquals(1, updatedEvent.actions.size)
        assertEquals(user, updatedEvent.actions[0].user)
    }

    @Test
    fun whenEnrollingAsCookUserWasAlreadyEnrolledCorrectBehaviour() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enroll(event.id, 5)
        val updatedEvent = cookingEventService.enrollAsCook(event.id)

        assertEquals(user, updatedEvent.cook)
        assertEquals(1, updatedEvent.enrollments.size)
        assertEquals(user, updatedEvent.enrollments.first().user)
        assertEquals(5, updatedEvent.enrollments.first().amount)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenEnrollingAsCookCookingEventNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingEventService.enrollAsCook(-1) }
    }

    @Test
    fun whenEnrollingAsCookThereIsAlreadyACookThrowsException() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val user2 = userService.create(UserFactory().createValid2())
        event.cook = user2

        entityManager.flush()
        entityManager.clear()

        assertThrows<ForbiddenException> { cookingEventService.enrollAsCook(event.id) }
    }

    @Test
    fun whenEnrollingAsCookThereIsAlreadyACookAlsoForAdminThrowsException() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val admin = userService.findByUsername("Admin")
        event.cook = user

        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)

        entityManager.flush()
        entityManager.clear()

        assertThrows<ForbiddenException> { cookingEventService.enrollAsCook(event.id) }
    }

    @Test
    fun whenUnEnrollingAsCookCorrectBehaviour() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val enrolledAsCook = cookingEventService.enrollAsCook(event.id)
        assertEquals(user, enrolledAsCook.cook)
        assertEquals(1, enrolledAsCook.enrollments.size)

        entityManager.flush()
        entityManager.clear()

        val unEnrolledAsCook = cookingEventService.unEnrollAsCook(event.id)
        assertNull(unEnrolledAsCook.cook)

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenUnEnrollingAlsoUnEnrollAsCook() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val eventEnrolledAsCook = cookingEventService.enrollAsCook(event.id)
        assertEquals(user, eventEnrolledAsCook.cook)
        assertEquals(1, eventEnrolledAsCook.enrollments.size)

        entityManager.flush()
        entityManager.clear()

        val updatedEvent = cookingEventService.enroll(event.id, 0)
        assertNull(updatedEvent.cook)
        assertEquals(0, updatedEvent.getTotalEnrollments())

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenUnEnrollingAsCookActionIsCreatedCorrectly() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(event.id)
        val updatedEvent = cookingEventService.unEnrollAsCook(event.id)

        assertEquals(2, updatedEvent.actions.size)
        assertEquals(user, updatedEvent.actions[0].user)
        assertEquals(user, updatedEvent.actions[1].user)
    }

    @Test
    fun whenUnEnrollingAsCookTransactionGetsDeleted() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        cookingEventService.enrollAsCook(event.id)

        val transaction = transactionService.createForCookingEvent(TransactionFactory().createValidForCookingEvent1(BigDecimal.TEN), event.id)

        cookingEventService.unEnrollAsCook(event.id)

        entityManager.flush()
        entityManager.clear()

        val foundEvent = cookingEventService.findById(event.id)
        assertNull(foundEvent.transaction)
        assertThrows<NotFoundException> { transactionService.findById(transaction.id) }

        cookingBalanceService.assertTotalBalanceIsZero()
    }

    @Test
    fun whenUnEnrollingAsCookCookingEventNotFoundThrowsException() {
        assertThrows<NotFoundException> { cookingEventService.unEnrollAsCook(-1) }
    }

    @Test
    fun whenUnEnrollingAsCookNoCookIsPresentThrowsException() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        assertThrows<NotValidException> { cookingEventService.unEnrollAsCook(event.id) }
    }

    @Test
    fun whenUnEnrollingAsCookOtherUserIsCookThrowsException() {
        val event = cookingEventService.findByDateOrCreate(LocalDate.now())
        val user2 = userService.create(UserFactory().createValid2())
        event.cook = user2

        entityManager.flush()
        entityManager.clear()

        assertThrows<ForbiddenException> { cookingEventService.unEnrollAsCook(event.id) }
    }
}