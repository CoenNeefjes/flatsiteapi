package nl.dirtya.flatsiteapi.model.beertype

import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import javax.persistence.EntityManager

class BeerTypeServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerTypeService: BeerTypeService
): BaseTest() {

    @Test
    fun whenFindingAllCorrectBehaviour() {
        val amount = beerTypeService.findAll().size
        beerTypeService.create(BeerTypeFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = beerTypeService.findAll().size
        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val beerType = beerTypeService.create(BeerTypeFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundBeerType = beerTypeService.findById(beerType.id)
        assertEquals(beerType, foundBeerType)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { beerTypeService.findById(-1) }
    }

    @Test
    fun whenFindingByNameCorrectBehaviour() {
        val beerType = beerTypeService.create(BeerTypeFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val foundBeerType = beerTypeService.findByName(beerType.name)
        assertEquals(beerType, foundBeerType)
    }

    @Test
    fun whenFindingByNameNotFoundReturnsNull() {
        val foundBeerType = beerTypeService.findByName("NotFound")
        assertNull(foundBeerType)
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val writeDto = BeerTypeFactory().createValid1()
        val beerType = beerTypeService.create(writeDto)

        assertEquals(writeDto.name, beerType.name)
        assertEquals(writeDto.beersPerCrate, beerType.beersPerCrate)
    }

    @Test
    fun whenCreatingNameIsNullThrowsException() {
        assertThrows<NotValidException> { beerTypeService.create(BeerTypeFactory().nameIsNull()) }
    }

    @Test
    fun whenCreatingNameIsEmptyThrowsException() {
        assertThrows<NotValidException> { beerTypeService.create(BeerTypeFactory().nameIsEmpty()) }
    }

    @Test
    fun whenCreatingBeersPerCrateIsNullThrowsException() {
        assertThrows<NotValidException> { beerTypeService.create(BeerTypeFactory().beersPerCrateIsNull()) }
    }

    @Test
    fun whenCreatingBeersPerCrateIsZeroThrowsException() {
        assertThrows<NotValidException> { beerTypeService.create(BeerTypeFactory().beersPerCrateIsZero()) }
    }

    @Test
    fun whenCreatingNameAlreadyInUseThrowsException() {
        beerTypeService.create(BeerTypeFactory().createValid1())
        assertThrows<NotValidException> { beerTypeService.create(BeerTypeFactory().createValid1()) }
    }

}