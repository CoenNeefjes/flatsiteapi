package nl.dirtya.flatsiteapi.model.beertype

import nl.dirtya.flatsiteapi.model.beertype.controller.dto.BeerTypeWriteDto

class BeerTypeFactory {

    fun createValid1(): BeerTypeWriteDto {
        return BeerTypeWriteDto("name1", 24)
    }

    fun createValid2(): BeerTypeWriteDto {
        return BeerTypeWriteDto("name2", 12)
    }

    fun nameIsNull(): BeerTypeWriteDto {
        return BeerTypeWriteDto(null, 24)
    }

    fun nameIsEmpty(): BeerTypeWriteDto {
        return BeerTypeWriteDto("", 24)
    }

    fun beersPerCrateIsNull(): BeerTypeWriteDto {
        return BeerTypeWriteDto("name1", null)
    }

    fun beersPerCrateIsZero(): BeerTypeWriteDto {
        return BeerTypeWriteDto("name1", 0)
    }

    fun updateValid1(): BeerTypeWriteDto {
        return BeerTypeWriteDto("nameUpdated1", 123)
    }
}