package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleChangeWifiWriteDto

class BeerScaleChangeWifiFactory {

    fun createValid1(): BeerScaleChangeWifiWriteDto {
        return BeerScaleChangeWifiWriteDto("myWifiNetwork1", "password1")
    }

    fun ssidIsNull(): BeerScaleChangeWifiWriteDto {
        return BeerScaleChangeWifiWriteDto(null, "password1")
    }
    fun ssidIsEmpty(): BeerScaleChangeWifiWriteDto {
        return BeerScaleChangeWifiWriteDto("", "password1")
    }

    fun passwordIsNull(): BeerScaleChangeWifiWriteDto {
        return BeerScaleChangeWifiWriteDto("myWifiNetwork1", null)
    }

    fun passwordIsEmpty(): BeerScaleChangeWifiWriteDto {
        return BeerScaleChangeWifiWriteDto("myWifiNetwork1", "")
    }

    fun updateValid1(): BeerScaleChangeWifiWriteDto {
        return BeerScaleChangeWifiWriteDto("myWifiNetwork2", "password2")
    }

}