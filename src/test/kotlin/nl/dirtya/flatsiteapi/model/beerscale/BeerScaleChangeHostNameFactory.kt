package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleChangeHostNameWriteDto

class BeerScaleChangeHostNameFactory {

    fun createValid1(): BeerScaleChangeHostNameWriteDto {
        return BeerScaleChangeHostNameWriteDto("http://domain1.nl")
    }

    fun hostNameIsNull(): BeerScaleChangeHostNameWriteDto {
        return BeerScaleChangeHostNameWriteDto(null)
    }

    fun hostNameIsEmpty(): BeerScaleChangeHostNameWriteDto {
        return BeerScaleChangeHostNameWriteDto("")
    }

    fun updateValid1(): BeerScaleChangeHostNameWriteDto {
        return BeerScaleChangeHostNameWriteDto("http://domain2.nl")
    }

}