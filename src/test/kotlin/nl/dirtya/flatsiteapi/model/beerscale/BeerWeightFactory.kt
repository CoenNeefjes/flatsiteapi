package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerWeightWriteDto

class BeerWeightFactory {

    fun createValid1(): BeerWeightWriteDto {
        return BeerWeightWriteDto("0.5800000000".toBigDecimal())
    }

    fun weightIsNull(): BeerWeightWriteDto {
        return BeerWeightWriteDto(null)
    }

    fun weightIsNegative(): BeerWeightWriteDto {
        return BeerWeightWriteDto("-1.0000000000".toBigDecimal())
    }

    fun updateValid1(): BeerWeightWriteDto {
        return BeerWeightWriteDto("0.5700000000".toBigDecimal())
    }

}