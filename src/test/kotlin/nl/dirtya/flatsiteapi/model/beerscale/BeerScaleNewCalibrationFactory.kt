package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleNewCalibrationWriteDto

class BeerScaleNewCalibrationFactory {

    fun createValid1(): BeerScaleNewCalibrationWriteDto {
        return BeerScaleNewCalibrationWriteDto(5f, 10)
    }

    fun calibrationWeightIsNull(): BeerScaleNewCalibrationWriteDto {
        return BeerScaleNewCalibrationWriteDto(null, 10)
    }

    fun calibrationWeightIsNegative(): BeerScaleNewCalibrationWriteDto {
        return BeerScaleNewCalibrationWriteDto(-1f, 10)
    }

    fun measuresNumberIsNull(): BeerScaleNewCalibrationWriteDto {
        return BeerScaleNewCalibrationWriteDto(5f, null)
    }

    fun measuresNumberIsZero(): BeerScaleNewCalibrationWriteDto {
        return BeerScaleNewCalibrationWriteDto(5f, 0)
    }

    fun updateValid1(): BeerScaleNewCalibrationWriteDto {
        return BeerScaleNewCalibrationWriteDto(10f, 5)
    }

}