package nl.dirtya.flatsiteapi.model.beerscale

import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.InternalServerException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.beerscale.service.BeerFridgeArduinoService
import nl.dirtya.flatsiteapi.model.beerscale.service.BeerScalePostRequestService
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.Response
import org.hibernate.annotations.NotFound
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertDoesNotThrow
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import javax.persistence.EntityManager

class BeerFridgeArduinoServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerFridgeArduinoService: BeerFridgeArduinoService
): BaseTest() {

    @MockBean lateinit var beerScalePostRequestService: BeerScalePostRequestService

    val okReponse = Response.Builder()
            .code(200)
            .request(Request.Builder().url("http://website.com").build())
            .protocol(Protocol.HTTP_1_1)
            .message("OK")
            .build()
    val internalServerErrorReponse = Response.Builder()
            .code(500)
            .request(Request.Builder().url("http://website.com").build())
            .protocol(Protocol.HTTP_1_1)
            .message("Internal Server Error")
            .build()

    @Test
    fun whenTaringScaleCorrectBehaviour() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertDoesNotThrow { beerFridgeArduinoService.scaleTare() }
    }

    @Test
    fun whenAddingCalibrationCorrectBehaviour() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertDoesNotThrow { beerFridgeArduinoService.scaleAddCalibration(BeerScaleNewCalibrationFactory().createValid1()) }
    }

    @Test
    fun whenAddingCalibrationWeightIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleAddCalibration(BeerScaleNewCalibrationFactory().calibrationWeightIsNull()) }
    }

    @Test
    fun whenAddingCalibrationWeightIsNegativeThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleAddCalibration(BeerScaleNewCalibrationFactory().calibrationWeightIsNegative()) }
    }

    @Test
    fun whenAddingCalibrationMeasuresIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleAddCalibration(BeerScaleNewCalibrationFactory().measuresNumberIsNull()) }
    }

    @Test
    fun whenAddingCalibrationMeasuresIsZeroThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleAddCalibration(BeerScaleNewCalibrationFactory().measuresNumberIsZero()) }
    }

    @Test
    fun whenChangingEndpointsCorrectBehaviour() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertDoesNotThrow { beerFridgeArduinoService.scaleChangeEndpoints(BeerScaleChangeEndpointsFactory().createValid1()) }
    }

    @Test
    fun whenChangingEndpointsMeasureEndpointIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeEndpoints(BeerScaleChangeEndpointsFactory().measureEndpointIsNull()) }
    }

    @Test
    fun whenChangingEndpointsMeasureEndpointIsEmptyThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeEndpoints(BeerScaleChangeEndpointsFactory().measureEndpointIsEmpty()) }
    }

    @Test
    fun whenChangingEndpointsCalibrationEndpointIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeEndpoints(BeerScaleChangeEndpointsFactory().calibrationEndpointIsNull()) }
    }

    @Test
    fun whenChangingEndpointsCalibrationEndpointIsEmptyThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeEndpoints(BeerScaleChangeEndpointsFactory().calibrationEndpointIsEmpty()) }
    }

    @Test
    fun whenChangingWiFiCorrectBehaviour() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertDoesNotThrow { beerFridgeArduinoService.scaleChangeWifi(BeerScaleChangeWifiFactory().createValid1()) }
    }

    @Test
    fun whenChangingWiFiSsidIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeWifi(BeerScaleChangeWifiFactory().ssidIsNull()) }
    }

    @Test
    fun whenChangingWiFiSsidIsEmptyThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeWifi(BeerScaleChangeWifiFactory().ssidIsEmpty()) }
    }

    @Test
    fun whenChangingWiFiPasswordIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeWifi(BeerScaleChangeWifiFactory().passwordIsNull()) }
    }

    @Test
    fun whenChangingWiFiPasswordIsEmptyThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeWifi(BeerScaleChangeWifiFactory().passwordIsEmpty()) }
    }

    @Test
    fun whenChangingApiCorrectBehaviour() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertDoesNotThrow { beerFridgeArduinoService.scaleChangeApi(BeerScaleChangeApiFactory().createValid1()) }
    }

    @Test
    fun whenChangingApiHostNameIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeApi(BeerScaleChangeApiFactory().hostNameIsNull()) }
    }

    @Test
    fun whenChangingApiHostNameIsEmptyThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeApi(BeerScaleChangeApiFactory().hostNameIsEmpty()) }
    }

    @Test
    fun whenChangingApiHostPortIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeApi(BeerScaleChangeApiFactory().portIsNull()) }
    }

    @Test
    fun whenChangingApiHostPortIsZeroThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeApi(BeerScaleChangeApiFactory().portIsZero()) }
    }

    @Test
    fun whenChangingDelayCorrectBehaviour() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertDoesNotThrow { beerFridgeArduinoService.scaleChangeDelay(BeerScaleChangeDelayFactory().createValid1()) }
    }

    @Test
    fun whenChangingDelayIsNullThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeDelay(BeerScaleChangeDelayFactory().delayIsNull()) }
    }

    @Test
    fun whenChangingDelayIsNegativeThrowsException() {
        whenever(beerScalePostRequestService.sendPostRequest(any(), any())).thenReturn(okReponse)
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        assertThrows <NotValidException> { beerFridgeArduinoService.scaleChangeDelay(BeerScaleChangeDelayFactory().delayIsNegative()) }
    }

    @Test
    fun whenChangingHostNameCorrectBehaviour() {
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val hostname = beerFridgeArduinoService.getHostname().hostname
        assertEquals("http://domain1.nl", hostname)
    }

    @Test
    fun whenHandlingStatusCodeCorrectBehaviour() {
        assertDoesNotThrow { beerFridgeArduinoService.correctlyHandleStatusCode(okReponse) }
        assertThrows <InternalServerException> { beerFridgeArduinoService.correctlyHandleStatusCode(internalServerErrorReponse) }
    }

    @Test
    fun whenGettingHostNameCorrectBehaviour() {
        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().createValid1())
        val hostname1 = beerFridgeArduinoService.getHostname().hostname
        assertEquals("http://domain1.nl", hostname1)

        beerFridgeArduinoService.scaleChangeHostName(BeerScaleChangeHostNameFactory().updateValid1())
        val hostname2 = beerFridgeArduinoService.getHostname().hostname
        assertEquals("http://domain2.nl", hostname2)
    }

    @Test
    fun whenGettingHostNameNotFoundThrowsException() {
        assertThrows <NotFoundException> { beerFridgeArduinoService.getHostname() }
    }

}