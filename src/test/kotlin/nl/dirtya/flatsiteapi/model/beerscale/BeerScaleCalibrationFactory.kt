package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleCalibrationWriteDto

class BeerScaleCalibrationFactory {

    fun createValid1(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(700000, "0.00".toBigDecimal())
    }

    fun updateValid1(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(800000, "1.00".toBigDecimal())
    }

    fun createValid2(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(600000, "0.00".toBigDecimal())
    }

    fun updateValid2(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(800000, "5.00".toBigDecimal())
    }

    fun averageReadingIsNull(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(null, "0.00".toBigDecimal())
    }

    fun weightIsNull(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(700000, null)
    }

    fun weightIsNegative(): BeerScaleCalibrationWriteDto {
        return BeerScaleCalibrationWriteDto(700000, "-1.00".toBigDecimal())
    }



}