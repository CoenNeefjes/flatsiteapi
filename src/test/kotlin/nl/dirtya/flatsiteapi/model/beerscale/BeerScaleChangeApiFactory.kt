package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleChangeApiWriteDto

class BeerScaleChangeApiFactory {

    fun createValid1(): BeerScaleChangeApiWriteDto {
        return BeerScaleChangeApiWriteDto("domain1.nl", 8080)
    }

    fun hostNameIsNull(): BeerScaleChangeApiWriteDto {
        return BeerScaleChangeApiWriteDto(null, 8080)
    }
    fun hostNameIsEmpty(): BeerScaleChangeApiWriteDto {
        return BeerScaleChangeApiWriteDto("", 8080)
    }

    fun portIsNull(): BeerScaleChangeApiWriteDto {
        return BeerScaleChangeApiWriteDto("domain1.nl", null)
    }

    fun portIsZero(): BeerScaleChangeApiWriteDto {
        return BeerScaleChangeApiWriteDto("domain1.nl", 0)
    }

    fun updateValid1(): BeerScaleChangeApiWriteDto {
        return BeerScaleChangeApiWriteDto("domain2.nl", 8888)
    }

}