package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleChangeEndpointsWriteDto

class BeerScaleChangeEndpointsFactory {

    fun createValid1(): BeerScaleChangeEndpointsWriteDto {
        return BeerScaleChangeEndpointsWriteDto("/api/measure-endpoint1", "/api/calibration-endpoint1")
    }

    fun measureEndpointIsNull(): BeerScaleChangeEndpointsWriteDto {
        return BeerScaleChangeEndpointsWriteDto(null, "/api/calibration-endpoint1")
    }

    fun measureEndpointIsEmpty(): BeerScaleChangeEndpointsWriteDto {
        return BeerScaleChangeEndpointsWriteDto("", "/api/calibration-endpoint1")
    }

    fun calibrationEndpointIsNull(): BeerScaleChangeEndpointsWriteDto {
        return BeerScaleChangeEndpointsWriteDto("/api/measure-endpoint1", null)
    }

    fun calibrationEndpointIsEmpty(): BeerScaleChangeEndpointsWriteDto {
        return BeerScaleChangeEndpointsWriteDto("/api/measure-endpoint1", "")
    }

    fun updateValid1(): BeerScaleChangeEndpointsWriteDto {
        return BeerScaleChangeEndpointsWriteDto("/api/measure-endpoint2", "/api/calibration-endpoint2")
    }

}