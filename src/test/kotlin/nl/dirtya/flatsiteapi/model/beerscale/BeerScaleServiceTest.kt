package nl.dirtya.flatsiteapi.model.beerscale

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.beerscale.service.BeerScaleService
import nl.dirtya.flatsiteapi.model.notification.service.NotificationService
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.controller.dto.AdminUserWriteDto
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.math.RoundingMode
import javax.persistence.EntityManager

class BeerScaleServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerScaleService: BeerScaleService,
        private val userService: UserService,
        private val notificationService: NotificationService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    @Test
    fun whenFindingOneMeasurementCorrectBehaviour() {
        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().createValid1())
        val reading1 = beerScaleService.findOneMeasurement().reading
        assertEquals(700000, reading1)

        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().updateValid1())
        val reading2 = beerScaleService.findOneMeasurement().reading
        assertEquals(800000,reading2)
    }

    @Test
    fun whenFindingOneMeasurementNotFoundThrowsException() {
        assertThrows<NotFoundException> {beerScaleService.findOneMeasurement()}
    }

    @Test
    fun whenFindingOneBeerWeightCorrectBehaviour() {
        beerScaleService.replaceBeerWeight(BeerWeightFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val weight1 = beerScaleService.findOneBeerWeight().weight
        assertEquals("0.58".toBigDecimal(), weight1.setScale(2, RoundingMode.HALF_UP))

        beerScaleService.replaceBeerWeight(BeerWeightFactory().updateValid1())

        entityManager.flush()
        entityManager.clear()

        val weight2 = beerScaleService.findOneBeerWeight().weight
        assertEquals("0.57".toBigDecimal(), weight2.setScale(2, RoundingMode.HALF_UP))
    }

    @Test
    fun whenFindingOneBeerWeightNotFoundThrowsException() {
        assertThrows<NotFoundException> {beerScaleService.findOneBeerWeight()}
    }

    @Test
    fun whenFindingOneRegressionCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())

        entityManager.flush()
        entityManager.clear()

        val slope1 = beerScaleService.findOneRegression().slope
        val intercept1 = beerScaleService.findOneRegression().intercept
        assertEquals("0.00001".toBigDecimal(),slope1.setScale(5, RoundingMode.HALF_UP))
        assertEquals("-7".toBigDecimal(),intercept1.setScale(0, RoundingMode.HALF_UP))

        beerScaleService.removeAllCalibrationPoints()
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid2())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid2())

        entityManager.flush()
        entityManager.clear()

        val slope2 = beerScaleService.findOneRegression().slope
        val intercept2 = beerScaleService.findOneRegression().intercept
        assertEquals("0.00003".toBigDecimal(),slope2.setScale(5, RoundingMode.HALF_UP))
        assertEquals("-15".toBigDecimal(),intercept2.setScale(0, RoundingMode.HALF_UP))
    }

    @Test
    fun whenFindingOneRegressionOneCalibrationPointThrowsNotFoundException() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotFoundException> {beerScaleService.findOneRegression()}
    }

    @Test
    fun whenFindingOneRegressionNotFoundThrowsException() {
        assertThrows<NotFoundException> {beerScaleService.findOneRegression()}
    }

    @Test
    fun whenGettingReadingCorrectBehaviour() {
        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().createValid1())
        val reading = beerScaleService.getReading()
        assertEquals(700000, reading)
    }

    @Test
    fun whenGettingReadingKgCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())
        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val measuredKg = beerScaleService.getReadingKg()
        val expectedKg = "0.00001".toBigDecimal() * 700000.toBigDecimal() + "-7".toBigDecimal()
        assertEquals(expectedKg, measuredKg)
    }

    @Test
    fun whenGettingReadingKgNotValidException() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotValidException> { beerScaleService.getReadingKg()}
    }

    @Test
    fun whenGettingReadingBeersCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())
        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().createValid1())
        beerScaleService.replaceBeerWeight(BeerWeightFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val measuredBeers = beerScaleService.getReadingBeers()
        val expectedBeers = ("0.00001".toBigDecimal() * 700000.toBigDecimal() + "-7".toBigDecimal()) / beerScaleService.getBeerWeight()
        assertEquals(expectedBeers.setScale(0, RoundingMode.HALF_UP).intValueExact(), measuredBeers)
    }

    @Test
    fun whenGettingBeerWeightCorrectBehaviour() {
        beerScaleService.replaceBeerWeight(BeerWeightFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val weight = beerScaleService.findOneBeerWeight().weight
        assertEquals("0.58".toBigDecimal(), weight.setScale(2, RoundingMode.HALF_UP))
    }

    @Test
    fun whenReplacingBeerWeightCorrectBehaviour() {
        val writeDto = BeerWeightFactory().createValid1()
        beerScaleService.replaceBeerWeight(writeDto)

        entityManager.flush()
        entityManager.clear()

        val beerWeight = beerScaleService.findOneBeerWeight()
        assertEquals(writeDto.weight, beerWeight.weight)
    }

    @Test
    fun whenReplacingBeerWeightIsNullThrowsException() {
        assertThrows<NotValidException> { beerScaleService.replaceBeerWeight(BeerWeightFactory().weightIsNull()) }
    }

    @Test
    fun whenReplacingBeerWeightIsNegativeThrowsException() {
        assertThrows<NotValidException> { beerScaleService.replaceBeerWeight(BeerWeightFactory().weightIsNegative()) }
    }

    @Test
    fun whenChangingBeerScaleRegressionCorrectBehaviour() {
        val writeDto = BeerScaleRegressionFactory().createValid1()
        beerScaleService.changeBeerScaleRegression(writeDto)

        entityManager.flush()
        entityManager.clear()

        val beerRegression = beerScaleService.findOneRegression()
        assertEquals(writeDto.slope, beerRegression.slope)
        assertEquals(writeDto.intercept, beerRegression.intercept)

    }

    @Test
    fun whenChangingBeerScaleRegressionSlopeIsNullThrowsException() {
        assertThrows<NotValidException> { beerScaleService.changeBeerScaleRegression(BeerScaleRegressionFactory().slopeIsNull()) }
    }

    @Test
    fun whenChangingBeerScaleRegressionInterceptIsNullThrowsException() {
        assertThrows<NotValidException> { beerScaleService.changeBeerScaleRegression(BeerScaleRegressionFactory().interceptIsNull()) }
    }

    @Test
    fun whenReplacingBeerScaleRegressionCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())

        entityManager.flush()
        entityManager.clear()

        val slope = beerScaleService.findOneRegression().slope
        val intercept = beerScaleService.findOneRegression().intercept
        assertEquals("0.00001".toBigDecimal(),slope.setScale(5, RoundingMode.HALF_UP))
        assertEquals("-7".toBigDecimal(),intercept.setScale(0, RoundingMode.HALF_UP))

    }

    @Test
    fun whenReplacingMeasurementCorrectBehaviour() {
        val writeDto = BeerScaleMeasurementFactory().createValid1()
        beerScaleService.replaceMeasurement(writeDto)

        entityManager.flush()
        entityManager.clear()

        val beerMeasurement = beerScaleService.findOneMeasurement()
        assertEquals(writeDto.reading, beerMeasurement.reading)

    }

    @Test
    fun creatingNotificationIfBeersLowerThanCorrectBehaviour() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(userService.findByUsername("Admin").id)

        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())
        beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().createValid1())
        beerScaleService.replaceBeerWeight(BeerWeightFactory().createValid1())
        userService.adminUpdate(userService.findByUsername("Admin").id, AdminUserWriteDto("Admin", "password",listOf(Roles.USER, Roles.ADMIN, Roles.FEUT), true))

        entityManager.flush()
        entityManager.clear()

        beerScaleService.createNotificationIfBeersLowerThanThreshold()
        assertTrue(notificationService.findByMe().size == 1)
    }

    @Test
    fun whenReplacingMeasurementReadingIsNullThrowsException() {
        assertThrows<NotValidException> { beerScaleService.replaceMeasurement(BeerScaleMeasurementFactory().readingIsNull()) }
    }

    @Test
    fun whenGettingAllCalibrationPointsCorrectBehaviour() {
        val amount = beerScaleService.getAllCalibrationPoints().size
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = beerScaleService.getAllCalibrationPoints().size
        assertEquals(amount+1, updatedAmount)
    }

    @Test
    fun whenRemovingAllCalibrationPointsCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())

        entityManager.flush()
        entityManager.clear()

        val amount = beerScaleService.getAllCalibrationPoints().size

        beerScaleService.removeAllCalibrationPoints()

        entityManager.flush()
        entityManager.clear()

        val updatedAmount = beerScaleService.getAllCalibrationPoints().size
        assertEquals(2, amount)
        assertEquals(0, updatedAmount)
    }

    @Test
    fun whenAddingCalibrationPointCorrectBehaviour() {
        val writeDto = BeerScaleCalibrationFactory().createValid1()
        beerScaleService.addCalibrationPoint(writeDto)

        entityManager.flush()
        entityManager.clear()

        val firstCalibrationPoint = beerScaleService.getAllCalibrationPoints()[0]
        assertEquals(writeDto.averagereading, firstCalibrationPoint.averagereading)
        assertEquals(writeDto.weight, firstCalibrationPoint.weight)

    }

    @Test
    fun whenAddingCalibrationPointAverageReadingIsNullThrowsException() {
        assertThrows<NotValidException> { beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().averageReadingIsNull()) }
    }

    @Test
    fun whenAddingCalibrationPointWeightIsNullThrowsException() {
        assertThrows<NotValidException> { beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().weightIsNull()) }
    }

    @Test
    fun whenAddingCalibrationPointWeightIsNegativeThrowsException() {
        assertThrows<NotValidException> { beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().weightIsNegative()) }
    }

    @Test
    fun whenCalculatingLinearRegressionCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())

        entityManager.flush()
        entityManager.clear()

        val regression = beerScaleService.calculateLinearRegression()
        val slope = regression.first
        val intercept = regression.second
        assertEquals("0.00001".toBigDecimal(),slope.setScale(5, RoundingMode.HALF_UP))
        assertEquals("-7".toBigDecimal(),intercept.setScale(0, RoundingMode.HALF_UP))
    }

    @Test
    fun whenPredictingWeightCorrectBehaviour() {
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().createValid1())
        beerScaleService.addCalibrationPoint(BeerScaleCalibrationFactory().updateValid1())

        entityManager.flush()
        entityManager.clear()

        val regression = beerScaleService.calculateLinearRegression()
        val weight = beerScaleService.predictWeightWithLinearRegression(regression, 700000)

        assertEquals(regression.first * 700000.toBigDecimal() + regression.second,weight)
    }

}