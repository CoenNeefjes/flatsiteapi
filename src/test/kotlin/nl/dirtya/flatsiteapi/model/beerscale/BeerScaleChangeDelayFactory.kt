package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleChangeDelayWriteDto

class BeerScaleChangeDelayFactory {

    fun createValid1(): BeerScaleChangeDelayWriteDto {
        return BeerScaleChangeDelayWriteDto(1000)
    }

    fun delayIsNull(): BeerScaleChangeDelayWriteDto {
        return BeerScaleChangeDelayWriteDto(null)
    }

    fun delayIsNegative(): BeerScaleChangeDelayWriteDto {
        return BeerScaleChangeDelayWriteDto(-100)
    }

    fun updateValid1(): BeerScaleChangeDelayWriteDto {
        return BeerScaleChangeDelayWriteDto(2000)
    }

}