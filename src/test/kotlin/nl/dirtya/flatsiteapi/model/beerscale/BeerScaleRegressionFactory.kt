package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleRegressionWriteDto

class BeerScaleRegressionFactory {

    fun createValid1(): BeerScaleRegressionWriteDto {
        return BeerScaleRegressionWriteDto("500.00000000000000000000000000000000000000000000000000".toBigDecimal(), "20.00000000000000000000000000000000000000000000000000".toBigDecimal())
    }

    fun slopeIsNull(): BeerScaleRegressionWriteDto {
        return BeerScaleRegressionWriteDto(null, "20.00000000000000000000000000000000000000000000000000".toBigDecimal())
    }

    fun interceptIsNull(): BeerScaleRegressionWriteDto {
        return BeerScaleRegressionWriteDto("500.00000000000000000000000000000000000000000000000000".toBigDecimal(), null)
    }

    fun updateValid1(): BeerScaleRegressionWriteDto {
        return BeerScaleRegressionWriteDto("300.00000000000000000000000000000000000000000000000000".toBigDecimal(), "100.00000000000000000000000000000000000000000000000000".toBigDecimal())
    }

}