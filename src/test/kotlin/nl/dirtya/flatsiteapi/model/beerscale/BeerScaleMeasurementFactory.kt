package nl.dirtya.flatsiteapi.model.beerscale

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.BeerScaleMeasurementWriteDto

class BeerScaleMeasurementFactory {

    fun createValid1(): BeerScaleMeasurementWriteDto {
        return BeerScaleMeasurementWriteDto(700000)
    }

    fun readingIsNull(): BeerScaleMeasurementWriteDto {
        return BeerScaleMeasurementWriteDto(null)
    }

    fun updateValid1(): BeerScaleMeasurementWriteDto {
        return BeerScaleMeasurementWriteDto(800000)
    }

}