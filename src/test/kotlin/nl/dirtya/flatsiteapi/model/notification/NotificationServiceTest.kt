package nl.dirtya.flatsiteapi.model.notification

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.notification.api.NotificationType
import nl.dirtya.flatsiteapi.model.notification.service.NotificationService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import javax.persistence.EntityManager

class NotificationServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val notificationService: NotificationService,
        private val userService: UserService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user: User

    @BeforeEach
    fun setup() {
        user = userService.create(UserFactory().createValid1())
    }

    @Test
    fun whenFindingByIdCorrectBehaviour() {
        val notification = notificationService.create(user, NotificationType.BEER_FRIDGE, "test")

        entityManager.flush()
        entityManager.clear()

        val found = notificationService.findById(notification.id)
        assertEquals(notification, found)
    }

    @Test
    fun whenFindingByIdNotFoundThrowsException() {
        assertThrows<NotFoundException> { notificationService.findById(-1L) }
    }

    @Test
    fun whenFindingByUserAndTypeCorrectBehaviour() {
        val notification = notificationService.create(user, NotificationType.BEER_FRIDGE, "test")

        entityManager.flush()
        entityManager.clear()

        val found = notificationService.findByUserAndType(notification.user, notification.type)
        assertEquals(1, found.size)
        assertTrue(found.contains(notification))
    }

    @Test
    fun whenFindingByMeCorrectBehaviour() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)

        val notification = notificationService.create(userService.getCurrentUser(), NotificationType.BEER_FRIDGE, "test")

        entityManager.flush()
        entityManager.clear()

        val found = notificationService.findByMe()
        assertEquals(1, found.size)
        assertTrue(found.contains(notification))
    }

    @Test
    fun whenFindingByMeAndDeleteCorrectBehaviour() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)

        val notification = notificationService.create(userService.getCurrentUser(), NotificationType.BEER_FRIDGE, "test")

        entityManager.flush()
        entityManager.clear()

        val found = notificationService.findByMeAndDelete()
        assertEquals(1, found.size)
        assertEquals(notification.id, found[0].id)

        entityManager.flush()
        entityManager.clear()

        val foundAfterDelete = notificationService.findByMe()
        assertTrue(foundAfterDelete.isEmpty())
    }

    @Test
    fun whenCreatingCorrectBehaviour() {
        val creatingFor = user
        val type = NotificationType.BEER_FRIDGE
        val message = "Bier bijvullen kut"
        val notification = notificationService.create(creatingFor, type, message)

        assertEquals(creatingFor, notification.user)
        assertEquals(type, notification.type)
        assertEquals(message, notification.message)
    }

    @Test
    fun whenCreatingSingletonCorrectBehaviour() {
        val notification = notificationService.createSingleton(user, NotificationType.BEER_FRIDGE, "test")

        assertNotNull(notification)

        val secondNotification = notificationService.createSingleton(user, NotificationType.BEER_FRIDGE, "test2")

        assertNull(secondNotification)
    }

    @Test
    fun whenDeletingCorrectBehaviour() {
        val notification = notificationService.create(user, NotificationType.BEER_FRIDGE, "test")

        entityManager.flush()
        entityManager.clear()

        notificationService.delete(notification)

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotFoundException> { notificationService.findById(notification.id) }
    }

    @Test
    fun whenDeletingAllCorrectBehaviour() {
        val notification = notificationService.create(user, NotificationType.BEER_FRIDGE, "test")
        val notification2 = notificationService.create(user, NotificationType.BEER_FRIDGE, "test2")

        entityManager.flush()
        entityManager.clear()

        notificationService.deleteAll(listOf(notification, notification2))

        entityManager.flush()
        entityManager.clear()

        assertThrows<NotFoundException> { notificationService.findById(notification.id) }
        assertThrows<NotFoundException> { notificationService.findById(notification2.id) }
    }

}