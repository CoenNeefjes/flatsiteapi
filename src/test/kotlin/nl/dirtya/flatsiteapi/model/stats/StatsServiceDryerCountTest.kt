package nl.dirtya.flatsiteapi.model.stats

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.CookingEventFactory
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.stats.service.StatsService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class StatsServiceDryerCountTest
@Autowired constructor(
    private val entityManager: EntityManager,
    private val statsService: StatsService,
    private val userService: UserService,
    private val cookingEventService: CookingEventService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun correctBehaviourWithFirstDryer() {
        val event = cookingEventService.create(LocalDate.now())
        event.cook = user1
        val user2 = userService.create(UserFactory().createValid2())
        cookingEventService.update(event.id, CookingEventFactory().createValid1(user1, user2))

        cookingEventService.updateDryers(event.id, CookingEventFactory().updateCookingDryersValid(user2.id, null))

        entityManager.flush()
        entityManager.clear()

        val dryerCount = statsService.getDryerCount()
        assertEquals(userService.countAll().toInt(), dryerCount.size)
        assertEquals(0, dryerCount.find { u -> u.user.id == user1.id }!!.amount)
        assertEquals(1, dryerCount.find { u -> u.user.id == user2.id }!!.amount)
        assertEquals(0, dryerCount.find { u -> u.user.id == user1.id }!!.eatAmount)
        assertEquals(1, dryerCount.find { u -> u.user.id == user2.id }!!.eatAmount)
        assertEquals(0.0, dryerCount.find { u -> u.user.id == user1.id }!!.fraction)
        assertEquals(1.0, dryerCount.find { u -> u.user.id == user2.id }!!.fraction)
    }

    @Test
    fun correctBehaviourWithAllDryers() {
        val user2 = userService.create(UserFactory().createValid2())
        val user3 = userService.create(UserFactory().createValid3())

        val event1 = cookingEventService.create(LocalDate.now())
        event1.cook = user1
        cookingEventService.update(event1.id, CookingEventFactory().createValid1(user1, user2, user3))
        cookingEventService.updateDryers(event1.id, CookingEventFactory().updateCookingDryersValid(user2.id, user3.id))

        val event2 = cookingEventService.create(LocalDate.now().plusDays(1))
        event2.cook = user1
        cookingEventService.update(event2.id, CookingEventFactory().createValid1(user1, user2, user3))
        cookingEventService.updateDryers(event2.id, CookingEventFactory().updateCookingDryersValid(user3.id, user2.id))

        val event3 = cookingEventService.create(LocalDate.now().plusDays(2))
        event3.cook = user1
        cookingEventService.update(event3.id, CookingEventFactory().createValid1(user1, user2, user3))
        cookingEventService.updateDryers(event3.id, CookingEventFactory().updateCookingDryersValid(user2.id, null))

        val event4 = cookingEventService.create(LocalDate.now().plusDays(3))
        event4.cook = user1
        cookingEventService.update(event4.id, CookingEventFactory().createValid1(user1, user2))
        cookingEventService.updateDryers(event4.id, CookingEventFactory().updateCookingDryersValid(user2.id, null))

        entityManager.flush()
        entityManager.clear()

        val dryerCount = statsService.getDryerCount()
        assertEquals(userService.countAll().toInt(), dryerCount.size)
        assertEquals(0, dryerCount.find { u -> u.user.id == user1.id }!!.amount)
        assertEquals(4, dryerCount.find { u -> u.user.id == user2.id }!!.amount)
        assertEquals(2, dryerCount.find { u -> u.user.id == user3.id }!!.amount)
        assertEquals(0, dryerCount.find { u -> u.user.id == user1.id }!!.eatAmount)
        assertEquals(4, dryerCount.find { u -> u.user.id == user2.id }!!.eatAmount)
        assertEquals(3, dryerCount.find { u -> u.user.id == user3.id }!!.eatAmount)
        assertEquals(0.0, dryerCount.find { u -> u.user.id == user1.id }!!.fraction)
        assertEquals(1.0, dryerCount.find { u -> u.user.id == user2.id }!!.fraction)
        assertEquals(2.toDouble() / 3.toDouble(), dryerCount.find { u -> u.user.id == user3.id }!!.fraction)
    }
}