package nl.dirtya.flatsiteapi.model.stats

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEnrolledService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.stats.service.StatsService
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionForCookingWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class StatsServiceForgottenTransactionsTest
@Autowired constructor(
    private val entityManager: EntityManager,
    private val statsService: StatsService,
    private val userService: UserService,
    private val cookingEventService: CookingEventService,
    private val cookingEnrolledService: CookingEnrolledService,
    private val transactionService: TransactionService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun noForgottenTransactionsReturnsEmptyList() {
        val event = cookingEventService.create(LocalDate.now())
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 1)))

        transactionService.createForCookingEvent(TransactionForCookingWriteDto(15.60.toBigDecimal()), event.id)

        entityManager.flush()
        entityManager.clear()

        val forgottenTransactions = statsService.getForgottenTransactions()

        assertEquals(0, forgottenTransactions.size)
    }

    @Test
    fun doesNotShowTransactionsOfTodayOrInFuture() {
        val now = LocalDate.now()
        val event = cookingEventService.create(now)
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        val event2 = cookingEventService.create(now.plusDays(1))
        event2.cook = user1
        cookingEventService.updateEnrollments(event2, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        entityManager.flush()
        entityManager.clear()

        val forgottenTransactions = statsService.getForgottenTransactions()

        assertEquals(0, forgottenTransactions.size)
    }

    @Test
    fun showsForgottenTransactionsOfSameCook() {
        val now = LocalDate.now().minusDays(10)
        val event = cookingEventService.create(now)
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        val event2 = cookingEventService.create(now.plusDays(1))
        event2.cook = user1
        cookingEventService.updateEnrollments(event2, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        entityManager.flush()
        entityManager.clear()

        val forgottenTransactions = statsService.getForgottenTransactions()

        assertEquals(2, forgottenTransactions.size)
        assertEquals(user1.id, forgottenTransactions[0].cook.id)
        assertEquals(now, forgottenTransactions[0].date)
        assertEquals(user1.id, forgottenTransactions[1].cook.id)
        assertEquals(now.plusDays(1), forgottenTransactions[1].date)
    }

    @Test
    fun showsForgottenTransactionsOfMultipleCooks() {
        val now = LocalDate.now().minusDays(10)
        val event = cookingEventService.create(now)
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        val user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)
        val event2 = cookingEventService.create(now.plusDays(1))
        event2.cook = user2
        cookingEventService.updateEnrollments(event2, listOf(CookingEnrolledWriteDto(user2.id, 6)))

        entityManager.flush()
        entityManager.clear()

        val forgottenTransactions = statsService.getForgottenTransactions()

        assertEquals(2, forgottenTransactions.size)
        assertEquals(user1.id, forgottenTransactions[0].cook.id)
        assertEquals(now, forgottenTransactions[0].date)
        assertEquals(user2.id, forgottenTransactions[1].cook.id)
        assertEquals(now.plusDays(1), forgottenTransactions[1].date)
    }
}