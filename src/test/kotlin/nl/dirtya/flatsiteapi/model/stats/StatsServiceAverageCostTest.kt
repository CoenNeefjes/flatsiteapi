package nl.dirtya.flatsiteapi.model.stats

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.stats.service.StatsService
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionForCookingWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import java.time.LocalDate
import javax.persistence.EntityManager

class StatsServiceAverageCostTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val statsService: StatsService,
        private val userService: UserService,
        private val cookingEventService: CookingEventService,
        private val transactionService: TransactionService
): BaseTest() {

    @MockBean lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun correctBehaviourWithOneCookingEvent() {
        val event = cookingEventService.create(LocalDate.now())
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 1)))

        transactionService.createForCookingEvent(TransactionForCookingWriteDto(15.60.toBigDecimal()), event.id)

        entityManager.flush()
        entityManager.clear()

        val averagePrices = statsService.getAverageCookingPriceForAllUsers()

        assertEquals(1, averagePrices.size)
        assertEquals(user1.id, averagePrices[0].user.id)
        assertEquals(1, averagePrices[0].amount)
        assertEquals(0, 15.60.toBigDecimal().compareTo(averagePrices[0].price))
        assertEquals(0, 0.toBigDecimal().compareTo(averagePrices[0].stdDev))
    }

    @Test
    fun correctBehaviourWithMultipleCookingEvents() {
        val event = cookingEventService.create(LocalDate.now())
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        val event2 = cookingEventService.create(LocalDate.now().plusDays(1))
        event2.cook = user1
        cookingEventService.updateEnrollments(event2, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        transactionService.createForCookingEvent(TransactionForCookingWriteDto(15.60.toBigDecimal()), event.id)
        transactionService.createForCookingEvent(TransactionForCookingWriteDto(19.80.toBigDecimal()), event2.id)

        entityManager.flush()
        entityManager.clear()

        val averagePrices = statsService.getAverageCookingPriceForAllUsers()

        assertEquals(2, averagePrices[0].amount)
        assertEquals(0, 2.95.toBigDecimal().compareTo(averagePrices[0].price))
        assertEquals(0, 0.35.toBigDecimal().compareTo(averagePrices[0].stdDev))
    }

    @Test
    fun personWithoutCookingTransactionsIsNotInAverageCost() {
        val event = cookingEventService.create(LocalDate.now())
        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 6)))

        entityManager.flush()
        entityManager.clear()

        val averagePrices = statsService.getAverageCookingPriceForAllUsers()

        assertEquals(0, averagePrices.count())
    }

    @Test
    fun correctBehaviourForMultipleCooks() {
        val event = cookingEventService.create(LocalDate.now())
        val event2 = cookingEventService.create(LocalDate.now().plusDays(1))

        event.cook = user1
        cookingEventService.updateEnrollments(event, listOf(CookingEnrolledWriteDto(user1.id, 6)))
        transactionService.createForCookingEvent(TransactionForCookingWriteDto(15.60.toBigDecimal()), event.id)

        val user2 = userService.create(UserFactory().createValid2())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)
        event2.cook = user2
        cookingEventService.updateEnrollments(event2, listOf(CookingEnrolledWriteDto(user2.id, 6)))
        transactionService.createForCookingEvent(TransactionForCookingWriteDto(19.80.toBigDecimal()), event2.id)

        entityManager.flush()
        entityManager.clear()

        val averagePrices = statsService.getAverageCookingPriceForAllUsers()

        assertEquals(user1.id, averagePrices[0].user.id)
        assertEquals(user2.id, averagePrices[1].user.id)
        assertEquals(1, averagePrices[0].amount)
        assertEquals(1, averagePrices[1].amount)
        assertEquals(0, 2.60.toBigDecimal().compareTo(averagePrices[0].price))
        assertEquals(0, 3.30.toBigDecimal().compareTo(averagePrices[1].price))
        assertEquals(0, 0.toBigDecimal().compareTo(averagePrices[0].stdDev))
        assertEquals(0, 0.toBigDecimal().compareTo(averagePrices[1].stdDev))
    }
}