package nl.dirtya.flatsiteapi.model.printing

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.printing.service.PrintingOverviewService
import nl.dirtya.flatsiteapi.model.printing.service.PrintingService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.data.domain.Pageable
import org.springframework.test.context.ActiveProfiles
import org.springframework.web.multipart.MultipartFile
import javax.persistence.EntityManager


@ActiveProfiles("test")
class PrintingOverviewServiceTest
@Autowired constructor(
    private val entityManager: EntityManager,
    private val printingOverviewService: PrintingOverviewService,
    private val printingService: PrintingService,
    private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User
    private lateinit var user2: User
    private lateinit var user3: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        user2 = userService.create(UserFactory().createValid2())
        user3 = userService.create(UserFactory().createValid3())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun overviewIsUpdatedWhenPrinting() {
        val file = createFile()
        printingService.print(file)

        entityManager.flush()
        entityManager.clear()

        val overview = printingOverviewService.getOverview(true)
        assertEquals(1, overview[user1])
        assertEquals(0, overview[user2])
        assertEquals(0, overview[user3])
    }

    @Test
    fun overviewIsUpdatedWhenPrintingForMultipleUsers() {
        val file = createFile()
        printingService.print(file)
        printingService.print(file)
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user2.id)
        printingService.print(file)
        printingService.print(file)
        printingService.print(file)

        entityManager.flush()
        entityManager.clear()

        val overview = printingOverviewService.getOverview(true)
        assertEquals(2, overview[user1])
        assertEquals(3, overview[user2])
        assertEquals(0, overview[user3])
    }

    @Test
    fun printJobCanBeFound() {
        val file = createFile()
        val secondFile = createFile("2.pdf")
        val job1 = printingService.print(file)
        val job2 = printingService.print(secondFile)

        entityManager.flush()
        entityManager.clear()

        val jobs = printingOverviewService.findAll(Pageable.unpaged())
        assertEquals(2, jobs.totalElements)
        assertTrue(jobs.contains(job1))
        assertTrue(jobs.contains(job2))
    }

    private fun createFile(fileName: String = "test.pdf"): MultipartFile {
        val file = mock<MultipartFile>()
        whenever(file.originalFilename).thenReturn(fileName)
        return file
    }
}