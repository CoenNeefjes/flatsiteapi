package nl.dirtya.flatsiteapi.model.printing

import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.model.printing.print.PrintOS
import nl.dirtya.flatsiteapi.model.printing.service.PrintingService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.web.multipart.MultipartFile
import javax.persistence.EntityManager

class PrintingServiceTest
@Autowired constructor(
    private val entityManager: EntityManager,
    private val printingService: PrintingService,
    private val printOS: PrintOS,
    private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var user1: User

    @BeforeEach
    fun setup() {
        user1 = userService.create(UserFactory().createValid1())
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user1.id)
    }

    @Test
    fun printingAFileCallsPrint() {
        val file = createFile()
        val secondFile = createFile("2.pdf")
        printingService.print(file)
        printingService.print(secondFile, 5)

        entityManager.flush()
        entityManager.clear()

        verify(printOS).print(file, 1)
        verify(printOS).print(secondFile, 5)
    }

    private fun createFile(fileName: String = "test.pdf"): MultipartFile {
        val file = mock<MultipartFile>()
        whenever(file.originalFilename).thenReturn(fileName)
        return file
    }
}