package nl.dirtya.flatsiteapi.model.printing

import com.nhaarman.mockitokotlin2.mock
import nl.dirtya.flatsiteapi.model.printing.print.PrintOS
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.Profile

@Profile("test")
@Configuration
class PrintingServiceTestConfiguration {
    @Bean
    @Primary
    fun mockedPrintOS(): PrintOS {
        return mock()
    }
}