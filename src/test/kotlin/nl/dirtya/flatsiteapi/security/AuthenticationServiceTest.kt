package nl.dirtya.flatsiteapi.security

import com.nhaarman.mockitokotlin2.whenever
import nl.dirtya.flatsiteapi.BaseTest
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.exception.UnauthorizedException
import nl.dirtya.flatsiteapi.model.user.factory.UserFactory
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.AdminUserWriteDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserWriteDto
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.authentication.AuthenticationService
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.mock.mockito.MockBean
import javax.persistence.EntityManager

class AuthenticationServiceTest
@Autowired constructor(
        private val entityManager: EntityManager,
        private val authenticationService: AuthenticationService,
        private val userService: UserService
): BaseTest() {

    @MockBean
    lateinit var currentPrincipalService: CurrentPrincipalService

    private lateinit var userWriteDto: UserWriteDto;
    private lateinit var user: User
    private lateinit var admin: User

    @BeforeEach
    fun setup() {
        userWriteDto = UserFactory().createValid1()
        user = userService.create(userWriteDto)
        admin = userService.findByUsername("Admin")
    }

    @Test
    fun whenGeneratingJwtCorrectBehaviour() {
        val jwt = authenticationService.generateJwt(userWriteDto.username!!, userWriteDto.password!!)
        assertTrue(jwt.matches("^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*\$".toRegex()))
    }

    @Test
    fun whenGeneratingJwtUserNameNotFoundThrowsException() {
        assertThrows<UnauthorizedException> { authenticationService.generateJwt("NotFound", userWriteDto.password!!) }
    }

    @Test
    fun whenGeneratingJwtPasswordDoesNotMatchThrowsException() {
        assertThrows<UnauthorizedException> { authenticationService.generateJwt(userWriteDto.username!!, "WrongPassword") }
    }

    @Test
    fun whenGeneratingJwtUserIsNotActiveThrowsException() {
        user.active = false
        entityManager.persist(user)

        assertThrows<UnauthorizedException> { authenticationService.generateJwt(userWriteDto.username!!, userWriteDto.password!!) }
    }

    @Test
    fun whenImpersonatingCorrectBehaviour() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)
        val jwt = authenticationService.impersonateUser(user.id)
        assertTrue(jwt.matches("^[A-Za-z0-9-_=]+\\.[A-Za-z0-9-_=]+\\.?[A-Za-z0-9-_.+/=]*\$".toRegex()))
    }

    @Test
    fun whenImpersonatingUserIsNotAdminThrowsException() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(user.id)
        assertThrows<ForbiddenException> { authenticationService.impersonateUser(user.id) }
    }

    @Test
    fun whenImpersonatingInactiveUserThrowsException() {
        whenever(currentPrincipalService.getCurrentPrincipalId()).thenReturn(admin.id)

        userService.adminUpdate(user.id, AdminUserWriteDto(user.username, null, user.getRoles(), false))

        assertThrows<NotValidException> { authenticationService.impersonateUser(user.id) }
    }

}