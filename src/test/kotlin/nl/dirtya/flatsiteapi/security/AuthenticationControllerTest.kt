package nl.dirtya.flatsiteapi.security

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import nl.dirtya.flatsiteapi.IntegrationTest
import nl.dirtya.flatsiteapi.security.authentication.AuthenticationWriteDto
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class AuthenticationControllerTest: IntegrationTest() {

    @Test
    fun whenAuthenticatingCorrectBehaviour() {
        mockMvc.perform(post("/auth/authenticate")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jacksonObjectMapper().writeValueAsString(AuthenticationWriteDto("Admin", "password")))
        )
                .andExpect(status().isOk)
    }

}