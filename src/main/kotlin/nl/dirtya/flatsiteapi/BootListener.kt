package nl.dirtya.flatsiteapi

import lombok.extern.slf4j.Slf4j
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentCreateWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.service.RoomSolicitingAppointmentService
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.service.RoomSolicitingDayService
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto.RoomSolicitorWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.service.RoomSolicitorService
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserWriteDto
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.context.event.EventListener
import org.springframework.core.env.Environment
import org.springframework.stereotype.Component
import java.time.LocalDate
import javax.transaction.Transactional

@Slf4j
@Component class BootListener
@Autowired constructor(
        private val environment: Environment,
        private val userService: UserService,
        private val roomSolicitingDayService: RoomSolicitingDayService,
        private val roomSolicitorService: RoomSolicitorService,
        private val roomSolicitingAppointmentService: RoomSolicitingAppointmentService
){

    private val log = LoggerFactory.getLogger(javaClass)

    @EventListener
    @Transactional
    fun onApplicationStart(event: ApplicationStartedEvent) {
        // The production profile name is 'pi'
        val isProduction = environment.activeProfiles.toList().contains("pi")

        if (!isProduction && userService.countAll() == 1L) {
            log.info("Generating develop data...")

            // Create users
            val defaultPassword = "0000"
            val userNames = listOf("Sharky", "Ollie", "Sandy", "Vicky", "Trui", "Rikkert", "Frans")
            userNames.forEach { userService.create(UserWriteDto(it, defaultPassword, defaultPassword, null, null)) }

            // Create room soliciting data
            val day = roomSolicitingDayService.findByDateOrCreate(LocalDate.now())
            val solicitor = roomSolicitorService.create(RoomSolicitorWriteDto("feut", "nieuwe-feut@ditry-a.nl", 0, 18, "TCS", ""))
            val appointment = roomSolicitingAppointmentService.create(RoomSolicitingAppointmentCreateWriteDto(day.id, solicitor.id, "19:30-20:00"))

            for (i in 1..25) {
                roomSolicitorService.create(RoomSolicitorWriteDto("feut$i", "nieuwe-feut$i@ditry-a.nl", 0, 18, "TCS", ""))
            }

            log.info("Finished generating develop seed data")
        }
    }

}