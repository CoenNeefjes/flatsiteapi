package nl.dirtya.flatsiteapi.security

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.security.userdetails.CustomUserDetails
import org.springframework.security.authentication.AnonymousAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Service

@Service
class CurrentPrincipalService {

    fun getCurrentPrincipalUsername(): String {
        val authentication = SecurityContextHolder.getContext().authentication
        if (authentication !is AnonymousAuthenticationToken) {
            return authentication.name
        } else {
            throw NotFoundException("Current authenticated user could not be found")
        }
    }

    fun getCurrentPrincipalId(): Long {
        val authentication = SecurityContextHolder.getContext().authentication
        if (authentication !is AnonymousAuthenticationToken && authentication.principal is CustomUserDetails) {
            val principal = authentication.principal as CustomUserDetails
            return principal.getId()
        } else {
            throw NotFoundException("Current authenticated user could not be found")
        }
    }

}