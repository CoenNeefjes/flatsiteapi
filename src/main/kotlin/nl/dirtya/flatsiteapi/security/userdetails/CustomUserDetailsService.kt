package nl.dirtya.flatsiteapi.security.userdetails

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service class CustomUserDetailsService
@Autowired constructor(private val userService: UserService) : UserDetailsService {

    override fun loadUserByUsername(username: String): CustomUserDetails {
        val user = userService.findByUsername(username)
        return userToUserDetails(user)
    }

    fun loadUserById(id: Long): CustomUserDetails {
        val user = userService.findById(id)
        return userToUserDetails(user)
    }

    private fun userToUserDetails(user: User): CustomUserDetails {
        return CustomUserDetails(
                user.id,
                user.username,
                user.password,
                AuthorityUtils.commaSeparatedStringToAuthorityList(user.roles)
        )
    }

}