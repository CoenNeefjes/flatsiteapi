package nl.dirtya.flatsiteapi.security.authentication

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.exception.UnauthorizedException
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.security.jwt.JwtService
import nl.dirtya.flatsiteapi.security.utils.PasswordEncoder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class AuthenticationService
@Autowired constructor(
        private val userService: UserService,
        private val jwtService: JwtService
) {

    private fun checkCredentials(username: String, password: String): User {
        val user = userService.findByUsernameOptional(username) ?: throw UnauthorizedException("Unknown username")

        if (!PasswordEncoder.instance.matches(password, user.password)) {
            throw UnauthorizedException("Incorrect password")
        }
        if (!user.active) {
            throw UnauthorizedException("Your account is not activated, please contact the admin")
        }

        return user
    }

    fun generateJwt(username: String, password: String): String {
        val user = checkCredentials(username, password)
        return jwtService.generateToken(user.id.toString())
    }

    /**
     * Allows an admin to request a valid JWT token that is meant for another user.
     * This way an admin can login as another user without knowing his/her credentials.
     */
    fun impersonateUser(id: Long): String {
        val currentUser = userService.getCurrentUser()

        if (!currentUser.getRoles().contains(Roles.ADMIN)) {
            throw ForbiddenException()
        }

        val userToImpersonate = userService.findById(id)

        if (!userToImpersonate.active) {
            throw NotValidException("You can not impersonate an inactive user")
        }

        return jwtService.generateToken(userToImpersonate.id.toString())
    }

}