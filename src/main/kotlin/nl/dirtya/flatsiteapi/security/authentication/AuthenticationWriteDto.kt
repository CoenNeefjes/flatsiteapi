package nl.dirtya.flatsiteapi.security.authentication

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullOrEmpty

class AuthenticationWriteDto(val username: String?, val password: String?) {

    fun assertValid() {
        stringNotNullOrEmpty(username, "Username")
        stringNotNullOrEmpty(password, "Password")
    }
}