package nl.dirtya.flatsiteapi.security.authentication

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import javax.transaction.Transactional

@RestController
@RequestMapping("/auth")
@Transactional
class AuthenticationController @Autowired constructor(private val authenticationService: AuthenticationService) {

    @GetMapping("/impersonate/{id}")
    fun impersonate(@PathVariable(name = "id") id: Long): String {
        return authenticationService.impersonateUser(id)
    }

    @PostMapping("/authenticate")
    fun authenticate(@RequestBody writeDto: AuthenticationWriteDto): String {
        writeDto.assertValid()
        return authenticationService.generateJwt(writeDto.username!!, writeDto.password!!)
    }
}