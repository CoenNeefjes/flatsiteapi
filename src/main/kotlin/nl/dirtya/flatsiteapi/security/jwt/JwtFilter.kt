package nl.dirtya.flatsiteapi.security.jwt

import io.jsonwebtoken.JwtException
import nl.dirtya.flatsiteapi.security.userdetails.CustomUserDetailsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import java.io.IOException
import java.lang.NumberFormatException
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component class JwtFilter: OncePerRequestFilter() {

    @Autowired private lateinit var jwtService: JwtService
    @Autowired private lateinit var userDetailsService: CustomUserDetailsService

    @Throws(ServletException::class, IOException::class)
    override fun doFilterInternal(request: HttpServletRequest, response: HttpServletResponse, filterChain: FilterChain) {
        val authHeader = request.getHeader("Authorization")

        if (authHeader != null && authHeader.length > 7 && authHeader.startsWith("Bearer ")) {

            val authHeaderToken = authHeader.substring(7)

            val jwtSubject = try { jwtService.extractSubject(authHeaderToken) } catch (e: JwtException) { "-1" }

            val userId = try { jwtSubject.toLong() } catch (e: NumberFormatException) { -1L }

            if (userId != -1L) {

                val userDetails = userDetailsService.loadUserById(userId)

                if (jwtService.validateToken(authHeaderToken, userDetails)) {
                    val token = UsernamePasswordAuthenticationToken(userDetails, null, userDetails.authorities)
                    token.details = WebAuthenticationDetailsSource().buildDetails(request)
                    SecurityContextHolder.getContext().authentication = token
                }
            }
        }

        filterChain.doFilter(request, response)
    }
}