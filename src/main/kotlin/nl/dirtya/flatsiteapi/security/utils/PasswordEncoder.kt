package nl.dirtya.flatsiteapi.security.utils

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

class PasswordEncoder {

    companion object {
        val instance: BCryptPasswordEncoder = BCryptPasswordEncoder()
    }

}