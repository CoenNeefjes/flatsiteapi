package nl.dirtya.flatsiteapi.security.utils

class BearerTokenUtils {

    companion object {

        fun getToken(authHeader: String?): String? {
            if (authHeader != null && authHeader.startsWith("Bearer ")) {
                return authHeader.substring(7)
            }
            return null
        }

    }

}