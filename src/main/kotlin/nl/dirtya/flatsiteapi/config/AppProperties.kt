package nl.dirtya.flatsiteapi.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "app")
class AppProperties(val jwt: Jwt, val passwordReset: PasswordReset, val directories: Directories, val transactions: Transactions, val beerfridge: Beerfridge) {

    data class Jwt(val secret: String, val expiration: Long)

    data class PasswordReset(val url: String, val validHours: Int)

    data class Directories(val images: String)

    data class Transactions(val safeMode: Boolean)

    data class Beerfridge(val threshold: Int)
}