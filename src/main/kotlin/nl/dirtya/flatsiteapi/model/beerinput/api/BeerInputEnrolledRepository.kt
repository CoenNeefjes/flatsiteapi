package nl.dirtya.flatsiteapi.model.beerinput.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface BeerInputEnrolledRepository: JpaRepository<BeerInputEnrolled, Long>, JpaSpecificationExecutor<BeerInputEnrolled> {

    @Query("SELECT SUM(bu.amount) FROM BeerInputEnrolled bu, BeerInput bi " +
            "WHERE bu.beerInput.id = bi.id AND bi.type = :beerInputType AND bi.beerType.id = :beerTypeId AND bu.user.id = :userId")
    fun sumAmountWhereUserIdEquals(
            @Param("userId") userId: Long,
            @Param("beerInputType") beerInputType: BeerInputType,
            @Param("beerTypeId") beerTypeId: Long
    ): Int?

}