package nl.dirtya.flatsiteapi.model.beerinput.controller.dto

import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputEnrolled
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class BeerInputEnrolledReadDto(beerInputEnrolled: BeerInputEnrolled) {
    val id = beerInputEnrolled.id
    val user = SimpleUserReadDto(beerInputEnrolled.user)
    val amount = beerInputEnrolled.amount
}