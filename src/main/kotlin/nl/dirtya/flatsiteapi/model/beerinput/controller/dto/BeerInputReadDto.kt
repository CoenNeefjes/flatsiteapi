package nl.dirtya.flatsiteapi.model.beerinput.controller.dto

import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.beertype.controller.dto.BeerTypeReadDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class BeerInputReadDto(beerInput: BeerInput) {

    val id = beerInput.id
    val date = beerInput.date
    val createdBy = SimpleUserReadDto(beerInput.createdBy)
    val beerType = BeerTypeReadDto(beerInput.beerType)
    val type = beerInput.type
    val note = beerInput.note
    val image = beerInput.image?.id
    val enrollments = beerInput.enrollments.map { BeerInputEnrolledReadDto(it) }
}