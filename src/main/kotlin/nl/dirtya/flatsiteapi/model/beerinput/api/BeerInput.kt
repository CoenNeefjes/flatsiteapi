package nl.dirtya.flatsiteapi.model.beerinput.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

/**
 * Representation of a user entering a receipt for bought crates of beer or the contents of a beer list
 */

@Entity
@ToString
class BeerInput(
        val date: LocalDate,
        @Enumerated(EnumType.STRING) var type: BeerInputType,
        @ManyToOne val createdBy: User,
        @ManyToOne var beerType: BeerType,
        var note: String?
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    @OneToMany(mappedBy = "beerInput", cascade = [CascadeType.ALL], orphanRemoval = true)
    var enrollments: MutableSet<BeerInputEnrolled> = HashSet()

    @OneToOne
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    var image: Image? = null
}