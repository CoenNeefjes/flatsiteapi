package nl.dirtya.flatsiteapi.model.beerinput.controller.dto

import nl.dirtya.flatsiteapi.util.EnrolledWriteDto
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull

class BeerInputEnrolledWriteDto(
        override val user: Long?,
        override val amount: Int?
): EnrolledWriteDto {

    override fun assertValid() {
        objectNotNull(user, "User")
        integerMinimumNotNull(amount, "Amount", 1)
    }

}