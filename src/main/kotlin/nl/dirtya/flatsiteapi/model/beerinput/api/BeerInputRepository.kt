package nl.dirtya.flatsiteapi.model.beerinput.api

import au.com.console.jpaspecificationdsl.*
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface BeerInputRepository: JpaRepository<BeerInput, Long>, JpaSpecificationExecutor<BeerInput> {

    companion object {

        fun idEquals(input: Long?): Specification<BeerInput>? = input?.let {
            return BeerInput::id.equal(input)
        }

        fun typeEquals(input: BeerInputType?): Specification<BeerInput>? = input?.let {
            return BeerInput::type.equal(input)
        }

        fun dateAfter(input: LocalDate?): Specification<BeerInput>? = input?.let {
            return BeerInput::date.greaterThanOrEqualTo(input)
        }

        fun dateBefore(input: LocalDate?): Specification<BeerInput>? = input?.let {
            return BeerInput::date.lessThanOrEqualTo(input)
        }

        fun createdByEquals(input: Long?): Specification<BeerInput>? = input?.let {
            return where { equal(it.join(BeerInput::createdBy).get(User::id), input) }
        }

        fun beerTypeEquals(input: Long?): Specification<BeerInput>? = input?.let {
            return where { equal(it.join(BeerInput::beerType).get(BeerType::id), input) }
        }

    }

}