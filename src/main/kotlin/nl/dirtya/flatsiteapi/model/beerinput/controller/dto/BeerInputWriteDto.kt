package nl.dirtya.flatsiteapi.model.beerinput.controller.dto

import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.collectionNotNullOrEmptyOrContainsNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import java.time.LocalDate

class BeerInputWriteDto(
        val date: LocalDate?,
        val type: BeerInputType?,
        val beerType: Long?,
        val note: String?,
        val enrollments: List<BeerInputEnrolledWriteDto?>?
) {

    fun assertValid() {
        objectNotNull(date, "Date")
        objectNotNull(type, "Type")
        objectNotNull(beerType, "BeerType")
        collectionNotNullOrEmptyOrContainsNull(enrollments, "Enrollments")
        enrollments!!.forEach { it!!.assertValid() }
    }

}