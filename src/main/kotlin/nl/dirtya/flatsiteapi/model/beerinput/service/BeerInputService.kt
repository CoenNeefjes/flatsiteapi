package nl.dirtya.flatsiteapi.model.beerinput.service

import au.com.console.jpaspecificationdsl.and
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputRepository
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputRepository.Companion.beerTypeEquals
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputRepository.Companion.createdByEquals
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputRepository.Companion.dateAfter
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputRepository.Companion.dateBefore
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputRepository.Companion.typeEquals
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputWriteDto
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.LocalDate
import javax.persistence.EntityManager

@Service class BeerInputService
@Autowired constructor(
        private val entityManager: EntityManager,
        private val beerInputRepository: BeerInputRepository,
        private val beerInputEnrolledService: BeerInputEnrolledService,
        private val beerTypeService: BeerTypeService,
        private val userService: UserService
) {

    fun findAll(): List<BeerInput> {
        return beerInputRepository.findAll()
    }

    fun search(
            before: LocalDate?,
            after: LocalDate?,
            type: BeerInputType?,
            createdBy: Long?,
            beerType: Long?,
            pageable: Pageable
    ): Page<BeerInput> {
        val filter = and(
                dateBefore(before),
                dateAfter(after),
                typeEquals(type),
                createdByEquals(createdBy),
                beerTypeEquals(beerType)
        )

        return beerInputRepository.findAll(filter, pageable)
    }

    fun findById(id: Long): BeerInput {
        return beerInputRepository.findById(id).orElseThrow { NotFoundException("BeerInput with id $id could not be found") }
    }

    fun create(writeDto: BeerInputWriteDto): BeerInput {
        writeDto.assertValid()

        if (!userService.currentUserIsAdmin()) {
            throw ForbiddenException("Only admins can create beer inputs")
        }

        val beerType = beerTypeService.findById(writeDto.beerType!!)
        val currentUser = userService.getCurrentUser()
        val beerInput = BeerInput(writeDto.date!!, writeDto.type!!, currentUser, beerType, writeDto.note)

        // Make sure the beerUnit exists in the database before creating the beerInputEnrollments
        entityManager.persist(beerInput)

        val units = writeDto.enrollments!!.map { beerInputEnrolledService.create(it!!, beerInput) }
        beerInput.enrollments.addAll(units)

        return beerInputRepository.save(beerInput)
    }

    fun update(id: Long, writeDto: BeerInputWriteDto): BeerInput {
        writeDto.assertValid()

        val beerInput = findById(id)

        if (beerInput.createdBy != userService.getCurrentUser()) {
            throw ForbiddenException("You cannot update the beerInput of someone else")
        }

        beerInput.type = writeDto.type!!
        beerInput.beerType = beerTypeService.findById(writeDto.beerType!!)
        beerInput.note = writeDto.note

        return updateEnrollments(beerInput, writeDto.enrollments!!)
    }

    private fun updateEnrollments(beerInput: BeerInput, enrollments: List<BeerInputEnrolledWriteDto?>): BeerInput {
        beerInput.enrollments.clear()

        // Make sure the deletion has happened before creating new enrollments
        entityManager.flush()

        beerInput.enrollments.addAll(enrollments.map { beerInputEnrolledService.create(it!!, beerInput) })

        return beerInputRepository.save(beerInput)
    }

    fun updateImage(beerInput: BeerInput, image: Image): BeerInput {
        if (beerInput.createdBy != userService.getCurrentUser()) {
            throw ForbiddenException("You cannot add an image to the beerInput of another user")
        }
        beerInput.image = image
        return beerInputRepository.save(beerInput)
    }

}