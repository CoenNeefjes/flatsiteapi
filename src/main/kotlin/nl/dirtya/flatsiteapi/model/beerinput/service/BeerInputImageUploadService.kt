package nl.dirtya.flatsiteapi.model.beerinput.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.image.service.ImageFileService
import nl.dirtya.flatsiteapi.model.image.service.ImageService
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service class BeerInputImageUploadService
@Autowired constructor(
    private val beerInputService: BeerInputService,
    private val userService: UserService,
    private val albumService: AlbumService,
    private val imageService: ImageService,
    private val imageFileService: ImageFileService
) {

    fun upload(file: MultipartFile, id: Long): BeerInput {
        val beerInput = beerInputService.findById(id)

        if (beerInput.image != null) {
            imageFileService.delete(beerInput.image!!.id)
        }

        val currentUser = userService.getCurrentUser()

        if (beerInput.createdBy != currentUser) {
            throw ForbiddenException("You cannot add an image to the beerInput of another user")
        }

        val album = albumService.findByName("bier inkoop bonnetjes")
        val extension = file.originalFilename!!.split('.').last()
        val image = imageService.create(album, extension)

        imageFileService.save(file, image)

        return beerInputService.updateImage(beerInput, image)
    }

}