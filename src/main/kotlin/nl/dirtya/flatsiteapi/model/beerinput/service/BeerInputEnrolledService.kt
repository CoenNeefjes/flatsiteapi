package nl.dirtya.flatsiteapi.model.beerinput.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputEnrolled
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputEnrolledRepository
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class BeerInputEnrolledService
@Autowired constructor(
        private val beerInputEnrolledRepository: BeerInputEnrolledRepository,
        private val userService: UserService
){

    fun findAll(): List<BeerInputEnrolled> {
        return beerInputEnrolledRepository.findAll()
    }

    fun findById(id: Long): BeerInputEnrolled {
        return beerInputEnrolledRepository.findById(id).orElseThrow { NotFoundException("BeerInputEnrollment with id $id could not be found") }
    }

    fun create(writeDto: BeerInputEnrolledWriteDto, beerInput: BeerInput): BeerInputEnrolled {
        writeDto.assertValid()

        val user = userService.findById(writeDto.user!!)

        return beerInputEnrolledRepository.save(BeerInputEnrolled(user, writeDto.amount!!, beerInput))
    }

    fun count(user: Long, beerInputType: BeerInputType, beerType: Long): Int {
        return beerInputEnrolledRepository.sumAmountWhereUserIdEquals(user, beerInputType, beerType) ?: 0
    }

}