package nl.dirtya.flatsiteapi.model.beerinput.controller

import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputBalanceReadDto
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputReadDto
import nl.dirtya.flatsiteapi.model.beerinput.controller.dto.BeerInputWriteDto
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputBalanceService
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputImageUploadService
import nl.dirtya.flatsiteapi.model.beerinput.service.BeerInputService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import java.time.LocalDate

@RestController
@RequestMapping("/beer-input")
@Transactional
class BeerInputController @Autowired constructor(
        private val beerInputService: BeerInputService,
        private val beerInputImageUploadService: BeerInputImageUploadService,
        private val beerInputBalanceService: BeerInputBalanceService
) {

    @GetMapping
    fun search(
            @RequestParam(name = "before", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) before: LocalDate?,
            @RequestParam(name = "after", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) after: LocalDate?,
            @RequestParam(name = "beerInputType", required = false) beerInputType: BeerInputType?,
            @RequestParam(name = "createdBy", required = false) createdBy: Long?,
            @RequestParam(name = "beerType", required = false) beerType: Long?,
            pageable: Pageable
    ): Page<BeerInputReadDto> {
        return beerInputService.search(before, after, beerInputType, createdBy, beerType, pageable).map(::BeerInputReadDto)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id") id: Long): BeerInputReadDto {
        return BeerInputReadDto(beerInputService.findById(id))
    }

    @GetMapping("/balance")
    fun balance(
            @RequestParam(name = "beerType", required = true) beerType: Long,
            @RequestParam(name = "activeOnly", required = true) activeOnly: Boolean
    ): List<BeerInputBalanceReadDto> {
        return beerInputBalanceService.calculateBalanceForUsers(beerType, activeOnly).map { BeerInputBalanceReadDto(it.key, it.value) }
    }

    @GetMapping("/balance/me")
    fun myBalance(@RequestParam(name = "beerType", required = true) beerType: Long): Int {
        return beerInputBalanceService.calculateBalanceForMe(beerType)
    }

    @PostMapping
    fun create(@RequestBody writeDto: BeerInputWriteDto): BeerInputReadDto {
        return BeerInputReadDto(beerInputService.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id") id: Long, @RequestBody writeDto: BeerInputWriteDto): BeerInputReadDto {
        return BeerInputReadDto(beerInputService.update(id, writeDto))
    }

    @PutMapping("/{id}/image")
    fun uploadImage(@PathVariable(name = "id") id: Long, @RequestParam("file") file: MultipartFile): BeerInputReadDto {
        return BeerInputReadDto(beerInputImageUploadService.upload(file, id))
    }

}