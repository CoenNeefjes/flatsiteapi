package nl.dirtya.flatsiteapi.model.beerinput.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

/**
 * Representation of how many crates of beer a user has bought
 */

@Entity
@ToString(exclude = ["beerInput"])
class BeerInputEnrolled(@ManyToOne val user: User, val amount: Int, @ManyToOne val beerInput: BeerInput): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()
}