package nl.dirtya.flatsiteapi.model.beerinput.api

enum class BeerInputType {
    LIST,
    RECEIPT
}