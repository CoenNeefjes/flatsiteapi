package nl.dirtya.flatsiteapi.model.beerinput.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class BeerInputBalanceReadDto(user: User, balance: Int) {
    val user = SimpleUserReadDto(user)
    val balance = balance
}