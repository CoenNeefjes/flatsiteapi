package nl.dirtya.flatsiteapi.model.beerinput.service

import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputType
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class BeerInputBalanceService
@Autowired constructor(
        private val beerInputEnrolledService: BeerInputEnrolledService,
        private val beerTypeService: BeerTypeService,
        private val userService: UserService
) {

    fun calculateBalanceForUsers(beerTypeId: Long, activeOnly: Boolean): Map<User, Int> {
        val beerType = beerTypeService.findById(beerTypeId)
        val users = if (activeOnly) userService.findByActive(true) else userService.findAll()

        return users.map { it to calculateBalanceForUser(it, beerType) }.toMap()
    }

    fun calculateBalanceForUser(user: User, beerType: BeerType): Int {
        val bought = beerInputEnrolledService.count(user.id, BeerInputType.RECEIPT, beerType.id)
        val drank = beerInputEnrolledService.count(user.id, BeerInputType.LIST, beerType.id)
        return bought - drank;
    }

    fun calculateBalanceForMe(beerType: Long): Int {
        return calculateBalanceForMe(beerTypeService.findById(beerType))
    }

    fun calculateBalanceForMe(beerType: BeerType): Int {
        val user = userService.getCurrentUser()
        return calculateBalanceForUser(user, beerType)
    }

}