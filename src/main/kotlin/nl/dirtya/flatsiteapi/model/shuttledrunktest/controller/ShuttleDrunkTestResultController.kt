package nl.dirtya.flatsiteapi.model.shuttledrunktest.controller

import nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto.ShuttleDrunkTestHighScoreReadDto
import nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto.ShuttleDrunkTestResultReadDto
import nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto.ShuttleDrunkTestResultWriteDto
import nl.dirtya.flatsiteapi.model.shuttledrunktest.service.ShuttleDrunkTestOverviewService
import nl.dirtya.flatsiteapi.model.shuttledrunktest.service.ShuttleDrunkTestResultService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/shuttle-drunk-test")
@Transactional
class ShuttleDrunkTestResultController
@Autowired constructor(
        private val shuttleDrunkTestResultService: ShuttleDrunkTestResultService,
        private val shuttleDrunkTestOverviewService: ShuttleDrunkTestOverviewService
){

    @GetMapping
    fun findAll(pageable: Pageable): Page<ShuttleDrunkTestResultReadDto> {
        return shuttleDrunkTestResultService.findAll(pageable).map(::ShuttleDrunkTestResultReadDto)
    }

    @GetMapping("/high-score/overview")
    fun overview(@RequestParam(name = "activeOnly", required = true) activeOnly: Boolean): List<ShuttleDrunkTestHighScoreReadDto> {
        return shuttleDrunkTestOverviewService.getOverview(activeOnly).map { ShuttleDrunkTestHighScoreReadDto(it.key, it.value) }
    }

    @PostMapping
    fun create(@RequestBody writeDto: ShuttleDrunkTestResultWriteDto): ShuttleDrunkTestResultReadDto {
        return ShuttleDrunkTestResultReadDto(shuttleDrunkTestResultService.create(writeDto))
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable(name = "id") id: Long) {
        shuttleDrunkTestResultService.delete(id)
    }

}