package nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.doubleNotNullOrNegative
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import java.time.LocalDate

class ShuttleDrunkTestResultWriteDto(val date: LocalDate?, val score: Double?, val user: Long?) {

    fun assertValid() {
        objectNotNull(date, "Date")
        doubleNotNullOrNegative(score, "Score")
        objectNotNull(user, "User")
    }

}