package nl.dirtya.flatsiteapi.model.shuttledrunktest.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.shuttledrunktest.api.ShuttleDrunkTestResult
import nl.dirtya.flatsiteapi.model.shuttledrunktest.api.ShuttleDrunkTestResultRepository
import nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto.ShuttleDrunkTestResultWriteDto
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service class ShuttleDrunkTestResultService
@Autowired constructor(
        private val shuttleDrunkTestResultRepository: ShuttleDrunkTestResultRepository,
        private val userService: UserService
){

    fun findById(id: Long): ShuttleDrunkTestResult {
        return shuttleDrunkTestResultRepository.findById(id).orElseThrow { NotFoundException("Result with id $id could not be found") }
    }

    fun findAll(pageable: Pageable): Page<ShuttleDrunkTestResult> {
        return shuttleDrunkTestResultRepository.findAll(pageable);
    }

    fun create(writeDto: ShuttleDrunkTestResultWriteDto): ShuttleDrunkTestResult {
        writeDto.assertValid()

        val user = userService.findById(writeDto.user!!)

        val currentUser = userService.getCurrentUser()

        if (!currentUser.getRoles().contains(Roles.ADMIN) && currentUser != user) {
            throw ForbiddenException("You cannot create a result for another user")
        }

        return shuttleDrunkTestResultRepository.save(ShuttleDrunkTestResult(writeDto.date!!, writeDto.score!!, user))
    }

    fun delete(id: Long) {
        val shuttleDrunkTestResult = findById(id)

        val currentUser = userService.getCurrentUser()
        if (!currentUser.getRoles().contains(Roles.ADMIN) && currentUser != shuttleDrunkTestResult.user) {
            throw ForbiddenException("You cannot delete the result of another user")
        }

        shuttleDrunkTestResultRepository.delete(shuttleDrunkTestResult)
    }

}