package nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto

import nl.dirtya.flatsiteapi.model.shuttledrunktest.api.ShuttleDrunkTestResult
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class ShuttleDrunkTestResultReadDto(shuttleDrunkTestResult: ShuttleDrunkTestResult) {
    val id = shuttleDrunkTestResult.id
    val date = shuttleDrunkTestResult.date
    val score = shuttleDrunkTestResult.score
    val user = SimpleUserReadDto(shuttleDrunkTestResult.user)
}