package nl.dirtya.flatsiteapi.model.shuttledrunktest.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
@ToString
class ShuttleDrunkTestResult(val date: LocalDate, val score: Double, @ManyToOne var user: User): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

}