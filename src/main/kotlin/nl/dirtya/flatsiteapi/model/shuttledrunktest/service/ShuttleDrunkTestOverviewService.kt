package nl.dirtya.flatsiteapi.model.shuttledrunktest.service

import nl.dirtya.flatsiteapi.model.shuttledrunktest.api.ShuttleDrunkTestResultRepository
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class ShuttleDrunkTestOverviewService
@Autowired constructor(
        private val shuttleDrunkTestResultRepository: ShuttleDrunkTestResultRepository,
        private val userService: UserService
){

    fun getOverview(activeOnly: Boolean): Map<User, Double> {
        val users = if (activeOnly) userService.findByActive(true) else userService.findAll()
        return users.map { it to (shuttleDrunkTestResultRepository.maxScoreWhereUserEquals(it.id) ?: 0.0) }.toMap()
    }

}