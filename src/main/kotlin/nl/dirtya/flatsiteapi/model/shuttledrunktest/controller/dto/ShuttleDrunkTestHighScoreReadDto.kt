package nl.dirtya.flatsiteapi.model.shuttledrunktest.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class ShuttleDrunkTestHighScoreReadDto(user: User, score: Double) {
    val user = SimpleUserReadDto(user)
    val score = score
}