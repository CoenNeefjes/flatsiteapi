package nl.dirtya.flatsiteapi.model.shuttledrunktest.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface ShuttleDrunkTestResultRepository: JpaRepository<ShuttleDrunkTestResult, Long>, JpaSpecificationExecutor<ShuttleDrunkTestResult> {

    @Query("SELECT MAX(s.score) FROM ShuttleDrunkTestResult s WHERE s.user.id = :userId")
    fun maxScoreWhereUserEquals(@Param("userId") userId: Long): Double?

}