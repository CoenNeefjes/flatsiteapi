package nl.dirtya.flatsiteapi.model.vote.controller.dto

import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import nl.dirtya.flatsiteapi.model.vote.api.Vote

class VoteReadDto(vote: Vote) {
    val id = vote.id
    val description = vote.description
    val createBy = SimpleUserReadDto(vote.createdBy)
    val createdOn = vote.createdOn
    val usersAgree = vote.usersAgree.map { SimpleUserReadDto(it) }
    val usersDisagree = vote.usersDisagree.map { SimpleUserReadDto(it) }
}