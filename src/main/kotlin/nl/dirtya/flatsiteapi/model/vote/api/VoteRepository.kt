package nl.dirtya.flatsiteapi.model.vote.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface VoteRepository: JpaRepository<Vote, Long>, JpaSpecificationExecutor<Vote> {
}