package nl.dirtya.flatsiteapi.model.vote.controller

import nl.dirtya.flatsiteapi.model.vote.controller.dto.VoteReadDto
import nl.dirtya.flatsiteapi.model.vote.controller.dto.VoteWriteDto
import nl.dirtya.flatsiteapi.model.vote.service.VoteService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/votes")
@Transactional
class VoteController @Autowired constructor(private val voteService: VoteService) {

    @GetMapping
    fun findAll(pageable: Pageable): Page<VoteReadDto> {
        return voteService.findAllPaged(pageable).map(::VoteReadDto)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id") id: Long): VoteReadDto {
        return VoteReadDto(voteService.findById(id))
    }

    @PostMapping
    fun create(@RequestBody writeDto: VoteWriteDto): VoteReadDto {
        return VoteReadDto(voteService.create(writeDto))
    }

    @PutMapping("/{id}")
    fun vote(@PathVariable(name = "id") id: Long, @RequestParam(name = "value", required = true) value: Boolean): VoteReadDto {
        return VoteReadDto(voteService.vote(id, value))
    }

}