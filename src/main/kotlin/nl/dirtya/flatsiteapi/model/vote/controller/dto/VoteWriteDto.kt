package nl.dirtya.flatsiteapi.model.vote.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullEmptyMaxLength

class VoteWriteDto(val description: String?) {

    fun assertValid() {
        stringNotNullEmptyMaxLength(description, "Description", 255)
    }

}