package nl.dirtya.flatsiteapi.model.vote.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.model.vote.api.Vote
import nl.dirtya.flatsiteapi.model.vote.api.VoteRepository
import nl.dirtya.flatsiteapi.model.vote.controller.dto.VoteWriteDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service class VoteService
@Autowired constructor(
        private val voteRepository: VoteRepository,
        private val userService: UserService
) {

    fun findAll(): List<Vote> {
        return voteRepository.findAll()
    }

    fun findAllPaged(pageable: Pageable): Page<Vote> {
        return voteRepository.findAll(pageable)
    }

    fun findById(id: Long): Vote {
        return voteRepository.findById(id).orElseThrow { NotFoundException("Vote with id $id could not be found") }
    }

    fun create(writeDto: VoteWriteDto): Vote {
        writeDto.assertValid()
        val currentUser = userService.getCurrentUser()
        return voteRepository.save(Vote(writeDto.description!!, currentUser))
    }

    fun vote(id: Long, value: Boolean): Vote {
        val vote = findById(id)
        val currentUser = userService.getCurrentUser()

        if (value) {
            vote.usersDisagree.remove(currentUser)
            vote.usersAgree.add(currentUser)
        } else {
            vote.usersAgree.remove(currentUser)
            vote.usersDisagree.add(currentUser)
        }

        return voteRepository.save(vote)
    }

}