package nl.dirtya.flatsiteapi.model.vote.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@ToString
class Vote(val description: String, @ManyToOne val createdBy: User): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    val createdOn: LocalDate = LocalDate.now()

    @ManyToMany
    @JoinTable(
        name = "vote_users_agree",
        joinColumns = [JoinColumn(name = "vote_id")],
        inverseJoinColumns = [JoinColumn(name = "user_model_id")]
    )
    val usersAgree: MutableSet<User> = HashSet()

    @ManyToMany
    @JoinTable(
        name = "vote_users_disagree",
        joinColumns = [JoinColumn(name = "vote_id")],
        inverseJoinColumns = [JoinColumn(name = "user_model_id")]
    )
    val usersDisagree: MutableSet<User> = HashSet()

}