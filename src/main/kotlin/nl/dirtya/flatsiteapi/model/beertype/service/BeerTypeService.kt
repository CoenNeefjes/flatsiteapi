package nl.dirtya.flatsiteapi.model.beertype.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.beertype.api.BeerType
import nl.dirtya.flatsiteapi.model.beertype.api.BeerTypeRepository
import nl.dirtya.flatsiteapi.model.beertype.api.BeerTypeRepository.Companion.nameEquals
import nl.dirtya.flatsiteapi.model.beertype.controller.dto.BeerTypeWriteDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class BeerTypeService
@Autowired constructor(
        private val beerTypeRepository: BeerTypeRepository
){

    fun findAll(): List<BeerType> {
        return beerTypeRepository.findAll()
    }

    fun findById(id: Long): BeerType {
        return beerTypeRepository.findById(id).orElseThrow { NotFoundException("BeerType with id $id could not be found") }
    }

    fun findByName(name: String): BeerType? {
        return beerTypeRepository.findOne(nameEquals(name)).orElse(null)
    }

    fun create(writeDto: BeerTypeWriteDto): BeerType {
        writeDto.assertValid()

        if (findByName(writeDto.name!!.trim()) != null) {
            throw NotValidException("BeerType with name ${writeDto.name} already exists")
        }

        return beerTypeRepository.save(BeerType(writeDto.name, writeDto.beersPerCrate!!))
    }

    fun update(id: Long, writeDto: BeerTypeWriteDto): BeerType {
        writeDto.assertValid()

        val beerType = findById(id)

        if (beerType.name != writeDto.name) {
            if (findByName(writeDto.name!!) != null) {
                throw NotValidException("BeerType with name ${writeDto.name} already exists")
            }
            beerType.name = writeDto.name
        }

        beerType.beersPerCrate = writeDto.beersPerCrate!!

        return beerTypeRepository.save(beerType)
    }

}