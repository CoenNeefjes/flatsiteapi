package nl.dirtya.flatsiteapi.model.beertype.api

import lombok.ToString
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@ToString
class BeerType(var name: String, var beersPerCrate: Int): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()
}