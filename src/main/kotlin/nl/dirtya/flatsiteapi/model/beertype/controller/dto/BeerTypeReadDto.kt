package nl.dirtya.flatsiteapi.model.beertype.controller.dto

import nl.dirtya.flatsiteapi.model.beertype.api.BeerType

class BeerTypeReadDto(beerType: BeerType) {
    val id = beerType.id
    val name = beerType.name
    val beersPerCrate = beerType.beersPerCrate
}