package nl.dirtya.flatsiteapi.model.beertype.controller

import nl.dirtya.flatsiteapi.model.beertype.controller.dto.BeerTypeReadDto
import nl.dirtya.flatsiteapi.model.beertype.controller.dto.BeerTypeWriteDto
import nl.dirtya.flatsiteapi.model.beertype.service.BeerTypeService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/beer-types")
@Transactional
class BeerTypeController @Autowired constructor(private val beerTypeService: BeerTypeService) {

    @GetMapping
    fun findAll(): List<BeerTypeReadDto> {
        return beerTypeService.findAll().map(::BeerTypeReadDto)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id") id: Long): BeerTypeReadDto {
        return BeerTypeReadDto(beerTypeService.findById(id))
    }

    @PostMapping
    fun create(@RequestBody writeDto: BeerTypeWriteDto): BeerTypeReadDto {
        return BeerTypeReadDto(beerTypeService.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id") id: Long, @RequestBody writeDto: BeerTypeWriteDto): BeerTypeReadDto {
        return BeerTypeReadDto(beerTypeService.update(id, writeDto))
    }

}