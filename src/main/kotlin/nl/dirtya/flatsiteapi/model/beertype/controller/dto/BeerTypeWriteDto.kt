package nl.dirtya.flatsiteapi.model.beertype.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullEmptyMaxLength

class BeerTypeWriteDto(val name: String?, val beersPerCrate: Int?) {

    fun assertValid() {
        stringNotNullEmptyMaxLength(name, "Name", 50)
        integerMinimumNotNull(beersPerCrate, "BeersPerCrate", 1)
    }

}