package nl.dirtya.flatsiteapi.model.beertype.api

import au.com.console.jpaspecificationdsl.equal
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BeerTypeRepository: JpaRepository<BeerType, Long>, JpaSpecificationExecutor<BeerType> {

 companion object {

     fun nameEquals(input: String): Specification<BeerType> {
         return BeerType::name.equal(input)
     }

 }

}