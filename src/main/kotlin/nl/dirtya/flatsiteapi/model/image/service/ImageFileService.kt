package nl.dirtya.flatsiteapi.model.image.service

import nl.dirtya.flatsiteapi.exception.InternalServerException
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.image.util.ImageHelper
import nl.dirtya.flatsiteapi.util.FileValidator
import org.imgscalr.Scalr
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.nio.file.Files

@Service class ImageFileService
@Autowired constructor(
        private val imageService: ImageService,
        private val imageLocationResolver: ImageLocationResolver
) {

    fun findById(id: Long): ByteArray {
        val image = imageService.findById(id)
        return getBytesFromFileDestination(image.id, image.extension)
    }

    fun save(file: MultipartFile, image: Image) {
        save(file, image, null)
    }

    fun save(file: MultipartFile, image: Image, resizeToWidth: Int?) {
        try {
            FileValidator.validateMultipartFile(file)

            val bufferedImage = ImageHelper.convertToBufferedImage(file)

            val tempFile = ImageHelper.convertToTempFile(file)
            val orientation = ImageHelper.getOrientationFromFile(tempFile)

            val resizedBufferedImage = if (resizeToWidth != null) Scalr.resize(ImageHelper.copyBufferedImage(bufferedImage), resizeToWidth) else bufferedImage

            ImageHelper.writeBufferedImageToLocation(
                    ImageHelper.rotateBasedOnOrientation(resizedBufferedImage, orientation),
                    getFileDestination(image.id, image.extension),
                    image.extension
            )
        } catch (e: Exception) {
            // Delete the image entity when something went wrong saving the file
            imageService.deleteDangerously(image)
            // Then throw the exception
            e.printStackTrace()
            throw e
        }
    }

    fun delete(imageId: Long) {
        val image = imageService.findById(imageId)
        imageService.delete(imageId)
        getFileFromDestination(imageId, image.extension).delete()
    }

    private fun getBytesFromFileDestination(id: Long, extension: String): ByteArray {
        val file = getFileFromDestination(id, extension)

        try {
            return Files.readAllBytes(file.toPath())
        } catch (e: Exception) {
            e.printStackTrace()
            throw InternalServerException("Something went wrong fetching your file")
        }
    }

    private fun getFileFromDestination(id: Long, extension: String): File {
        val file = File(getFileDestination(id, extension))

        if (!file.exists()) {
            if (imageService.findByIdOptional(id) != null) {
                imageService.delete(id)
            }
        }

        return file
    }

    private fun getFileDestination(id: Long, extension: String): String {
        return imageLocationResolver.resolve() + id.toString() + "." + extension
    }

}