package nl.dirtya.flatsiteapi.model.image.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.album.api.Album
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.*

@Entity
@ToString
class Image(@ManyToOne val createdBy: User, @ManyToOne val album: Album, val extension: String): Identifiable() {

    constructor(createdBy: User, album: Album, user: User, extension: String): this(createdBy, album, extension) {
        this.user = user
    }

    constructor(createdBy: User, album: Album, beerInput: BeerInput, extension: String): this(createdBy, album, extension) {
        this.beerInput = beerInput
    }

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    val createdOn: LocalDate = LocalDate.now()

    @OneToOne(mappedBy = "image")
    var user: User? = null

    @OneToOne(mappedBy = "image")
    var beerInput: BeerInput? = null

}