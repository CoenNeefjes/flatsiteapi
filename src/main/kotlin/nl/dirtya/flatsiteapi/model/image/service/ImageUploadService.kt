package nl.dirtya.flatsiteapi.model.image.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.image.api.Image
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service class ImageUploadService
@Autowired constructor(
        private val imageFileService: ImageFileService,
        private val imageService: ImageService,
        private val albumService: AlbumService
) {

    fun upload(file: MultipartFile, albumId: Long): Image {
        val album = albumService.findById(albumId)

        if (!album.editable) {
            throw ForbiddenException("You are not allowed to edit this album")
        }

        val extension = file.originalFilename!!.split('.').last()
        val image = imageService.create(album, extension)

        imageFileService.save(file, image)

        return image
    }

}