package nl.dirtya.flatsiteapi.model.image.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.album.api.Album
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.image.api.ImageRepository
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service class ImageService
@Autowired constructor(
        private val imageRepository: ImageRepository,
        private val userService: UserService
) {

    fun findAll(): List<Image> {
        return imageRepository.findAll()
    }

    fun findById(id: Long): Image {
        return imageRepository.findById(id).orElseThrow { NotFoundException("Image with id $id could not be found") }
    }

    fun findByIdOptional(id: Long): Image? {
        return imageRepository.findById(id).orElse(null)
    }

    fun create(album: Album, extension: String = "jpg"): Image {
        val currentUser = userService.getCurrentUser()

        val image = imageRepository.save(Image(currentUser, album, extension))

        album.images.add(image)
        album.lastEdited = LocalDate.now()

        return image
    }

    fun delete(id: Long) {
        delete(findById(id))
    }

    fun delete(image: Image) {
        // Check if user has correct rights
        if (image.createdBy != userService.getCurrentUser()) {
            throw ForbiddenException("You are not allowed to delete an image of another user")
        }
        // Delete any reference to this image
        image.user?.let { it.image = null }
        image.beerInput?.let { it.image = null }

        // Update album meta data
        image.album.lastEdited = LocalDate.now()

        imageRepository.delete(image)
    }

    fun deleteDangerously(image: Image) {
        imageRepository.delete(image)
    }
}