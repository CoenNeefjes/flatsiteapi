package nl.dirtya.flatsiteapi.model.image.controller

import io.jsonwebtoken.JwtException
import nl.dirtya.flatsiteapi.exception.UnauthorizedException
import nl.dirtya.flatsiteapi.model.image.controller.dto.ImageReadDto
import nl.dirtya.flatsiteapi.model.image.service.ImageFileService
import nl.dirtya.flatsiteapi.model.image.service.ImageService
import nl.dirtya.flatsiteapi.model.image.service.ImageUploadService
import nl.dirtya.flatsiteapi.security.jwt.JwtService
import nl.dirtya.flatsiteapi.security.utils.BearerTokenUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile
import javax.activation.FileTypeMap

@RestController
@RequestMapping("/images")
@Transactional
class ImageController
@Autowired constructor(
        private val imageFileService: ImageFileService,
        private val imageUploadService: ImageUploadService,
        private val imageService: ImageService,
        private val jwtService: JwtService
) {

    /**
     * Get an image by id
     *
     * Usually this endpoint would be protected like all the others, but since a html <img> element cannot handle
     * authentication using a header, we have to send the jwt via the url. This is not standard practice and could be
     * considered unsafe. Normally a short-lived jwt should be generated for purposes like this. However that is
     * outside of the scope of this project.
     */
    @GetMapping("/{id}")
    fun findById(
        @PathVariable(name = "id") id: Long,
        @RequestParam(name = "token") token: String?,
        @RequestHeader(name = "Authorization") authHeader: String?
    ): ResponseEntity<ByteArray> {
        // Try to get the token from the auth header, else take the token from the url
        val providedAuthentication = BearerTokenUtils.getToken(authHeader) ?: token
        try {
            // Check if a token is present and if it is valid
            if (providedAuthentication == null || jwtService.isTokenExpired(providedAuthentication)) throw UnauthorizedException()
        } catch (e: JwtException) {
            // Checking if a token is valid can throw a JwtException
            throw UnauthorizedException()
        }

        val image = imageService.findById(id)
        val mediaType = MediaType.valueOf(FileTypeMap.getDefaultFileTypeMap().getContentType("a." + image.extension))
        return ResponseEntity.ok().contentType(mediaType).body(imageFileService.findById(id))
    }

    @PostMapping
    fun upload(@RequestParam("file") file: MultipartFile, @RequestParam album: Long): ImageReadDto {
        return ImageReadDto(imageUploadService.upload(file, album))
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable(name = "id") id: Long) {
        imageFileService.delete(id)
    }

}