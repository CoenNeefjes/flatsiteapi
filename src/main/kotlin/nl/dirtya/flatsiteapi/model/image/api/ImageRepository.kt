package nl.dirtya.flatsiteapi.model.image.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface ImageRepository: JpaRepository<Image, Long>, JpaSpecificationExecutor<Image> {
}