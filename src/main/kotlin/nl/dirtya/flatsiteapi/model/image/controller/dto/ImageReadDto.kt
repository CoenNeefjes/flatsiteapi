package nl.dirtya.flatsiteapi.model.image.controller.dto

import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class ImageReadDto(image: Image) {
    val id = image.id
    val createdBy = SimpleUserReadDto(image.createdBy)
    val createdOn = image.createdOn
}