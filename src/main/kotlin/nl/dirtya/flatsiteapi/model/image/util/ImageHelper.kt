package nl.dirtya.flatsiteapi.model.image.util

import com.drew.imaging.ImageMetadataReader
import com.drew.metadata.Directory
import com.drew.metadata.exif.ExifIFD0Directory
import nl.dirtya.flatsiteapi.exception.InternalServerException
import org.imgscalr.Scalr
import org.springframework.web.multipart.MultipartFile
import java.awt.image.BufferedImage
import java.io.ByteArrayInputStream
import java.io.File
import javax.imageio.ImageIO

class ImageHelper {

    companion object {

        fun convertToTempFile(file: MultipartFile): File {
            try {
                val parts = file.originalFilename!!.split(".").toTypedArray()
                val tempFile = File.createTempFile(System.getProperty("java.io.tmpdir") + "/" + parts[0], parts[1])
                tempFile.deleteOnExit()
                file.transferTo(tempFile)
                return tempFile
            } catch (e: Exception) {
                e.printStackTrace()
                throw InternalServerException("Something went wrong handling your file")
            }
        }

        fun convertToBufferedImage(file: MultipartFile): BufferedImage {
            try {
                val inputStream = ByteArrayInputStream(file.bytes)
                return ImageIO.read(inputStream)
            } catch (e: Exception) {
                e.printStackTrace()
                throw InternalServerException("Something went wrong transforming your image")
            }
        }

        fun copyBufferedImage(bi: BufferedImage): BufferedImage {
            val cm = bi.colorModel
            val isAlphaPremultiplied = cm.isAlphaPremultiplied
            val raster = bi.copyData(null)
            return BufferedImage(cm, raster, isAlphaPremultiplied, null)
        }

        fun getOrientationFromFile(file: File): Int {
            try {
                val metadata = ImageMetadataReader.readMetadata(file)
                val directory: Directory? = metadata.getFirstDirectoryOfType(ExifIFD0Directory::class.java)
                if (directory == null || !directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {
                    return 1
                }
                return directory.getInt(ExifIFD0Directory.TAG_ORIENTATION)
            } catch (e: Exception) {
                e.printStackTrace()
                throw InternalServerException("Something went wrong evaluating the orientation of your image")
            }
        }

        fun rotateBasedOnOrientation(bufferedImage: BufferedImage, orientation: Int): BufferedImage {
            return when (orientation) {
                0, 1 -> bufferedImage
                2 -> Scalr.rotate(bufferedImage, Scalr.Rotation.FLIP_HORZ)
                3 -> Scalr.rotate(bufferedImage, Scalr.Rotation.CW_180)
                4 -> Scalr.rotate(bufferedImage, Scalr.Rotation.FLIP_VERT)
                5 -> Scalr.rotate(Scalr.rotate(bufferedImage, Scalr.Rotation.CW_90), Scalr.Rotation.FLIP_HORZ)
                6 -> Scalr.rotate(bufferedImage, Scalr.Rotation.CW_90)
                7 -> Scalr.rotate(Scalr.rotate(bufferedImage, Scalr.Rotation.CW_90), Scalr.Rotation.FLIP_VERT)
                8 -> Scalr.rotate(bufferedImage, Scalr.Rotation.CW_270)
                else -> throw InternalServerException("Encountered unsupported rotation when processing your image")
            }
        }

        fun writeBufferedImageToLocation(bufferedImage: BufferedImage, fileDestination: String, extension: String) {
            try {
                ImageIO.write(bufferedImage, extension, File(fileDestination))
            } catch (e: Exception) {
                e.printStackTrace()
                throw InternalServerException("Something went wrong writing your file to storage")
            }
        }
    }

}