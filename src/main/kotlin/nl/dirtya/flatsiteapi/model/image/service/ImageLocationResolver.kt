package nl.dirtya.flatsiteapi.model.image.service

import nl.dirtya.flatsiteapi.config.AppProperties
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component
class ImageLocationResolver @Autowired constructor(private val appProperties: AppProperties) {

    fun resolve(): String {
        return appProperties.directories.images
    }

}