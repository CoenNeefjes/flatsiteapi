package nl.dirtya.flatsiteapi.model.album.controller.dto

import nl.dirtya.flatsiteapi.model.album.api.Album
import nl.dirtya.flatsiteapi.model.image.controller.dto.ImageReadDto

class AlbumReadDto(album: Album) {
    val id = album.id
    val name = album.name
    val images = album.images.map { ImageReadDto(it) }
    val editable = album.editable
    val lastEdited = album.lastEdited
}