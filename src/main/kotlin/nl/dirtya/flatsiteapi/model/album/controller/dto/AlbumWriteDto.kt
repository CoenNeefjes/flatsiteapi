package nl.dirtya.flatsiteapi.model.album.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullEmptyMaxLength

class AlbumWriteDto(val name: String?) {

    fun assertValid() {
        stringNotNullEmptyMaxLength(name, "Name", 50)
    }
}