package nl.dirtya.flatsiteapi.model.album.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany
import kotlin.collections.HashSet

@Entity
@ToString
class Album(var name: String): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    val createdOn: LocalDate = LocalDate.now()

    var lastEdited: LocalDate = LocalDate.now()

    @OneToMany(mappedBy = "album")
    val images: MutableSet<Image> = HashSet()

    /**
     * Albums that cannot be edited should be added using a migration
     */
    val editable: Boolean = true
}