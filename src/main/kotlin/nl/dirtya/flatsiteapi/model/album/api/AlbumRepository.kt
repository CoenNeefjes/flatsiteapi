package nl.dirtya.flatsiteapi.model.album.api

import au.com.console.jpaspecificationdsl.equal
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface AlbumRepository: JpaRepository<Album, Long>, JpaSpecificationExecutor<Album> {

    companion object {

        fun idEquals(input: Long?): Specification<Album>? = input?.let {
            return Album::id.equal(input)
        }

        fun nameEquals(input: String?): Specification<Album>? = input?.let {
            return Album::name.equal(input)
        }

    }

}