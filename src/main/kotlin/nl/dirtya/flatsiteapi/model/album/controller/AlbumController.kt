package nl.dirtya.flatsiteapi.model.album.controller

import nl.dirtya.flatsiteapi.model.album.controller.dto.AlbumReadDto
import nl.dirtya.flatsiteapi.model.album.controller.dto.AlbumWriteDto
import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/albums")
@Transactional
class AlbumController
@Autowired constructor(private val albumService: AlbumService) {

    @GetMapping
    fun findAll(): List<AlbumReadDto> {
        return albumService.findAll().map(::AlbumReadDto)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id") id: Long): AlbumReadDto {
        return AlbumReadDto(albumService.findById(id))
    }

    @PostMapping
    fun create(@RequestBody writeDto: AlbumWriteDto): AlbumReadDto {
        return AlbumReadDto(albumService.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id") id: Long, @RequestBody writeDto: AlbumWriteDto): AlbumReadDto {
        return AlbumReadDto(albumService.update(id, writeDto))
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable(name = "id") id: Long) {
        albumService.delete(id)
    }
}