package nl.dirtya.flatsiteapi.model.album.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.album.api.Album
import nl.dirtya.flatsiteapi.model.album.api.AlbumRepository
import nl.dirtya.flatsiteapi.model.album.api.AlbumRepository.Companion.nameEquals
import nl.dirtya.flatsiteapi.model.album.controller.dto.AlbumWriteDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service class AlbumService
@Autowired constructor(private val albumRepository: AlbumRepository) {

    fun findAll(): List<Album> {
        return albumRepository.findAll()
    }

    fun findById(id: Long): Album {
        return albumRepository.findById(id).orElseThrow { NotFoundException("Album with id $id could not be found") }
    }

    fun findByName(name: String): Album {
        return albumRepository.findOne(nameEquals(name)).orElseThrow { NotFoundException("Album with name $name could not be found") }
    }

    fun findByNameOptional(name: String): Album? {
        return albumRepository.findOne(nameEquals(name)).orElse(null)
    }

    fun create(writeDto: AlbumWriteDto): Album {
        writeDto.assertValid()

        if (findByNameOptional(writeDto.name!!) != null) {
            throw NotValidException("Album with that name already exists")
        }

        return albumRepository.save(Album(writeDto.name))
    }

    fun update(id: Long, writeDto: AlbumWriteDto): Album {
        writeDto.assertValid()

        val album = findById(id)

        if (album.name != writeDto.name) {
            if (findByNameOptional(writeDto.name!!) != null) {
                throw NotValidException("Album with that name already exists")
            }
            album.name = writeDto.name
        }

        album.lastEdited = LocalDate.now()

        return albumRepository.save(album)
    }

    fun delete(id: Long) {
        val album = findById(id)

        if (!album.editable) {
            throw NotValidException("You cannot edit this album")
        }

        if (album.images.isNotEmpty()) {
            throw NotValidException("You can only delete empty albums")
        }

        albumRepository.delete(album)
    }

}