package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionReadDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import java.time.LocalDate

class CookingEventReadDto(cookingEvent: CookingEvent) {
    val id: Long = cookingEvent.id
    val date: LocalDate = cookingEvent.date
    val cook: SimpleUserReadDto? = if (cookingEvent.cook != null) SimpleUserReadDto(cookingEvent.cook!!) else null
    val enrolled: List<CookingEnrolledReadDto> = cookingEvent.enrollments.map { CookingEnrolledReadDto(it) }
    val transaction: TransactionReadDto? = if (cookingEvent.transaction != null) TransactionReadDto(cookingEvent.transaction!!) else null
    val dryer1: SimpleUserReadDto? = if (cookingEvent.dryer1 != null) SimpleUserReadDto(cookingEvent.dryer1!!) else null
    val dryer2: SimpleUserReadDto? = if (cookingEvent.dryer2 != null) SimpleUserReadDto(cookingEvent.dryer2!!) else null
}