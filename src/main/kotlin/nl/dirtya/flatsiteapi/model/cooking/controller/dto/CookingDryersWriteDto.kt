package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.exception.NotValidException

class CookingDryersWriteDto(val dryer1: Long?, val dryer2: Long?) {

    fun assertValid() {
        if (dryer1 != null && dryer2 != null) {
            if (dryer1 == dryer2) {
                throw NotValidException("Dryers cannot be the same")
            }
        }
    }

}