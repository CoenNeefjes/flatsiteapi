package nl.dirtya.flatsiteapi.model.cooking.api

import au.com.console.jpaspecificationdsl.*
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface CookingEventRepository: JpaRepository<CookingEvent, Long>, JpaSpecificationExecutor<CookingEvent> {

    companion object {

        fun idEquals(input: Long?): Specification<CookingEvent>? = input?.let {
            return CookingEvent::id.equal(input)
        }

        fun cookEquals(input: Long?): Specification<CookingEvent>? = input?.let {
            return where { equal(it.join(CookingEvent::cook).get(User::id), input) }
        }

        fun dateEquals(input: LocalDate?): Specification<CookingEvent>? = input?.let {
            return CookingEvent::date.equal(input)
        }

        fun dateIn(input: Set<LocalDate>?): Specification<CookingEvent>? = input?.let {
            return CookingEvent::date.`in`(input)
        }

        fun dateAfterOrEqual(input: LocalDate?): Specification<CookingEvent>? = input?.let {
            return CookingEvent::date.greaterThanOrEqualTo(input)
        }

        fun dateBeforeOrEqual(input: LocalDate?): Specification<CookingEvent>? = input?.let {
            return CookingEvent::date.lessThanOrEqualTo(input)
        }

        fun cookNotNull() : Specification<CookingEvent> = let {
            return CookingEvent::cook.isNotNull()
        }

        fun transactionNotNull() : Specification<CookingEvent> = let {
            return CookingEvent::transaction.isNotNull()
        }

        fun transactionIsNull() : Specification<CookingEvent> = let {
            return CookingEvent::transaction.isNull()
        }

    }

    @Query("SELECT SUM(c.pointsForCook) FROM CookingEvent c WHERE c.cook.id = :userId")
    fun sumPointsForCookWhereCookIdEquals(@Param("userId") userId: Long): Int?

}