package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.model.cooking.api.CookingEnrolled
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class CookingEnrolledReadDto(cookingEnrolled: CookingEnrolled) {

    val id: Long = cookingEnrolled.id
    val user: SimpleUserReadDto = SimpleUserReadDto(cookingEnrolled.user)
    val amount: Int = cookingEnrolled.amount

}