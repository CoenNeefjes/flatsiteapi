package nl.dirtya.flatsiteapi.model.cooking.controller

import nl.dirtya.flatsiteapi.model.cooking.controller.dto.*
import nl.dirtya.flatsiteapi.model.cooking.service.CookingBalanceService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@RequestMapping("/cooking-events")
@Transactional
class CookingEventController
@Autowired constructor(
        private val cookingEventService: CookingEventService,
        private val cookingBalanceService: CookingBalanceService
) {

    @GetMapping("get-or-create")
    fun getOrCreate(@RequestParam(name = "date", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) date: LocalDate): CookingEventReadDto {
        return CookingEventReadDto(cookingEventService.findByDateOrCreate(date))
    }

    @GetMapping("get-or-create/range")
    fun getOrCreateInRange(
            @RequestParam(name = "from", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) from: LocalDate,
            @RequestParam(name = "to", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) to: LocalDate
    ): List<CookingEventReadDto> {
        return cookingEventService.findAllInRangeOrCreate(from, to).map { CookingEventReadDto(it) }
    }

    @GetMapping("/balance")
    fun balance(@RequestParam(name = "activeOnly", required = true) activeOnly: Boolean): List<CookingBalanceReadDto> {
        return cookingBalanceService.calculateBalanceForUsers(activeOnly).map { CookingBalanceReadDto(it.key, it.value) }
    }

    @GetMapping("/balance/me")
    fun myBalance(): Int {
        return cookingBalanceService.calculateBalanceForMe()
    }

    @GetMapping("/{id}/actions")
    fun getActions(@PathVariable(name = "id") id: Long): List<CookingActionReadDto> {
        return cookingEventService.findById(id).actions.map(::CookingActionReadDto)
    }

    @GetMapping("/{id}/dryers/suggestion")
    fun getDryerSuggestions(@PathVariable(name = "id") id: Long): List<SimpleUserReadDto> {
        return cookingEventService.getDryerSuggestions(id).map(::SimpleUserReadDto)
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id") id: Long, @RequestBody writeDto: CookingEventWriteDto): CookingEventReadDto {
        return CookingEventReadDto(cookingEventService.update(id, writeDto))
    }

    @PutMapping("/{id}/enroll")
    fun enroll(@PathVariable(name = "id") id: Long, @RequestParam(name = "amount", required = true) amount: Int): CookingEventReadDto {
        return CookingEventReadDto(cookingEventService.enroll(id, amount))
    }

    @PutMapping("/{id}/enroll/cook")
    fun enrollAsCook(@PathVariable(name = "id") id: Long): CookingEventReadDto {
        return CookingEventReadDto(cookingEventService.enrollAsCook(id))
    }

    @PutMapping("/{id}/un-enroll/cook")
    fun unEnrollAsCook(@PathVariable(name = "id") id: Long): CookingEventReadDto {
        return CookingEventReadDto(cookingEventService.unEnrollAsCook(id))
    }

    @PutMapping("/{id}/dryers")
    fun updateDryers(@PathVariable(name = "id") id: Long, @RequestBody writeDto: CookingDryersWriteDto): CookingEventReadDto {
        return CookingEventReadDto(cookingEventService.updateDryers(id, writeDto))
    }

}