package nl.dirtya.flatsiteapi.model.cooking.api

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDateTime
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class CookingAction(@ManyToOne val user: User, val message: String, @ManyToOne val cookingEvent: CookingEvent): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    val timestamp: LocalDateTime = LocalDateTime.now()

}