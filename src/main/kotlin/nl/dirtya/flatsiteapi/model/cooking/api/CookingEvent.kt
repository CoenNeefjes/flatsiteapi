package nl.dirtya.flatsiteapi.model.cooking.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.transaction.api.Transaction
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

@Entity
@ToString
class CookingEvent(val date: LocalDate): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    @ManyToOne
    var cook: User? = null

    var pointsForCook: Int = 0

    @OneToMany(mappedBy = "cookingEvent", cascade = [CascadeType.ALL], orphanRemoval = true)
    val enrollments: MutableSet<CookingEnrolled> = HashSet()

    @OneToMany(mappedBy = "cookingEvent", cascade = [CascadeType.ALL], orphanRemoval = true)
    @OrderBy("timestamp ASC")
    val actions: MutableList<CookingAction> = ArrayList()

    @OneToOne
    @JoinColumn(name = "transaction_id", referencedColumnName = "id")
    var transaction: Transaction? = null

    @ManyToOne
    var dryer1: User? = null

    @ManyToOne
    var dryer2: User? = null

    fun getTotalEnrollments(): Int {
        return enrollments.sumOf { it.amount }
    }

    fun updatePointsForCook() {
        pointsForCook = getTotalEnrollments()
    }

    /**
     * Return true if this CookingEvent has a CookingEnrolled for the given user
     * and the user is enrolled more than 0 times
     */
    fun isEnrolled(userId: Long): Boolean {
        val enrollment = enrollments.find { it.user.id == userId }
        return enrollment != null && enrollment.amount > 0
    }

    /**
     * Return true if this CookingEvent has a CookingEnrolled for the given user
     */
    fun hasEnrollmentForUser(userId: Long): Boolean {
        return enrollments.find { it.user.id == userId } != null
    }

}