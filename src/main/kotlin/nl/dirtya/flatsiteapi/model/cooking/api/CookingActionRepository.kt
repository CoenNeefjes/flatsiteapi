package nl.dirtya.flatsiteapi.model.cooking.api

import au.com.console.jpaspecificationdsl.*
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface CookingActionRepository: JpaRepository<CookingAction, Long>, JpaSpecificationExecutor<CookingAction> {

    companion object {

        fun userEquals(input: Long?): Specification<CookingAction>? = input?.let {
            return where { equal(it.join(CookingAction::user).get(User::id), input) }
        }

        fun cookingEventEquals(input: Long?): Specification<CookingAction>? = input?.let {
            return where { equal(it.join(CookingAction::cookingEvent).get(CookingEvent::id), input) }
        }

    }

}