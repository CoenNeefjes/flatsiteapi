package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class CookingBalanceReadDto(user: User, balance: Int) {
    val user = SimpleUserReadDto(user)
    val balance = balance
}