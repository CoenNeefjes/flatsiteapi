package nl.dirtya.flatsiteapi.model.cooking.controller

import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDonationReadDto
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDonationWriteDto
import nl.dirtya.flatsiteapi.model.cooking.service.CookingDonationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.time.LocalDate

@RestController
@RequestMapping("/cooking-donations")
@Transactional
class CookingDonationController @Autowired constructor(private val cookingDonationService: CookingDonationService) {

    @GetMapping
    fun findAll(pageable: Pageable): Page<CookingDonationReadDto> {
        return cookingDonationService.findAllPaged(pageable).map(::CookingDonationReadDto)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id") id: Long): CookingDonationReadDto {
        return CookingDonationReadDto(cookingDonationService.findById(id))
    }

    @GetMapping("/search")
    fun search(
            @RequestParam(name = "before", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) before: LocalDate?,
            @RequestParam(name = "after", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) after: LocalDate?,
            @RequestParam(name = "createdBy", required = false) createdBy: Long?,
            @RequestParam(name = "recipient", required = false) recipient: Long?,
            pageable: Pageable
    ): Page<CookingDonationReadDto> {
        return cookingDonationService.search(before, after, createdBy, recipient, pageable).map(::CookingDonationReadDto)
    }

    @PostMapping
    fun create(@RequestBody writeDto: CookingDonationWriteDto): CookingDonationReadDto {
        return CookingDonationReadDto(cookingDonationService.create(writeDto))
    }

}