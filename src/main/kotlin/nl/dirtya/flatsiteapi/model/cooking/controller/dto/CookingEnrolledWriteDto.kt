package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.util.EnrolledWriteDto

/**
 * WriteDto that can only be used by the cook of a cookingEvent
 */

class CookingEnrolledWriteDto(
        override val user: Long?,
        override val amount: Int?
): EnrolledWriteDto