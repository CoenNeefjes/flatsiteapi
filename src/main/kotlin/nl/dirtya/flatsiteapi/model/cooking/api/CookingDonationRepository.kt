package nl.dirtya.flatsiteapi.model.cooking.api

import au.com.console.jpaspecificationdsl.*
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface CookingDonationRepository: JpaRepository<CookingDonation, Long>, JpaSpecificationExecutor<CookingDonation> {

    companion object {

        fun createdOnAfter(input: LocalDate?): Specification<CookingDonation>? = input?.let {
            return CookingDonation::createdOn.greaterThanOrEqualTo(input)
        }

        fun createdOnBefore(input: LocalDate?): Specification<CookingDonation>? = input?.let {
            return CookingDonation::createdOn.lessThanOrEqualTo(input)
        }

        fun createdByEquals(input: Long?): Specification<CookingDonation>? = input?.let {
            return where { equal(it.join(CookingDonation::createdBy).get(User::id), input) }
        }

        fun recipientEquals(input: Long?): Specification<CookingDonation>? = input?.let {
            return where { equal(it.join(CookingDonation::recipient).get(User::id), input) }
        }

    }

    @Query("SELECT SUM(cd.amount) FROM CookingDonation cd WHERE cd.createdBy.id = :userId")
    fun sumAmountWhereCreatedByEquals(@Param("userId") userId: Long): Int?

    @Query("SELECT SUM(cd.amount) FROM CookingDonation cd WHERE cd.recipient.id = :userId")
    fun sumAmountWhereRecipientEquals(@Param("userId") userId: Long): Int?
}