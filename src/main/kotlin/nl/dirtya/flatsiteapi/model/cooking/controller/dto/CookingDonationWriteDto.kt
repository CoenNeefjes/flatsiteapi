package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull

class CookingDonationWriteDto(val amount: Int?, val recipient: Long?) {

    fun assertValid() {
        integerMinimumNotNull(amount, "Amount", 1)
        objectNotNull(recipient, "Recipient")
    }

}