package nl.dirtya.flatsiteapi.model.cooking.service

import au.com.console.jpaspecificationdsl.and
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonation
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonationRepository
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonationRepository.*
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonationRepository.Companion.createdByEquals
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonationRepository.Companion.createdOnAfter
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonationRepository.Companion.createdOnBefore
import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonationRepository.Companion.recipientEquals
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDonationWriteDto
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.time.LocalDate

@Service class CookingDonationService
@Autowired constructor(
        private val cookingDonationRepository: CookingDonationRepository,
        private val userService: UserService
) {

    fun findAll(): List<CookingDonation> {
        return cookingDonationRepository.findAll()
    }

    fun findAllPaged(pageable: Pageable): Page<CookingDonation> {
        return cookingDonationRepository.findAll(pageable)
    }

    fun findById(id: Long): CookingDonation {
        return cookingDonationRepository.findById(id).orElseThrow { NotFoundException("CookingDonation with id $id could not be found") }
    }

    fun search(
            before: LocalDate?,
            after: LocalDate?,
            createdBy: Long?,
            recipient: Long?,
            pageable: Pageable
    ): Page<CookingDonation> {
        val filter = and(
                createdOnBefore(before),
                createdOnAfter(after),
                createdByEquals(createdBy),
                recipientEquals(recipient)
        )
        return cookingDonationRepository.findAll(filter, pageable)
    }

    fun create(writeDto: CookingDonationWriteDto): CookingDonation {
        writeDto.assertValid()

        val recipient = userService.findById(writeDto.recipient!!)
        val currentUser = userService.getCurrentUser()

        if (currentUser == recipient) {
            throw NotValidException("You cannot donate to yourself")
        }

        return cookingDonationRepository.save(CookingDonation(currentUser, writeDto.amount!!, recipient))
    }

    fun countAmountGiven(userId: Long): Int {
        return cookingDonationRepository.sumAmountWhereCreatedByEquals(userId) ?: 0
    }

    fun countAmountReceived(userId: Long): Int {
        return cookingDonationRepository.sumAmountWhereRecipientEquals(userId) ?: 0
    }

}