package nl.dirtya.flatsiteapi.model.cooking.api

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class CookingEnrolled(@ManyToOne val user: User, var amount: Int, @ManyToOne val cookingEvent: CookingEvent ): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    override fun toString(): String {
        return "CookingEnrolled(user={id=${user.id},name=${user.username}}, amount=$amount, cookingEvent=${cookingEvent.id}, id=$id, uuid='$uuid')"
    }

}