package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.model.cooking.api.CookingDonation
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class CookingDonationReadDto(cookingDonation: CookingDonation) {
    val id = cookingDonation.id
    val createdOn = cookingDonation.createdOn
    val amount = cookingDonation.amount
    val createdBy = SimpleUserReadDto(cookingDonation.createdBy)
    val recipient = SimpleUserReadDto(cookingDonation.recipient)
}