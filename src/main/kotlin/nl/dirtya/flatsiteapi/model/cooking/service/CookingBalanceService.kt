package nl.dirtya.flatsiteapi.model.cooking.service

import nl.dirtya.flatsiteapi.exception.InternalServerException
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class CookingBalanceService
@Autowired constructor(
        private val cookingEventService: CookingEventService,
        private val cookingEnrolledService: CookingEnrolledService,
        private val cookingDonationService: CookingDonationService,
        private val userService: UserService
) {

    fun calculateBalanceForUsers(activeOnly: Boolean): Map<User, Int> {
        val users = if (activeOnly) userService.findByActive(true) else userService.findAll()
        return users.map { it to calculateBalanceForUser(it.id) }.toMap()
    }

    fun calculateBalanceForUser(userId: Long): Int {
        val earned = cookingEventService.countCookingPointsForCook(userId)
        val spent = cookingEnrolledService.sumEnrollmentsWhereCookWasPresent(userId)
        val cookingBalance = earned - spent

        val donated = cookingDonationService.countAmountGiven(userId)
        val received = cookingDonationService.countAmountReceived(userId)
        val donationBalance = received - donated

        return cookingBalance + donationBalance
    }

    fun calculateBalanceForMe(): Int {
        return calculateBalanceForUser(userService.getCurrentUser().id)
    }

    /**
     * Function that asserts the total cooking points balance of all users is 0. If that is the case
     * some logic in the cooking events is incorrect.
     *
     * Only use this function for testing purposes, since it is heavy to compute this.
     */
    fun assertTotalBalanceIsZero() {
        val totalBalance = calculateBalanceForUsers(false).values.sumOf { it }
        if (totalBalance != 0) {
            throw InternalServerException("Total cooking points balance is not 0, but: $totalBalance")
        }
    }

}