package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.model.cooking.api.CookingAction

class CookingActionReadDto(cookingAction: CookingAction) {
    val id = cookingAction.id
    val message = cookingAction.message
    val timestamp = cookingAction.timestamp
}