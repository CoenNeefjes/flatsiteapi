package nl.dirtya.flatsiteapi.model.cooking.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface CookingEnrolledRepository: JpaRepository<CookingEnrolled, Long>, JpaSpecificationExecutor<CookingEnrolled> {

    @Query("SELECT SUM(c.amount) FROM CookingEnrolled c, CookingEvent e WHERE c.user.id = :userId AND c.cookingEvent.id = e.id AND e.cook IS NOT NULL")
    fun sumAmountWhereUserIdEqualsAndCookIsPresent(@Param("userId") userId: Long): Int?

    @Query("SELECT COUNT(c) FROM CookingEnrolled c, CookingEvent e " +
            "WHERE c.user.id = :userId " +
            "AND c.cookingEvent.id = e.id " +
            "AND e.cook IS NOT NULL " +
            "AND NOT e.cook = c.user " +
            "AND c.amount > 0")
    fun countAmountWhereUserIdEqualsAndCookIsPresentAndUserIsNotCook(@Param("userId") userId: Long): Int?

}