package nl.dirtya.flatsiteapi.model.cooking.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEnrolled
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository.Companion.cookNotNull
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository.Companion.dateBeforeOrEqual
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository.Companion.dateEquals
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository.Companion.dateIn
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository.Companion.transactionIsNull
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEventRepository.Companion.transactionNotNull
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingDryersWriteDto
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.cooking.controller.dto.CookingEventWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.util.EnrolledWriteDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Lazy
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.stream.Collectors
import java.util.stream.IntStream
import javax.persistence.EntityManager
import kotlin.streams.toList

@Service class CookingEventService
@Autowired constructor(
        private val entityManager: EntityManager,
        private val cookingEventRepository: CookingEventRepository,
        private val userService: UserService,
        private val cookingEnrolledService: CookingEnrolledService,
        @Lazy private val transactionService: TransactionService,
        private val cookingActionService: CookingActionService
) {
    fun findAll(): List<CookingEvent> {
        return cookingEventRepository.findAll()
    }

    fun findById(id: Long): CookingEvent {
        return cookingEventRepository.findById(id).orElseThrow { NotFoundException("CookingEvent with id $id could not be found") }
    }

    fun findByIdOptional(id: Long): CookingEvent? {
        return cookingEventRepository.findById(id).orElse(null)
    }

    fun findByDate(date: LocalDate): CookingEvent? {
        return cookingEventRepository.findOne(dateEquals(date)).orElse(null)
    }

    fun findByDateOrCreate(date: LocalDate): CookingEvent {
        return findByDate(date) ?: create(date)
    }

    fun findWithCookAndTransaction() : List<CookingEvent> {
        return cookingEventRepository.findAll(cookNotNull().and(transactionNotNull()))
    }

    fun findForgottenTransactions(date: LocalDate): List<CookingEvent> {
        return cookingEventRepository.findAll(cookNotNull().and(transactionIsNull().and(dateBeforeOrEqual(date))))
    }

    /**
     * Finds all cookingEvent within the specified date range (exclusive of end date)
     */
    fun findAllInRange(start: LocalDate, end: LocalDate): Map<LocalDate, CookingEvent?> {
        val numOfDaysBetween: Long = ChronoUnit.DAYS.between(start, end)

        if (numOfDaysBetween < 0) {
            throw NotValidException("End date cannot be before start date")
        }

        val dates: Set<LocalDate> = IntStream.iterate(0) { i: Int -> i + 1 }
                .limit(numOfDaysBetween)
                .mapToObj<LocalDate> { i: Int -> start.plusDays(i.toLong()) }
                .collect(Collectors.toSet())

        val cookingEvents = cookingEventRepository.findAll(dateIn(dates))

        return dates.map { it to cookingEvents.find { x -> x.date == it } }.toMap()
    }

    fun findAllInRangeOrCreate(start: LocalDate, end: LocalDate): List<CookingEvent> {
        val optionalCookingEvents: Map<LocalDate, CookingEvent?> = findAllInRange(start, end)
        return optionalCookingEvents.map { it.value ?: create(it.key) }
    }

    fun getDryerSuggestions(id: Long): List<User> {
        // Get cooking event
        val cookingEvent = findById(id)

        // Initialise empty list
        val pastEventsThisWeek: MutableList<CookingEvent> = mutableListOf()

        // Add past events of this week
        val startDate = cookingEvent.date.minusDays((cookingEvent.date.dayOfWeek.value - 1).toLong())
        pastEventsThisWeek.addAll(findAllInRangeOrCreate(startDate, cookingEvent.date))

        // Get all enrolled users, except for the cook and users that are enrolled 0 times
        val enrolledUsers = cookingEvent.enrollments.filter { it.user != cookingEvent.cook && it.amount > 0 }.map { it.user }

        // Initialise empty map to keep track how many times a user has been dryer
        val map: MutableMap<User, Int> = mutableMapOf()

        // Initialise map with 0 values
        enrolledUsers.forEach { map[it] = 0 }

        // Check how many times users have been dryer
        pastEventsThisWeek.forEach {
            if (it.dryer1 != null) {
                if (enrolledUsers.contains(it.dryer1!!)) {
                    map[it.dryer1!!] = map[it.dryer1!!]!! + 1
                }
            }
            if (it.dryer2 != null) {
                if (enrolledUsers.contains(it.dryer2!!)) {
                    map[it.dryer2!!] = map[it.dryer2!!]!! + 1
                }
            }
        }

        // Sort the map by how many times someone has been dryer and then on id (hierarchy)
        // and then convert to list of users
        val sortedList = map.toList().sortedWith(compareBy({ it.second }, {-it.first.id})).map { it.first }

        // Return the first 2 found
        return sortedList.stream().limit(2).toList()
    }

    fun create(date: LocalDate): CookingEvent {
        if (findByDate(date) != null) {
            throw NotValidException("CookingEvent for this date already exists")
        }
        return cookingEventRepository.save(CookingEvent(date))
    }

    fun update(id: Long, writeDto: CookingEventWriteDto): CookingEvent {
        writeDto.assertValid()

        val cookingEvent = findById(id)

        // Also update the transaction if it exists
        cookingEvent.transaction?.let { transactionService.updateEnrollments(it, writeDto.enrollments!!) }

        return updateEnrollments(cookingEvent, writeDto.enrollments!!)
    }

    fun updateEnrollments(cookingEvent: CookingEvent, writeDtoList: List<EnrolledWriteDto?>): CookingEvent {
        // Make sure no enrollments are lost
        if (cookingEvent.enrollments.size > writeDtoList.size) {
            throw NotValidException("Amount of enrollments cannot decrease in size")
        }

        val currentUser = userService.getCurrentUser()

        // Check if the current user is the cook
        if (cookingEvent.cook != currentUser) {
            throw ForbiddenException("You are not allowed to update a cookingEvent if you are not the cook")
        }

        // Delete all old enrollments
        cookingEnrolledService.deleteAll(cookingEvent.enrollments)
        cookingEvent.enrollments.clear()

        // Make sure the deletion has happened before creating new enrollments
        entityManager.flush()

        // Create new enrollments
        val enrollments = writeDtoList.map { cookingEnrolledService.create(userService.findById(it!!.user!!), it.amount!!, cookingEvent) }

        // Add the enrollments to the cooking event
        cookingEvent.enrollments.addAll(enrollments)

        // Update the points for the cook
        cookingEvent.updatePointsForCook()

        // Create action
        cookingEvent.actions.add(cookingActionService.create(cookingEvent, currentUser, "${currentUser.username} past inschrijvingen aan"))

        // Remove yourself as cook if you are no longer enrolled
        if (!cookingEvent.isEnrolled(currentUser.id)) {
            return unEnrollAsCook(cookingEvent, currentUser)
        }

        return cookingEventRepository.save(cookingEvent)
    }

    fun updateDryers(id: Long, writeDto: CookingDryersWriteDto): CookingEvent {
        writeDto.assertValid()

        val cookingEvent = findById(id)

        val currentUser = userService.getCurrentUser()

        // Check if the current user is the cook or admin
        if (cookingEvent.cook != currentUser && !currentUser.getRoles().contains(Roles.ADMIN)) {
            throw ForbiddenException("You are not allowed to update the dryers!")
        }

        val dryer1 = if (writeDto.dryer1 == null) null else userService.findById(writeDto.dryer1)
        val dryer2 = if (writeDto.dryer2 == null) null else userService.findById(writeDto.dryer2)

        if (dryer1 != null) {
            if (!cookingEvent.isEnrolled(dryer1.id)) {
                throw NotValidException("User with id ${dryer1.id} cannot be dryer since the user is not enrolled")
            }
            if (dryer1 == cookingEvent.cook) {
                throw NotValidException("You cannot be dryer if you are cook")
            }
        }
        if (dryer2 != null) {
            if (!cookingEvent.isEnrolled(dryer2.id)) {
                throw NotValidException("User with id ${dryer2.id} cannot be dryer since the user is not enrolled")
            }
            if (dryer2 == cookingEvent.cook) {
                throw NotValidException("You cannot be dryer if you are cook")
            }
        }

        cookingEvent.dryer1 = dryer1
        cookingEvent.dryer2 = dryer2

        return cookingEventRepository.save(cookingEvent)
    }

    fun enroll(id: Long, amount: Int): CookingEvent {
        val currentUser: User = userService.getCurrentUser()
        val cookingEvent: CookingEvent = findById(id)
        return enroll(cookingEvent, amount, currentUser)
    }

    fun enroll(cookingEvent: CookingEvent, amount: Int, currentUser: User): CookingEvent {
        if (amount < 0) {
            throw NotValidException("Amount cannot be negative")
        }

        if (!currentUser.getRoles().contains(Roles.ADMIN)) {
            if (cookingEvent.date.isBefore(LocalDate.now()) && cookingEvent.cook != currentUser) {
                throw ForbiddenException("You are not allowed to enroll in the past")
            }
        }

        // Try to find the enrollment of this user
        val enrollment: CookingEnrolled? = cookingEvent.enrollments.find { it.user == currentUser }

        if (enrollment == null) {
            // Create new enrollment if none was present
            cookingEvent.enrollments.add(cookingEnrolledService.create(currentUser, amount, cookingEvent))
        } else {
            // Update enrollment if present
            cookingEvent.enrollments.add(cookingEnrolledService.update(enrollment, amount))
        }

        if (amount > 0) {
            // Create action
            cookingEvent.actions.add(cookingActionService.create(cookingEvent, currentUser, "${currentUser.username} schrijft zich ${amount}x in"))
        } else {
            // If we are un-enrolling, but we were cook, we need to un-enroll as cook
            if (cookingEvent.cook == currentUser) {
                unEnrollAsCook(cookingEvent, currentUser)
            } else {
                // Create action
                cookingEvent.actions.add(cookingActionService.create(cookingEvent, currentUser, "${currentUser.username} schrijft zich uit"))
            }
        }

        // Update the points for the cook
        cookingEvent.updatePointsForCook()

        // Also update the transaction if it exists
        cookingEvent.transaction?.let { transactionService.updateEnrollments(it, toEnrolledWriteDtoList(cookingEvent.enrollments)) }

        return cookingEventRepository.save(cookingEvent)
    }

    fun enrollAsCook(id: Long): CookingEvent {
        val cookingEvent = findById(id)
        val currentUser = userService.getCurrentUser()

        if (cookingEvent.cook != null) {
            throw ForbiddenException("${cookingEvent.cook?.username} is already enrolled as cook")
        }

        cookingEvent.cook = currentUser

        // Check if user is not yet enrolled
        val enrollment = cookingEvent.enrollments.find { it.user == currentUser }
        if (enrollment == null || enrollment.amount == 0) {
            // Make sure we are enrolled at least one time
            if (enrollment == null) {
                cookingEvent.enrollments.add(cookingEnrolledService.create(currentUser, 1, cookingEvent))
            } else {
                cookingEvent.enrollments.add(cookingEnrolledService.update(enrollment, 1))
            }
            // Update the points for the cook
            cookingEvent.updatePointsForCook()
        }

        // Create action
        cookingEvent.actions.add(cookingActionService.create(cookingEvent, currentUser, "${currentUser.username} schrijft zich in als kok"))

        return cookingEventRepository.save(cookingEvent)
    }

    fun unEnrollAsCook(id: Long): CookingEvent {
        val cookingEvent = findById(id)
        return unEnrollAsCook(cookingEvent)
    }

    fun unEnrollAsCook(cookingEvent: CookingEvent): CookingEvent {
        val currentUser = userService.getCurrentUser()
        return unEnrollAsCook(cookingEvent, currentUser)
    }

    fun unEnrollAsCook(cookingEvent: CookingEvent, currentUser: User): CookingEvent {
        if (cookingEvent.cook == null) {
            throw NotValidException("There is no cook to un-enroll")
        }

        if (!currentUser.getRoles().contains(Roles.ADMIN)) {
            if (cookingEvent.cook != currentUser) {
                throw ForbiddenException("You are not enrolled as cook, so you are not allowed to un-enroll")
            }
        }

        cookingEvent.cook = null

        // If we created a transaction while we were cook, delete it
        if (cookingEvent.transaction != null) {
            transactionService.delete(cookingEvent.transaction!!.id)
            cookingEvent.transaction = null
        }

        // Create action
        cookingEvent.actions.add(cookingActionService.create(cookingEvent, currentUser, "${currentUser.username} schrijft zich uit als kok"))

        return cookingEventRepository.save(cookingEvent)
    }

    fun countCookingPointsForCook(userId: Long): Int {
        return cookingEventRepository.sumPointsForCookWhereCookIdEquals(userId) ?: 0
    }

    fun removeTransaction(cookingEvent: CookingEvent) {
        cookingEvent.transaction = null
    }

    private fun toEnrolledWriteDtoList(enrollments: Set<CookingEnrolled>): List<EnrolledWriteDto> {
        return enrollments.map { CookingEnrolledWriteDto(it.user.id, it.amount) }
    }
}