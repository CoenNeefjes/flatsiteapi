package nl.dirtya.flatsiteapi.model.cooking.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.collectionNotNullOrContainsNull

/**
 * WriteDto than can only be used by the cook of a cookingEvent
 */

class CookingEventWriteDto(
        val enrollments: List<CookingEnrolledWriteDto?>?
) {

    fun assertValid() {
        collectionNotNullOrContainsNull(enrollments, "Enrollments")
        enrollments!!.forEach { it!!.assertValid() }
    }

}