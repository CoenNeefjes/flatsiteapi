package nl.dirtya.flatsiteapi.model.cooking.service

import nl.dirtya.flatsiteapi.model.cooking.api.CookingAction
import nl.dirtya.flatsiteapi.model.cooking.api.CookingActionRepository
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class CookingActionService
@Autowired constructor(
    private val cookingActionRepository: CookingActionRepository
){

    fun create(cookingEvent: CookingEvent, user: User, message: String): CookingAction {
        return cookingActionRepository.save(CookingAction(user, message, cookingEvent))
    }


}