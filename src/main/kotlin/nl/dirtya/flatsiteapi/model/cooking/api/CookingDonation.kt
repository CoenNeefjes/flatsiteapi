package nl.dirtya.flatsiteapi.model.cooking.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

/**
 * Represents a transfer of cooking points from one use to another user
 */

@Entity
@ToString
class CookingDonation(@ManyToOne val createdBy: User, val amount: Int, @ManyToOne val recipient: User): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    val createdOn: LocalDate = LocalDate.now()

}