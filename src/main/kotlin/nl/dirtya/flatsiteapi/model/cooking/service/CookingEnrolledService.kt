package nl.dirtya.flatsiteapi.model.cooking.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEnrolled
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEnrolledRepository
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class CookingEnrolledService
@Autowired constructor(
        private val cookingEnrolledRepository: CookingEnrolledRepository
) {

    fun findAll(): List<CookingEnrolled> {
        return cookingEnrolledRepository.findAll()
    }

    fun findById(id: Long): CookingEnrolled {
        return cookingEnrolledRepository.findById(id)
                .orElseThrow { NotFoundException("Enrollment with id $id could not be found") }
    }

    fun create(user: User, amount: Int, event: CookingEvent): CookingEnrolled {
        if (amount < 0) {
            throw NotValidException("Cannot create enrollment with a negative amount")
        }
        return cookingEnrolledRepository.save(CookingEnrolled(user, amount, event))
    }

    fun update(enrollment: CookingEnrolled, amount: Int): CookingEnrolled {
        if (amount < 0) {
            throw NotValidException("Amount cannot be negative")
        }
        enrollment.amount = amount
        return cookingEnrolledRepository.save(enrollment)
    }

    fun deleteAll(enrollments: Set<CookingEnrolled>) {
        cookingEnrolledRepository.deleteAll(enrollments)
    }

    fun sumEnrollmentsWhereCookWasPresent(userId: Long): Int {
        return cookingEnrolledRepository.sumAmountWhereUserIdEqualsAndCookIsPresent(userId) ?: 0
    }

    fun countEnrollmentsWhereCookWasPresent(userId: Long): Int {
        return cookingEnrolledRepository.countAmountWhereUserIdEqualsAndCookIsPresentAndUserIsNotCook(userId) ?: 0
    }

}