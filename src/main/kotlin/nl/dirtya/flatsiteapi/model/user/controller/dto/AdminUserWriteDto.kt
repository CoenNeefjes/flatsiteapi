package nl.dirtya.flatsiteapi.model.user.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.util.ValidationUtils

class AdminUserWriteDto(
        val username: String?,
        val password: String?,
        val roles: List<Roles?>?,
        val active: Boolean?
) {

    private fun assertValidBase() {
        ValidationUtils.stringNotNullEmptyMaxLength(username, "Username", 255)
        if (password != null) {
            ValidationUtils.stringNotNullEmptyMaxLength(password, "Password", 255)
        }
        ValidationUtils.collectionNotNullOrEmptyOrContainsNull(roles, "Roles")
        ValidationUtils.objectNotNull(active, "Active")
    }

    fun assertValidCreate() {
        assertValidBase()
    }

    fun assertValidUpdate() {
        assertValidBase()
    }
}