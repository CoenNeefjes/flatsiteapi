package nl.dirtya.flatsiteapi.model.user.service

import nl.dirtya.flatsiteapi.model.album.service.AlbumService
import nl.dirtya.flatsiteapi.model.image.service.ImageFileService
import nl.dirtya.flatsiteapi.model.image.service.ImageService
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile

@Service class UserImageUploadService
@Autowired constructor(
        private val userService: UserService,
        private val imageFileService: ImageFileService,
        private val albumService: AlbumService,
        private val imageService: ImageService
) {

    fun upload(file: MultipartFile): User {
        val album = albumService.findByName("profielfoto's")
        val user = userService.getCurrentUser()
        val extension = file.originalFilename!!.split('.').last()
        val image = imageService.create(album, extension)

        user.image?.let { imageFileService.delete(it.id) }
        imageFileService.save(file, image, 300)

        return userService.updateImage(user, image)
    }

}