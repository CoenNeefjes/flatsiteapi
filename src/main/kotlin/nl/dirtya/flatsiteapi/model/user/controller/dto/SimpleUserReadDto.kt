package nl.dirtya.flatsiteapi.model.user.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.User

class SimpleUserReadDto(user: User) {

    val id: Long = user.id
    val username: String = user.username
    val image: Long? = user.image?.id

}