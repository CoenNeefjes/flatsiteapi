package nl.dirtya.flatsiteapi.model.user.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.AutoEnroll
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.api.User

class UserReadDto(user: User) {
    val id: Long = user.id
    val username: String = user.username
    val roles: List<Roles> = user.getRoles()
    val image: Long? = user.image?.id
    val active: Boolean = user.active
    val autoEnroll: AutoEnroll = user.autoEnroll
}