package nl.dirtya.flatsiteapi.model.user.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.api.UserRepository
import nl.dirtya.flatsiteapi.model.user.api.UserRepository.Companion.activeEquals
import nl.dirtya.flatsiteapi.model.user.api.UserRepository.Companion.usernameEquals
import nl.dirtya.flatsiteapi.model.user.controller.dto.AdminUserWriteDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserWriteDto
import nl.dirtya.flatsiteapi.security.CurrentPrincipalService
import nl.dirtya.flatsiteapi.security.utils.PasswordEncoder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class UserService
@Autowired constructor(
        private val userRepository: UserRepository,
        private val currentPrincipalService: CurrentPrincipalService
){

    fun countAll(): Long {
        return userRepository.count()
    }

    fun findAll(): List<User> {
        return userRepository.findAll()
    }

    fun findById(id: Long): User {
        return userRepository.findById(id).orElseThrow { NotFoundException("User with id '$id' could not be found") }
    }

    fun findByUsername(username: String): User {
        return userRepository.findOne(usernameEquals(username))
                .orElseThrow { NotFoundException("User with name '$username' could not be found") }
    }

    fun findByUsernameOptional(username: String): User? {
        return userRepository.findOne(usernameEquals(username)).orElse(null)
    }

    fun findByActive(active: Boolean): List<User> {
        return userRepository.findAll(activeEquals(active))
    }

    fun getCurrentUser(): User {
        return findById(currentPrincipalService.getCurrentPrincipalId())
    }

    fun currentUserIsAdmin(): Boolean {
        return getCurrentUser().getRoles().contains(Roles.ADMIN)
    }

    fun create(writeDto: UserWriteDto): User {
        writeDto.assertValidCreate()

        if (findByUsernameOptional(writeDto.username!!.trim()) != null) {
            throw NotValidException("User with username ${writeDto.username} already exists")
        }

        return userRepository.save(User(writeDto.username, writeDto.password!!))
    }

    fun adminCreate(writeDto: AdminUserWriteDto): User {
        if (!getCurrentUser().getRoles().contains(Roles.ADMIN)) {
            throw ForbiddenException("Only admins are allowed to create new users")
        }

        writeDto.assertValidCreate()

        if (findByUsernameOptional(writeDto.username!!.trim()) != null) {
            throw NotValidException("User with username ${writeDto.username} already exists")
        }

        return userRepository.save(User(writeDto.username, writeDto.password!!, (writeDto.roles as List<Roles>?)!!))
    }

    fun updateMe(writeDto: UserWriteDto): User {
        return update(getCurrentUser(), writeDto)
    }

    fun update(id: Long, writeDto: UserWriteDto): User {
        val user = findById(id)
        return update(user, writeDto)
    }

    fun update(user: User, writeDto: UserWriteDto): User {
        writeDto.assertValidUpdate()

        if (writeDto.username != user.username) {
            if (findByUsernameOptional(writeDto.username!!) != null) {
                throw NotValidException("User with username ${writeDto.username} already exists")
            }
            user.username = writeDto.username
        }

        if (writeDto.password != null) {
            if (!PasswordEncoder.instance.matches(writeDto.oldPassword, user.password)) {
                throw NotValidException("Wrong password")
            }
            user.password = PasswordEncoder.instance.encode(writeDto.password)
        }

        if (writeDto.autoEnroll != null) {
            user.autoEnroll = writeDto.autoEnroll
        }

        return userRepository.save(user)
    }

    fun adminUpdate(id: Long, writeDto: AdminUserWriteDto): User {
        if (!getCurrentUser().getRoles().contains(Roles.ADMIN)) {
            throw ForbiddenException("Only admins are allowed to create new users")
        }

        writeDto.assertValidUpdate()

        val user = findById(id)

        user.username = writeDto.username!!
        if (writeDto.password != null) {
            user.password = PasswordEncoder.instance.encode(writeDto.password)
        }
        user.setRoles((writeDto.roles as List<Roles>?)!!)
        user.active = writeDto.active!!

        return userRepository.save(user)
    }

    fun updateImage(user: User, image: Image?): User {
        user.image = image
        return userRepository.save(user)
    }
}