package nl.dirtya.flatsiteapi.model.user.controller.dto

import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.user.api.AutoEnroll
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullEmptyMaxLength

class UserWriteDto(
        val username: String?,
        val password: String?,
        val passwordRepeat: String?,
        val oldPassword: String?,
        val autoEnroll: AutoEnroll?
) {

    private fun assertValidBase() {
        stringNotNullEmptyMaxLength(username, "Username", 255)
    }

    fun assertValidCreate() {
        assertValidBase()

        stringNotNullEmptyMaxLength(password, "Password", 255)
        stringNotNullEmptyMaxLength(passwordRepeat, "PasswordRepeat", 255)

        if (!password.equals(passwordRepeat)) {
            throw NotValidException("Passwords must match")
        }
    }

    fun assertValidUpdate() {
        assertValidBase()

        if (password != null) {
            stringNotNullEmptyMaxLength(password, "Password", 255)
            stringNotNullEmptyMaxLength(passwordRepeat, "PasswordRepeat", 255)
            stringNotNullEmptyMaxLength(oldPassword, "OldPassword", 255)

            if (password != passwordRepeat) {
                throw NotValidException("Passwords must match")
            }
        }
    }
}