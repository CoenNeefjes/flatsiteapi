package nl.dirtya.flatsiteapi.model.user.api

/**
 * Authorities based on the Roles enums. They need to be final to be used in annotations.
 */
class Authorities {

    companion object {

        const val USER: String = "USER"
        const val ADMIN: String = "ADMIN"
        const val FEUT: String = "FEUT"
        const val BEER_SCALE_ADMIN: String = "BEER_SCALE_ADMIN"

        const val IS_USER: String = "hasAuthority('$USER')"
        const val IS_ADMIN: String = "hasAuthority('$ADMIN')"
        const val IS_FEUT: String = "hasAuthority('$FEUT')"
        const val IS_BEER_SCALE_ADMIN: String = "hasAuthority('$BEER_SCALE_ADMIN')"

    }

}