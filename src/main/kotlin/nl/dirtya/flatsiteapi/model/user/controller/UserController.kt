package nl.dirtya.flatsiteapi.model.user.controller

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.controller.dto.AdminUserWriteDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserReadDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.UserWriteDto
import nl.dirtya.flatsiteapi.model.user.service.UserImageUploadService
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/users")
@Transactional
class UserController
@Autowired constructor(
        private val userService: UserService,
        private val userImageUploadService: UserImageUploadService
) {

    @GetMapping("/active")
    fun findActive(): List<SimpleUserReadDto> {
        return userService.findByActive(true).map(::SimpleUserReadDto)
    }

    @GetMapping("/me")
    fun findMe(): UserReadDto {
        return UserReadDto(userService.getCurrentUser())
    }

    @GetMapping("/admin")
    fun adminFindAll(): List<UserReadDto> {
        val currentUser = userService.getCurrentUser()
        if (!currentUser.getRoles().contains(Roles.ADMIN)) { throw ForbiddenException() }
        return userService.findAll().map(::UserReadDto)
    }

    @GetMapping("/admin/{id}")
    fun adminFindById(@PathVariable(name = "id") id: Long): UserReadDto {
        val currentUser = userService.getCurrentUser()
        if (!currentUser.getRoles().contains(Roles.ADMIN)) { throw ForbiddenException() }
        return UserReadDto(userService.findById(id))
    }

    @PostMapping("/admin")
    fun adminCreate(@RequestBody writeDto: AdminUserWriteDto): UserReadDto {
        return UserReadDto(userService.adminCreate(writeDto))
    }

    @PutMapping("/me")
    fun updateMe(@RequestBody writeDto: UserWriteDto): UserReadDto {
        return UserReadDto(userService.updateMe(writeDto))
    }

    @PutMapping("/admin/{id}")
    fun adminUpdate(@PathVariable(name = "id") id: Long, @RequestBody writeDto: AdminUserWriteDto): UserReadDto {
        return UserReadDto(userService.adminUpdate(id, writeDto))
    }

    @PutMapping("/me/image")
    fun updateMyImage(@RequestParam("file") file: MultipartFile): UserReadDto {
        return UserReadDto(userImageUploadService.upload(file))
    }

}