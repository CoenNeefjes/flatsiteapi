package nl.dirtya.flatsiteapi.model.user.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInput
import nl.dirtya.flatsiteapi.model.beerinput.api.BeerInputEnrolled
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEnrolled
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.image.api.Image
import nl.dirtya.flatsiteapi.model.notification.api.Notification
import nl.dirtya.flatsiteapi.model.shuttledrunktest.api.ShuttleDrunkTestResult
import nl.dirtya.flatsiteapi.model.transaction.api.Transaction
import nl.dirtya.flatsiteapi.model.transaction.api.TransactionEnrolled
import nl.dirtya.flatsiteapi.model.vote.api.Vote
import nl.dirtya.flatsiteapi.security.utils.PasswordEncoder
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import java.util.stream.Collectors
import javax.persistence.*
import kotlin.collections.ArrayList
import kotlin.streams.toList

@Entity(name = "user_model")
@ToString(exclude = [
    "password",
    "cookingEvents",
    "cookingEnrollments",
    "transactions",
    "transactionEnrollments",
    "images",
    "beerInputs",
    "beerInputEnrolleds"
])
class User: Identifiable {

    constructor(username: String, password: String, roles: List<Roles>) {
        this.username = username
        this.password = PasswordEncoder.instance.encode(password)
        this.roles = roles.stream().map { it.toString() }.collect(Collectors.joining(","))
        this.active = true
    }

    constructor(username: String, password: String) : this(username, password, listOf(Roles.USER))

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    var username: String

    var password: String

    var roles: String

    var active: Boolean

    @Embedded
    var autoEnroll: AutoEnroll = AutoEnroll()
        // Custom getter that always returns an object, needed since hibernate makes this null
        // when all of its properties are null. Even though the code states that it cannot be null.
        get() = if (field == null) AutoEnroll() else field

    @OneToOne
    @JoinColumn(name = "image_id", referencedColumnName = "id")
    var image: Image? = null

    @OneToMany(mappedBy = "cook")
    val cookingEvents: List<CookingEvent> = ArrayList()

    @OneToMany(mappedBy = "user")
    val cookingEnrollments: List<CookingEnrolled> = ArrayList()

    @OneToMany(mappedBy = "owner")
    private val transactions: List<Transaction> = ArrayList()

    @OneToMany(mappedBy = "user")
    private val transactionEnrollments: List<TransactionEnrolled> = ArrayList()

    @OneToMany(mappedBy = "createdBy")
    private val images: List<Image> = ArrayList()

    @OneToMany(mappedBy = "createdBy")
    private val beerInputs: List<BeerInput> = ArrayList()

    @OneToMany(mappedBy = "user")
    private val beerInputEnrollments: List<BeerInputEnrolled> = ArrayList()

    @OneToMany(mappedBy = "createdBy")
    private val votes: List<Vote> = ArrayList()

    @OneToMany(mappedBy = "user")
    private val notifications: List<Notification> = ArrayList()

    @ManyToMany(mappedBy = "usersAgree")
    private val votesAgree: List<Vote> = ArrayList()

    @ManyToMany(mappedBy = "usersDisagree")
    private val votesDisagree: List<Vote> = ArrayList()

    @OneToMany(mappedBy = "user")
    private val shuttleDrunkTestResults: List<ShuttleDrunkTestResult> = ArrayList()

    fun getRoles(): List<Roles> {
        return roles.split(",").stream().map { Roles.valueOf(it) }.toList()
    }

    fun setRoles(roles: List<Roles>) {
        this.roles = roles.stream().map { it.toString() }.collect(Collectors.joining(","))
    }
}