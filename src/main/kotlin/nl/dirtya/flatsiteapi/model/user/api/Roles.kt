package nl.dirtya.flatsiteapi.model.user.api

enum class Roles {
    USER,
    ADMIN,
    FEUT,
    BEER_SCALE_ADMIN
}