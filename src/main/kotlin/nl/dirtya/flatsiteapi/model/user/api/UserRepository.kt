package nl.dirtya.flatsiteapi.model.user.api

import au.com.console.jpaspecificationdsl.equal
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface UserRepository: JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    companion object {

        fun usernameEquals(input: String): Specification<User> {
            return User::username.equal(input)
        }

        fun activeEquals(input: Boolean): Specification<User> {
            return User::active.equal(input)
        }

    }

}