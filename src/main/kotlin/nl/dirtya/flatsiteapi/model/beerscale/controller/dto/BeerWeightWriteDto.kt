package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.bigDecimalNotNullOrNegative
import java.math.BigDecimal

class BeerWeightWriteDto(val weight: BigDecimal?) {

    fun assertValid() {
        bigDecimalNotNullOrNegative(weight, "weight")
    }

}