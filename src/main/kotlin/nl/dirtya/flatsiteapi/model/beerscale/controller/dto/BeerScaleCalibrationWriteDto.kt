package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.bigDecimalNotNullOrNegative
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import java.math.BigDecimal

class BeerScaleCalibrationWriteDto(val averagereading: Int?, val weight: BigDecimal?) {

    fun assertValid() {
        objectNotNull(averagereading, "averagereading")
        bigDecimalNotNullOrNegative(weight, "weight")
    }

}