package nl.dirtya.flatsiteapi.model.beerscale.controller

import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.*
import nl.dirtya.flatsiteapi.model.beerscale.service.BeerFridgeArduinoService
import nl.dirtya.flatsiteapi.model.beerscale.service.BeerScaleService
import nl.dirtya.flatsiteapi.model.user.api.Authorities
import nl.dirtya.flatsiteapi.model.user.api.Authorities.Companion.IS_ADMIN
import nl.dirtya.flatsiteapi.model.user.api.Authorities.Companion.IS_BEER_SCALE_ADMIN
import nl.dirtya.flatsiteapi.model.user.api.Roles
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal

@RestController
@RequestMapping("/beer-scale")
@Transactional
class BeerScaleController @Autowired constructor(private val beerScaleService: BeerScaleService,
                                                 private val beerFridgeArduinoService: BeerFridgeArduinoService) {

    /* For flatsite */

    //Probably not needed later
    @GetMapping("/measurement/raw")
    fun getLatestReading(): Int {
        return beerScaleService.getReading()
    }

    @GetMapping("/measurement/kg")
    fun getLatestMeasurement(): BigDecimal {
        return beerScaleService.getReadingKg()
    }

    @GetMapping("/measurement/beers")
    fun getLatestBeers(): Int {
        return beerScaleService.getReadingBeers()
    }

    @GetMapping("/calibration")
    fun getCalibrationPoints(): List<BeerScaleCalibrationReadDto> {
        return beerScaleService.getAllCalibrationPoints().map(::BeerScaleCalibrationReadDto)
    }

    @DeleteMapping("/calibration")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun deleteAllCalibrationPoints() {
        beerScaleService.removeAllCalibrationPoints()
    }

    @GetMapping("/beerweight")
    fun getBeerWeight(): BigDecimal {
        return beerScaleService.getBeerWeight()
    }

    @PostMapping("/beerweight")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun editBeerWeight(@RequestBody writeDto: BeerWeightWriteDto) {
        beerScaleService.replaceBeerWeight(writeDto)
    }

    /* For scale */

    @PostMapping("/measurement")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun createNewMeasurement(@RequestBody writeDto: BeerScaleMeasurementWriteDto) {
        beerScaleService.replaceMeasurement(writeDto)
    }

    @PostMapping("/calibration")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun createNewCalibrationPoint(@RequestBody writeDto: BeerScaleCalibrationWriteDto) {
        beerScaleService.addCalibrationPoint(writeDto)
    }

    @PutMapping("/scale/tare")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleTare() {
        beerFridgeArduinoService.scaleTare()
    }

    @PostMapping("/scale/addcalibration")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleAddCalibration(@RequestBody writeDto: BeerScaleNewCalibrationWriteDto) {
        beerFridgeArduinoService.scaleAddCalibration(writeDto)
    }

    @PutMapping("/scale/changeendpoint")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleChangeEndpoints(@RequestBody writeDto: BeerScaleChangeEndpointsWriteDto) {
        beerFridgeArduinoService.scaleChangeEndpoints(writeDto)
    }

    @PutMapping("/scale/changewifi")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleChangeWifi(@RequestBody writeDto: BeerScaleChangeWifiWriteDto) {
        beerFridgeArduinoService.scaleChangeWifi(writeDto)
    }

    @PutMapping("/scale/changeapi")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleChangeApi(@RequestBody writeDto: BeerScaleChangeApiWriteDto) {
        beerFridgeArduinoService.scaleChangeApi(writeDto)
    }

    @PutMapping("/scale/changedelay")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleChangeDelay(@RequestBody writeDto: BeerScaleChangeDelayWriteDto) {
        beerFridgeArduinoService.scaleChangeDelay(writeDto)
    }

    @PutMapping("/scale/hostname")
    @PreAuthorize("$IS_BEER_SCALE_ADMIN || $IS_ADMIN")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun scaleChangeHostName(@RequestBody writeDto: BeerScaleChangeHostNameWriteDto) {
        beerFridgeArduinoService.scaleChangeHostName(writeDto)
    }


}