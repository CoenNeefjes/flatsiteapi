package nl.dirtya.flatsiteapi.model.beerscale.api

import au.com.console.jpaspecificationdsl.equal
import au.com.console.jpaspecificationdsl.greaterThan
import au.com.console.jpaspecificationdsl.isNotNull
import au.com.console.jpaspecificationdsl.notEqual
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BeerScaleMeasurementRepository: JpaRepository<BeerScaleMeasurement, Long>, JpaSpecificationExecutor<BeerScaleMeasurement> {

    companion object {

        fun baseSpecificationMeasurement(): Specification<BeerScaleMeasurement> {
            return BeerScaleMeasurement::id.isNotNull()
        }

    }
}