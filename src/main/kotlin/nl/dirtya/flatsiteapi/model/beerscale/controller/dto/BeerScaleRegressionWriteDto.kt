package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import java.math.BigDecimal

class BeerScaleRegressionWriteDto(val slope: BigDecimal?, val intercept: BigDecimal?) {

    fun assertValid() {
        objectNotNull(slope, "slope")
        objectNotNull(intercept, "intercept")
    }
}