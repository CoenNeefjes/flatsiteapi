package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullOrEmpty

class BeerScaleChangeEndpointsWriteDto(val measureendpoint: String?, val calibrationendpoint: String?) {

    fun assertValid() {
        stringNotNullOrEmpty(measureendpoint, "Measure endpoint")
        stringNotNullOrEmpty(calibrationendpoint, "Calibration Endpoint")
    }

}