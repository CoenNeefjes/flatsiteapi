package nl.dirtya.flatsiteapi.model.beerscale.service

import nl.dirtya.flatsiteapi.config.AppProperties
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.beerscale.api.*
import nl.dirtya.flatsiteapi.model.beerscale.api.BeerScaleMeasurementRepository.Companion.baseSpecificationMeasurement
import nl.dirtya.flatsiteapi.model.beerscale.api.BeerScaleRegressionRepository.Companion.baseSpecificationRegression
import nl.dirtya.flatsiteapi.model.beerscale.api.BeerWeightRepository.Companion.baseSpecificationWeight
import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.*
import nl.dirtya.flatsiteapi.model.notification.api.NotificationType
import nl.dirtya.flatsiteapi.model.notification.service.NotificationService
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode

@Service class BeerScaleService
@Autowired constructor(
        private val beerScaleMeasurementRepository: BeerScaleMeasurementRepository,
        private val beerScaleCalibrationRepository: BeerScaleCalibrationRepository,
        private val beerWeightRepository: BeerWeightRepository,
        private val beerScaleRegressionRepository: BeerScaleRegressionRepository,
        private val notificationService: NotificationService,
        private val userService: UserService,
        private val appProperties: AppProperties
) {

    fun findOneMeasurement(): BeerScaleMeasurement {
        return beerScaleMeasurementRepository.findOne(baseSpecificationMeasurement()).orElseThrow{NotFoundException("There is no measurement yet")}
    }

    fun findOneBeerWeight(): BeerWeight {
        return beerWeightRepository.findOne(baseSpecificationWeight()).orElseThrow{NotFoundException("There is no beer weight set")}
    }

    fun findOneRegression(): BeerScaleRegression {
        return beerScaleRegressionRepository.findOne(baseSpecificationRegression()).orElseThrow{NotFoundException("No beer scale regression is set")}
    }

    fun getReading(): Int {
        return findOneMeasurement().reading
    }

    fun getReadingKg(): BigDecimal {
        if (beerScaleCalibrationRepository.findAll().size < 2) {
            throw NotValidException("You need at least 2 calibration points to estimate the weight")
        }
        val reading = findOneMeasurement().reading
        val regression = findOneRegression()

        return predictWeightWithLinearRegression(Pair(regression.slope, regression.intercept), reading).setScale(5, RoundingMode.HALF_UP)
    }

    fun getReadingBeers(): Int {
        return (getReadingKg() / getBeerWeight()).setScale(0, RoundingMode.HALF_UP).intValueExact()
    }

    fun getBeerWeight(): BigDecimal {
        return findOneBeerWeight().weight
    }

    fun replaceBeerWeight(writeDto: BeerWeightWriteDto) {
        writeDto.assertValid()
        beerWeightRepository.deleteAll()
        beerWeightRepository.save(BeerWeight(writeDto.weight!!))
    }

    fun changeBeerScaleRegression(writeDto: BeerScaleRegressionWriteDto) {
        writeDto.assertValid()
        beerScaleRegressionRepository.deleteAll()
        beerScaleRegressionRepository.save(BeerScaleRegression(writeDto.slope!!, writeDto.intercept!!))
    }

    fun replaceBeerScaleRegression() {
        val regression = calculateLinearRegression()
        changeBeerScaleRegression(BeerScaleRegressionWriteDto(regression.first, regression.second))
    }

    fun replaceMeasurement(writeDto: BeerScaleMeasurementWriteDto) {
        writeDto.assertValid()
        beerScaleMeasurementRepository.deleteAll()
        beerScaleMeasurementRepository.save(BeerScaleMeasurement(writeDto.reading!!))

        createNotificationIfBeersLowerThanThreshold()
    }

    fun createNotificationIfBeersLowerThanThreshold() {
        try {
            if (getReadingBeers() < appProperties.beerfridge.threshold) {
                for (user in userService.findByActive(true)) {
                    if (user.getRoles().contains(Roles.FEUT)) {
                        notificationService.createSingleton(user, NotificationType.BEER_FRIDGE, "Bier bijvullen kutfeut")
                    }
                }
            }
        } catch (e : Exception) {
            // If we get any exception, we don't need to create the notification
        }
    }

    fun getAllCalibrationPoints(): List<BeerScaleCalibration> {
        return beerScaleCalibrationRepository.findAll()
    }

    fun removeAllCalibrationPoints() {
        beerScaleCalibrationRepository.deleteAll()
    }

    fun addCalibrationPoint(writeDto: BeerScaleCalibrationWriteDto) {
        writeDto.assertValid()
        beerScaleCalibrationRepository.save(BeerScaleCalibration(writeDto.averagereading!!, writeDto.weight!!))
        if (beerScaleCalibrationRepository.findAll().size >= 2) {
            replaceBeerScaleRegression()
        }
    }

    fun calculateLinearRegression(): Pair<BigDecimal,BigDecimal> {
        val datapoints = beerScaleCalibrationRepository.findAll()

        // First pass
        var sumx = 0f.toBigDecimal()
        var sumx2 = 0f.toBigDecimal()
        var sumy = 0f.toBigDecimal()
        for (datapoint in datapoints) {
            sumx += (datapoint.averagereading.toBigDecimal())
            sumx2 += ((datapoint.averagereading * datapoint.averagereading).toBigDecimal())
            sumy += (datapoint.weight)
        }
        val xbar = sumx.divide(datapoints.size.toBigDecimal(), 50, RoundingMode.HALF_UP)
        val ybar = sumy.divide(datapoints.size.toBigDecimal(), 50, RoundingMode.HALF_UP)

        // Second pass: compute summary statistics
        var xxbar = 0f.toBigDecimal()
        var yybar = 0f.toBigDecimal()
        var xybar = 0f.toBigDecimal()
        for (datapoint in datapoints) {
            xxbar += (datapoint.averagereading.toBigDecimal() - xbar) * (datapoint.averagereading.toBigDecimal() - xbar)
            yybar += (datapoint.weight - ybar) * (datapoint.weight - ybar)
            xybar += (datapoint.averagereading.toBigDecimal() - xbar) * (datapoint.weight - ybar)
        }
        val slope = xybar.divide(xxbar, 50, RoundingMode.HALF_UP)
        val intercept = ybar - slope * xbar

        return Pair(slope,intercept)
    }

    fun predictWeightWithLinearRegression(linearPair: Pair<BigDecimal, BigDecimal>, reading: Int): BigDecimal {
        val slope = linearPair.first
        val intercept = linearPair.second

        return slope * reading.toBigDecimal() + intercept
    }
}