package nl.dirtya.flatsiteapi.model.beerscale.api

import au.com.console.jpaspecificationdsl.isNotNull
import au.com.console.jpaspecificationdsl.notEqual
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BeerWeightRepository: JpaRepository<BeerWeight, Long>, JpaSpecificationExecutor<BeerWeight> {
    companion object {

        fun baseSpecificationWeight(): Specification<BeerWeight> {
            return BeerWeight::id.isNotNull()
        }

    }
}