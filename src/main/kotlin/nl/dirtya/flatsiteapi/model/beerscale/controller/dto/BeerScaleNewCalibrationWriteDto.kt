package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.bigDecimalNotNullOrNegative
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.floatMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import java.math.BigDecimal

class BeerScaleNewCalibrationWriteDto(val calibrationweight: Float?, val measuresnumber: Int?) {

    fun assertValid() {
        floatMinimumNotNull(calibrationweight, "Calibration Weight", 0f)
        integerMinimumNotNull(measuresnumber, "Measures Number", 1)
    }

}