package nl.dirtya.flatsiteapi.model.beerscale.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BeerScaleCalibrationRepository: JpaRepository<BeerScaleCalibration, Long>, JpaSpecificationExecutor<BeerScaleCalibration> {

}