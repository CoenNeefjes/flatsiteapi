package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullOrEmpty

class BeerScaleChangeWifiWriteDto(val ssid: String?, val password: String?) {

    fun assertValid() {
        stringNotNullOrEmpty(ssid, "SSID")
        stringNotNullOrEmpty(password, "Password")
    }

}