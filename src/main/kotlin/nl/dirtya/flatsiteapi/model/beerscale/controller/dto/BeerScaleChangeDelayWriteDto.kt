package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullOrEmpty

class BeerScaleChangeDelayWriteDto(val delay: Int?) {

    fun assertValid() {
        integerMinimumNotNull(delay, "Delay", 0)
    }

}