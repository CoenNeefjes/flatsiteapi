package nl.dirtya.flatsiteapi.model.beerscale.api

import au.com.console.jpaspecificationdsl.isNotNull
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BeerScaleHostNameRepository: JpaRepository<BeerScaleHostName, Long>, JpaSpecificationExecutor<BeerScaleHostName> {

    companion object {

        fun baseSpecificationHostName(): Specification<BeerScaleHostName> {
            return BeerScaleHostName::id.isNotNull()
        }

    }
}