package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull

class BeerScaleMeasurementWriteDto(val reading: Int?) {

    fun assertValid() {
        objectNotNull(reading, "reading")
    }

}