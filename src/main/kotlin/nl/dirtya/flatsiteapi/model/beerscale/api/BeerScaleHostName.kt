package nl.dirtya.flatsiteapi.model.beerscale.api

import lombok.ToString
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@ToString
class BeerScaleHostName(var hostname: String): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

}