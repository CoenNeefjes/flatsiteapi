package nl.dirtya.flatsiteapi.model.beerscale.api

import lombok.ToString
import nl.dirtya.flatsiteapi.util.Identifiable
import java.math.BigDecimal
import java.math.BigInteger
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
@ToString
class BeerScaleRegression(var slope: BigDecimal, var intercept: BigDecimal): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

}