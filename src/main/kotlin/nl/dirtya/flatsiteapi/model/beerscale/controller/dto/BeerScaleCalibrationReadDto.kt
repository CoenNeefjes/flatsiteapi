package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.model.beerscale.api.BeerScaleCalibration

class BeerScaleCalibrationReadDto(beerScaleCalibration: BeerScaleCalibration) {
    val averagereading = beerScaleCalibration.averagereading
    val weight = beerScaleCalibration.weight
}