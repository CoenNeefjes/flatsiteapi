package nl.dirtya.flatsiteapi.model.beerscale.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.integerMinimumNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullOrEmpty

class BeerScaleChangeApiWriteDto(val hostname: String?, val port: Int?) {

    fun assertValid() {
        stringNotNullOrEmpty(hostname, "Hostname")
        integerMinimumNotNull(port, "Port", 1)
    }

}