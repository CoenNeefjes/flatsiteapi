package nl.dirtya.flatsiteapi.model.beerscale.api

import au.com.console.jpaspecificationdsl.isNotNull
import au.com.console.jpaspecificationdsl.notEqual
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface BeerScaleRegressionRepository: JpaRepository<BeerScaleRegression, Long>, JpaSpecificationExecutor<BeerScaleRegression> {
    companion object {

        fun baseSpecificationRegression(): Specification<BeerScaleRegression> {
            return BeerScaleRegression::id.isNotNull()
        }

    }
}