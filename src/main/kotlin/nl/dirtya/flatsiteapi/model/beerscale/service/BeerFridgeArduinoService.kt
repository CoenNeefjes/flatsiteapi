package nl.dirtya.flatsiteapi.model.beerscale.service

import nl.dirtya.flatsiteapi.exception.InternalServerException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.beerscale.api.BeerScaleHostName
import nl.dirtya.flatsiteapi.model.beerscale.api.BeerScaleHostNameRepository
import nl.dirtya.flatsiteapi.model.beerscale.api.BeerScaleHostNameRepository.Companion.baseSpecificationHostName
import nl.dirtya.flatsiteapi.model.beerscale.controller.dto.*
import okhttp3.Response
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class BeerFridgeArduinoService
@Autowired constructor(
        private val beerScaleHostNameRepository: BeerScaleHostNameRepository,
        private val beerScalePostRequestService: BeerScalePostRequestService
) {

    /**
     * Tell the beer fridge ESP8266 to tare (set current weight as 0)
     */
    fun scaleTare() {
        correctlyHandleStatusCode(beerScalePostRequestService.sendPostRequest("tare", getHostname().hostname))
    }

    fun scaleAddCalibration(writeDto: BeerScaleNewCalibrationWriteDto) {
        writeDto.assertValid()
        val weight = writeDto.calibrationweight
        val measures = writeDto.measuresnumber
        correctlyHandleStatusCode(beerScalePostRequestService.sendPostRequest("kg $weight $measures", getHostname().hostname))
    }

    fun scaleChangeEndpoints(writeDto: BeerScaleChangeEndpointsWriteDto) {
        writeDto.assertValid()
        val measureendpoint = writeDto.measureendpoint
        val calibrationendpoint = writeDto.calibrationendpoint
        correctlyHandleStatusCode(beerScalePostRequestService.sendPostRequest("endpoint $measureendpoint $calibrationendpoint", getHostname().hostname))
    }

    fun scaleChangeWifi(writeDto: BeerScaleChangeWifiWriteDto) {
        writeDto.assertValid()
        val ssid = writeDto.ssid
        val pass = writeDto.password
        correctlyHandleStatusCode(beerScalePostRequestService.sendPostRequest("wifi $ssid $pass", getHostname().hostname))
    }

    fun scaleChangeApi(writeDto: BeerScaleChangeApiWriteDto) {
        writeDto.assertValid()
        val host = writeDto.hostname
        val port = writeDto.port
        correctlyHandleStatusCode(beerScalePostRequestService.sendPostRequest("api $host $port", getHostname().hostname))
    }

    fun scaleChangeDelay(writeDto: BeerScaleChangeDelayWriteDto) {
        writeDto.assertValid()
        val delay = writeDto.delay
        correctlyHandleStatusCode(beerScalePostRequestService.sendPostRequest("delay $delay", getHostname().hostname))
    }

    fun scaleChangeHostName(writeDto: BeerScaleChangeHostNameWriteDto) {
        writeDto.assertValid()
        beerScaleHostNameRepository.deleteAll()
        beerScaleHostNameRepository.save(BeerScaleHostName(writeDto.hostname!!))
    }

    fun correctlyHandleStatusCode(response: Response) {
        if(response.code != 200) {
            val responseCode = response.code
            val responseBody = response.body
            throw InternalServerException("Encountered error from beer scale. Error: $responseCode: $responseBody")
        }
        response.body?.close()
    }

    fun getHostname(): BeerScaleHostName {
        return beerScaleHostNameRepository.findOne(baseSpecificationHostName()).orElseThrow{ NotFoundException("There is no host name yet") }
    }

}
