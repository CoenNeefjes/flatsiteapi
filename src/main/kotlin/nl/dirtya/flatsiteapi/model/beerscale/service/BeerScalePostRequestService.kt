package nl.dirtya.flatsiteapi.model.beerscale.service

import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.util.concurrent.TimeUnit


@Service class BeerScalePostRequestService
@Autowired constructor(
) {
    fun sendPostRequest(data: String, hostname: String): Response {
        val client: OkHttpClient = OkHttpClient.Builder()
                .connectTimeout(90, TimeUnit.SECONDS)
                .readTimeout(90, TimeUnit.SECONDS)
                .build()
        val body = data.toRequestBody()
        val request: Request = Request.Builder()
                .method("POST", body)
                .url(hostname)
                .build()
        return client.newCall(request).execute()
    }


}