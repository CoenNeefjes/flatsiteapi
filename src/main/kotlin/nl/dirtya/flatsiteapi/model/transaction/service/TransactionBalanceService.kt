package nl.dirtya.flatsiteapi.model.transaction.service

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal

@Service class TransactionBalanceService
@Autowired constructor(
        private val transactionService: TransactionService,
        private val transactionEnrolledService: TransactionEnrolledService,
        private val userService: UserService
) {

    fun calculateBalanceForUsers(activeOnly: Boolean): Map<User, BigDecimal> {
        val users = if (activeOnly) userService.findByActive(true) else userService.findAll()
        return users.map { it to calculateBalanceForUser(it.id) }.toMap()
    }

    fun calculateBalanceForUser(userId: Long): BigDecimal {
        val paid = transactionService.countAmountPaid(userId)
        val spend = transactionEnrolledService.countAmountSpend(userId)
        return paid.plus(spend) // Use plus here since the spend is a negative amount
    }

    fun calculateBalanceForMe(): BigDecimal {
        return calculateBalanceForUser(userService.getCurrentUser().id)
    }

}