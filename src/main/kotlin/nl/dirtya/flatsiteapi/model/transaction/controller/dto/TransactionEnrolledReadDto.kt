package nl.dirtya.flatsiteapi.model.transaction.controller.dto

import nl.dirtya.flatsiteapi.model.transaction.api.TransactionEnrolled
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class TransactionEnrolledReadDto(enrollment: TransactionEnrolled) {
    val id = enrollment.id
    val user = SimpleUserReadDto(enrollment.user)
    val amount = enrollment.amount
    val price = enrollment.price
}