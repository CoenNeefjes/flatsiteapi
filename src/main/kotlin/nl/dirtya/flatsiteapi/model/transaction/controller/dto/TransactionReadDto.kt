package nl.dirtya.flatsiteapi.model.transaction.controller.dto

import nl.dirtya.flatsiteapi.model.transaction.api.Transaction
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class TransactionReadDto(transaction: Transaction) {
    val id = transaction.id
    val date = transaction.date
    val description = transaction.description
    val amount = transaction.price
    val owner = SimpleUserReadDto(transaction.owner)
    val enrollments = transaction.enrollments.map { TransactionEnrolledReadDto(it) }
    val cookingEventId = transaction.cookingEvent?.id
}