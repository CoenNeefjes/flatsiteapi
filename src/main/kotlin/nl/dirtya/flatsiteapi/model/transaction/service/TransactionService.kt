package nl.dirtya.flatsiteapi.model.transaction.service

import nl.dirtya.flatsiteapi.config.AppProperties
import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.InternalServerException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.transaction.api.Transaction
import nl.dirtya.flatsiteapi.model.transaction.api.TransactionRepository
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionForCookingWriteDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionWriteDto
import nl.dirtya.flatsiteapi.model.user.api.Roles
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.util.EnrolledWriteDto
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.time.LocalDate
import javax.persistence.EntityManager

@Service class TransactionService
@Autowired constructor(
        private val entityManager: EntityManager,
        private val transactionRepository: TransactionRepository,
        private val transactionEnrolledService: TransactionEnrolledService,
        private val userService: UserService,
        private val cookingEventService: CookingEventService,
        private val appProperties: AppProperties
){

    private val logger: Logger = LoggerFactory.getLogger(TransactionService::class.java)

    fun findAll(): List<Transaction> {
        return transactionRepository.findAll()
    }

    fun findAllPaged(pageable: Pageable): Page<Transaction> {
        return transactionRepository.findAll(pageable)
    }

    fun findById(id: Long): Transaction {
        return transactionRepository.findById(id).orElseThrow { NotFoundException("Transaction with id $id could not be found") }
    }

    fun create(writeDto: TransactionWriteDto): Transaction {
        writeDto.assertValid()

        // Get current user
        val owner = userService.getCurrentUser()

        // Create transaction
        val transaction = Transaction(LocalDate.now(), writeDto.description!!, writeDto.price!!, owner, null)

        // Persist transaction to the database so the foreign key exists when creating the enrollments
        entityManager.persist(transaction)

        // Create the enrollments
        val enrollments = transactionEnrolledService.createAll(writeDto.enrolled!!.map { it!! }, transaction )
        transaction.enrollments.addAll(enrollments)

        // Check the balance
        assertTransactionCorrectBalance(transaction)

        return transactionRepository.save(transaction)
    }

    fun createForCookingEvent(writeDto: TransactionForCookingWriteDto, cookingEventId: Long): Transaction {
        writeDto.assertValid()

        // Get current user
        val currentUser = userService.getCurrentUser()

        // Get the cookingEvent
        val cookingEvent = cookingEventService.findById(cookingEventId)

        // Check if we are the cook of this cookingEvent
        if (cookingEvent.cook != currentUser) {
            throw ForbiddenException("You must be cook to create a transaction for a cooking event")
        }

        // Check if there is already a transaction for this cooking event
        if (cookingEvent.transaction != null) {
            throw NotValidException("There is already a transaction for this cooking event")
        }

        // Create transaction
        val transaction = Transaction(cookingEvent.date, "Eten k-lijst", writeDto.price!!, currentUser, cookingEvent)

        // Persist transaction to the database so the foreign key exists when creating the enrollments
        entityManager.persist(transaction)

        // Create the enrollments
        val enrollmentDtoList = cookingEvent.enrollments.map { TransactionEnrolledWriteDto(it.user.id, it.amount) }
        val enrollments = transactionEnrolledService.createAll(enrollmentDtoList, transaction)
        transaction.enrollments.addAll(enrollments)

        // Check the balance
        assertTransactionCorrectBalance(transaction)

        // Put this transaction on the cookingEvent
        cookingEvent.transaction = transaction

        return transactionRepository.save(transaction)
    }

    fun updateForCookingEvent(writeDto: TransactionForCookingWriteDto, cookingEventId: Long): Transaction {
        writeDto.assertValid()

        // Get current user
        val currentUser = userService.getCurrentUser()

        // Get the cookingEvent
        val cookingEvent = cookingEventService.findById(cookingEventId)

        // Check if we are the cook of this cookingEvent
        if (cookingEvent.cook != currentUser) {
            throw ForbiddenException("You must be cook to create a transaction for a cooking event")
        }

        // Check if the transaction exists
        if (cookingEvent.transaction == null) {
            throw NotValidException("There is no transaction for this cooking event")
        }

        // Update price
        cookingEvent.transaction!!.price = writeDto.price!!

        // Update enrollments so the price gets recalculated for everyone
        return updateEnrollments(cookingEvent.transaction!!, enrollmentsToWriteDtoList(cookingEvent.transaction!!))
    }

    fun update(id: Long, writeDto: TransactionWriteDto): Transaction {
        writeDto.assertValid()

        val transaction = findById(id)

        val currentUser = userService.getCurrentUser()

        // Check if current user is the owner
        if (transaction.owner != currentUser) {
            throw ForbiddenException("You cannot update the transaction of someone else")
        }

        // Check if this is the transaction of a cooking event and the cook un-enrolls
        if (transaction.cookingEvent != null && transaction.cookingEvent.cook == currentUser && !userWillStillBeEnrolled(writeDto.enrolled!!, currentUser)) {
            throw NotValidException("If you want to un-enroll as cook you need to update the cooking event")
        }

        // Update details
        transaction.description = writeDto.description!!
        transaction.price = writeDto.price!!

        // Also update the cookingEvent if it exists
        transaction.cookingEvent?.let { cookingEventService.updateEnrollments(it, writeDto.enrolled!!) }

        return updateEnrollments(transaction, writeDto.enrolled!!)
    }

    fun updateEnrollments(transaction: Transaction, writeDtoList: List<EnrolledWriteDto?>): Transaction {
        // Make sure no enrollments are lost
        if (transaction.enrollments.size > writeDtoList.size) {
            throw NotValidException("Amount of enrollments cannot decrease in size")
        }

        // Make sure no division by 0 happens
        if (writeDtoList.sumOf { it?.amount!! } == 0) {
            throw NotValidException("It is not allowed to have 0 people enrolled on a transaction")
        }

        // Remove all old enrollments
        transaction.enrollments.clear()

        // Make sure the deletion has happened before creating new enrollments
        entityManager.flush()

        // Create new enrollments
        val enrollments = transactionEnrolledService.createAll(writeDtoList.map { it!! }, transaction)

        // Add the enrollments to the transaction
        transaction.enrollments.addAll(enrollments)

        // Make sure the transaction balance is correct
        assertTransactionCorrectBalance(transaction)

        return transactionRepository.save(transaction)
    }

    fun delete(id: Long) {
        val transaction = findById(id)

        if (transaction.owner != userService.getCurrentUser()) {
            throw ForbiddenException("You are not allowed to delete the transaction of other users")
        }

        if (transaction.cookingEvent != null) {
            cookingEventService.removeTransaction(transaction.cookingEvent)
        }

        transactionRepository.delete(transaction)
    }

    fun countAmountPaid(userId: Long): BigDecimal {
        return transactionRepository.sumPriceWhereUserIdEquals(userId) ?: BigDecimal.ZERO
    }

    fun fixExistingTransactions() {
        if (!userService.getCurrentUser().getRoles().contains(Roles.ADMIN)) {
            throw ForbiddenException("Only admins can fix existing transactions")
        }

        for (transaction in transactionRepository.findAll()) {
            val total = getRemainingBalanceOfTransaction(transaction)
            if (total.compareTo(BigDecimal.ZERO) != 0) {
                this.updateEnrollments(transaction, enrollmentsToWriteDtoList(transaction))
            }
        }
    }

    private fun assertTransactionCorrectBalance(transaction: Transaction) {
        // Only check for correct handling in safe mode
        if (appProperties.transactions.safeMode) {
            val total = getRemainingBalanceOfTransaction(transaction)
            if (total.compareTo(BigDecimal.ZERO) != 0) {
                val message = "Incorrect transaction handling detected, remaining balance is: $total!"
                logger.error(message)
                throw InternalServerException(message)
            }
            for (enrollment in transaction.enrollments) {
                if (enrollment.amount == 0 && BigDecimal.ZERO.compareTo(enrollment.price) != 0) {
                    val message = "Incorrect transaction handling detected. Zero enrolled user had to pay ${enrollment.price}!"
                    logger.error(message)
                    throw InternalServerException(message)
                }
            }
        }
    }

    private fun getRemainingBalanceOfTransaction(transaction: Transaction): BigDecimal {
        var total = transaction.price
        for (enrollment in transaction.enrollments) {
            total = total.plus(enrollment.price)
        }

        // DO use compareTo here instead of binary operator, otherwise it will not work
        return total
    }

    private fun enrollmentsToWriteDtoList(transaction: Transaction): List<EnrolledWriteDto> {
        return transaction.enrollments.map { TransactionEnrolledWriteDto(it.user.id, it.amount) }
    }

    private fun userWillStillBeEnrolled(enrollments: List<EnrolledWriteDto?>, user: User): Boolean {
        return enrollments.any { it!!.user == user.id && it.amount!! > 0 }
    }

}