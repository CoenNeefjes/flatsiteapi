package nl.dirtya.flatsiteapi.model.transaction.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.math.BigDecimal

@Repository
interface TransactionRepository: JpaRepository<Transaction, Long>, JpaSpecificationExecutor<Transaction> {

    @Query("SELECT SUM(t.price) FROM Transaction t WHERE t.owner.id = :userId")
    fun sumPriceWhereUserIdEquals(@Param("userId") userId: Long): BigDecimal?

}