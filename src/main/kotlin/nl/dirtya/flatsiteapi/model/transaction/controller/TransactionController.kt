package nl.dirtya.flatsiteapi.model.transaction.controller

import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionBalanceReadDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionForCookingWriteDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionReadDto
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionWriteDto
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionBalanceService
import nl.dirtya.flatsiteapi.model.transaction.service.TransactionService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import java.math.BigDecimal

@RestController
@RequestMapping("/transactions")
@Transactional
class TransactionController @Autowired constructor(
        private val transactionService: TransactionService,
        private val transactionBalanceService: TransactionBalanceService
){

    @GetMapping
    fun findAll(pageable: Pageable): Page<TransactionReadDto> {
        return transactionService.findAllPaged(pageable).map(::TransactionReadDto)
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id") id: Long): TransactionReadDto {
        return TransactionReadDto(transactionService.findById(id))
    }

    @GetMapping("/balance")
    fun balance(@RequestParam(name = "activeOnly", required = true) activeOnly: Boolean): List<TransactionBalanceReadDto> {
        return transactionBalanceService.calculateBalanceForUsers(activeOnly).map { TransactionBalanceReadDto(it.key, it.value) }
    }

    @GetMapping("/balance/me")
    fun myBalance(): BigDecimal {
        return transactionBalanceService.calculateBalanceForMe()
    }

    @PostMapping
    fun create(@RequestBody writeDto: TransactionWriteDto): TransactionReadDto {
        return TransactionReadDto(transactionService.create(writeDto))
    }

    @PostMapping("/cooking-event/{id}")
    fun createForCookingEvent(@PathVariable(name = "id") id: Long, @RequestBody writeDto: TransactionForCookingWriteDto): TransactionReadDto {
        return TransactionReadDto(transactionService.createForCookingEvent(writeDto, id))
    }

    @PutMapping("/cooking-event/{id}")
    fun updateForCookingEvent(@PathVariable(name = "id") id: Long, @RequestBody writeDto: TransactionForCookingWriteDto): TransactionReadDto {
        return TransactionReadDto(transactionService.updateForCookingEvent(writeDto, id))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id") id: Long, @RequestBody writeDto: TransactionWriteDto): TransactionReadDto {
        return TransactionReadDto(transactionService.update(id, writeDto))
    }

    @PostMapping("/fix-existing-transactions")
    fun fixExisting() {
        transactionService.fixExistingTransactions()
    }
}