package nl.dirtya.flatsiteapi.model.transaction.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.transaction.api.Transaction
import nl.dirtya.flatsiteapi.model.transaction.api.TransactionEnrolled
import nl.dirtya.flatsiteapi.model.transaction.api.TransactionEnrolledRepository
import nl.dirtya.flatsiteapi.model.transaction.controller.dto.TransactionEnrolledWriteDto
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import nl.dirtya.flatsiteapi.util.EnrolledWriteDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.math.BigDecimal
import java.math.RoundingMode

@Service
class TransactionEnrolledService
@Autowired constructor(
        private val transactionEnrolledRepository: TransactionEnrolledRepository,
        private val userService: UserService
){

    private val oneCent = BigDecimal.valueOf(0.01)
    private val minusOne = BigDecimal.valueOf(-1)

    fun findAll(): List<TransactionEnrolled> {
        return transactionEnrolledRepository.findAll()
    }

    fun findById(id: Long): TransactionEnrolled {
        return transactionEnrolledRepository.findById(id).orElseThrow { NotFoundException("Enrollment with id $id could not be found") }
    }

    /**
     * Creates all TransactionEnrolled entities for a given transaction and its TransactionEnrolledWriteDto's
     * Also takes into account that no money can be lost due too rounding to 2 decimal digits
     */
    fun createAll(writeDtoList: List<EnrolledWriteDto>, transaction: Transaction): List<TransactionEnrolled> {
        // Check how many people are enrolled
        val totalEnrolled = writeDtoList.sumOf { it.amount!! }

        // Calculate the price per enrollment (this must be a negative amount)
        val pricePerEnrollment = transaction.price.divide(totalEnrolled.toBigDecimal(), 2, RoundingMode.HALF_DOWN).times(minusOne)

        // Calculate the remaining value that would be caused by rounding to 2 decimal digits
        var remainingValue = transaction.price.plus(pricePerEnrollment.times(totalEnrolled.toBigDecimal()))

        // Create enrollments and sort them by user id descending
        val enrollments = writeDtoList.map { TransactionEnrolled(userService.findById(it.user!!), it.amount!!, pricePerEnrollment.times(it.amount!!.toBigDecimal()), transaction) }.sortedByDescending { it.user.id }

        for (enrollment in enrollments) {
            repeat(enrollment.amount) {
                // Check if there is still remaining value
                if (remainingValue.compareTo(BigDecimal.ZERO) != 0) {
                    // Check if we should charge extra or subtract
                    if (remainingValue > BigDecimal.ZERO) {
                        // We should charge extra, so we make the price a larger negative amount
                        enrollment.price = enrollment.price.minus(oneCent)
                        remainingValue = remainingValue.minus(oneCent)
                    } else {
                        // We should charge less, so we can add to the negative amount
                        enrollment.price = enrollment.price.plus(oneCent)
                        remainingValue = remainingValue.plus(oneCent)
                    }
                }
            }
        }

        return transactionEnrolledRepository.saveAll(enrollments)
    }

    fun countAmountSpend(userId: Long): BigDecimal {
        return transactionEnrolledRepository.sumPriceWhereUserIdEquals(userId) ?: BigDecimal.ZERO
    }

}