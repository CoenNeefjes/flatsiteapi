package nl.dirtya.flatsiteapi.model.transaction.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import java.math.BigDecimal

class TransactionBalanceReadDto(user: User, balance: BigDecimal) {
    val user = SimpleUserReadDto(user)
    val balance = balance
}