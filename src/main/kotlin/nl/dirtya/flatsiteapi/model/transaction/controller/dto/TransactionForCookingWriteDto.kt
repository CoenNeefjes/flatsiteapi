package nl.dirtya.flatsiteapi.model.transaction.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils
import java.math.BigDecimal

class TransactionForCookingWriteDto(
        val price: BigDecimal?
) {
    fun assertValid() {
        ValidationUtils.objectNotNull(price, "Price")
    }
}