package nl.dirtya.flatsiteapi.model.transaction.api

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.math.BigDecimal
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class TransactionEnrolled
constructor(
        @ManyToOne val user: User,
        var amount: Int,
        var price: BigDecimal,
        @ManyToOne val transaction: Transaction
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    override fun toString(): String {
        return "TransactionEnrolled(user=${user.id}, amount=$amount, price=$price, transaction=${transaction.id}, id=$id, uuid='$uuid')"
    }


}