package nl.dirtya.flatsiteapi.model.transaction.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.collectionNotNullOrEmptyOrContainsNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.stringNotNullEmptyMaxLength
import java.math.BigDecimal

class TransactionWriteDto(
        val description: String?,
        val price: BigDecimal?,
        val enrolled: List<TransactionEnrolledWriteDto?>?
) {

    fun assertValid() {
        stringNotNullEmptyMaxLength(description, "Description", 255)
        objectNotNull(price, "Price")
        collectionNotNullOrEmptyOrContainsNull(enrolled, "Enrolled")
        enrolled!!.forEach { it!!.assertValid()}
    }

}