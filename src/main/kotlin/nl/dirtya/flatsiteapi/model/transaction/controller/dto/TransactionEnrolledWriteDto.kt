package nl.dirtya.flatsiteapi.model.transaction.controller.dto

import nl.dirtya.flatsiteapi.util.EnrolledWriteDto

class TransactionEnrolledWriteDto(
        override val user: Long?,
        override val amount: Int?
): EnrolledWriteDto