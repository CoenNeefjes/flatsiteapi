package nl.dirtya.flatsiteapi.model.transaction.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.math.BigDecimal
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import kotlin.collections.HashSet

@Entity
@ToString
class Transaction
constructor(
        val date: LocalDate,
        var description: String,
        var price: BigDecimal,
        @ManyToOne var owner: User,
        @OneToOne(mappedBy = "transaction") val cookingEvent: CookingEvent?
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    @OneToMany(mappedBy = "transaction", cascade = [CascadeType.ALL], orphanRemoval = true)
    val enrollments: MutableSet<TransactionEnrolled> = HashSet()
}