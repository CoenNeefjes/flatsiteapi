package nl.dirtya.flatsiteapi.model.stats.api

import nl.dirtya.flatsiteapi.model.cooking.api.CookingEvent
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface DryerCountRepository: JpaRepository<CookingEvent, Long>, JpaSpecificationExecutor<CookingEvent> {

    @Query("SELECT COUNT(c.id) FROM CookingEvent c WHERE c.dryer1.id = :userId OR c.dryer2.id = :userId")
    fun getDryerCountOfUser(@Param("userId") userId: Long): Int?
}
