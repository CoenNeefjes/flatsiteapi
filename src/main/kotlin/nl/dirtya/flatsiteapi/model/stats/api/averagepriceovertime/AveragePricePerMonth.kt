package nl.dirtya.flatsiteapi.model.stats.api.averagepriceovertime

import nl.dirtya.flatsiteapi.model.user.api.User
import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "average_price_per_month")
class AveragePricePerMonth(val month: Date, val averagePrice: Double, val stdDev: Double, val count: Long, @OneToOne var user: User) {

    @Id
    var id: Long = 0;
}