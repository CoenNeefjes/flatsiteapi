package nl.dirtya.flatsiteapi.model.stats.api.dto

import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import java.time.LocalDate

class ForgottenTransactionDto(val cook: SimpleUserReadDto, val date: LocalDate) {
}