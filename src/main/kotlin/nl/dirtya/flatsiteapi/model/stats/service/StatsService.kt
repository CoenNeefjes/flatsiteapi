package nl.dirtya.flatsiteapi.model.stats.service

import nl.dirtya.flatsiteapi.model.cooking.service.CookingEnrolledService
import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.stats.api.averageprice.AveragePriceRepository
import nl.dirtya.flatsiteapi.model.stats.api.DryerCountRepository
import nl.dirtya.flatsiteapi.model.stats.api.averagepriceovertime.AveragePricePerMonthRepository
import nl.dirtya.flatsiteapi.model.stats.api.dto.AveragePricePerMonthReadDto
import nl.dirtya.flatsiteapi.model.stats.api.dto.AveragePriceReadDto
import nl.dirtya.flatsiteapi.model.stats.api.dto.DryerCountReadDto
import nl.dirtya.flatsiteapi.model.stats.api.dto.ForgottenTransactionDto
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.text.SimpleDateFormat
import java.time.LocalDate

@Service
class StatsService
@Autowired constructor(
    val userService: UserService,
    val cookingEventService: CookingEventService,
    val cookingEnrolledService: CookingEnrolledService,
    val averagePriceRepository: AveragePriceRepository,
    val averagePriceOverTimeRepository: AveragePricePerMonthRepository,
    val dryerCountRepository: DryerCountRepository
) {

    fun getAverageCookingPriceForAllUsers(): List<AveragePriceReadDto> {
        return averagePriceRepository.findAll().map {
            AveragePriceReadDto(SimpleUserReadDto(it.user), it.averagePrice.toBigDecimal(), it.stdDev.toBigDecimal(), it.count.toInt())
        }
    }

    fun getAveragePricePerMonth(): List<AveragePricePerMonthReadDto> {
        val format = SimpleDateFormat("yyyy-MM")
        return averagePriceOverTimeRepository.findAll().map {
            AveragePricePerMonthReadDto(format.format(it.month), SimpleUserReadDto(it.user), it.averagePrice.toBigDecimal(), it.stdDev.toBigDecimal(), it.count.toInt())
        }
    }

    fun getForgottenTransactions(): List<ForgottenTransactionDto> {
        val cookingEvents = cookingEventService.findForgottenTransactions(LocalDate.now().minusDays(1))

        return cookingEvents.map { e -> ForgottenTransactionDto(SimpleUserReadDto(e.cook!!), e.date) }
    }

    fun getDryerCount() : List<DryerCountReadDto> {
        return userService.findByActive(true).map {
            val dryerCount = dryerCountRepository.getDryerCountOfUser(it.id) ?: 0
            val eatAmount = cookingEnrolledService.countEnrollmentsWhereCookWasPresent(it.id)
            val fraction = if (eatAmount > 0) (dryerCount.toDouble() / eatAmount.toDouble()) else 0.0
            DryerCountReadDto(SimpleUserReadDto((it)), dryerCount, eatAmount, fraction)
        }
    }
}