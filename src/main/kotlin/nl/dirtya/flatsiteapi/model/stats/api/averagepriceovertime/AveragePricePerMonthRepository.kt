package nl.dirtya.flatsiteapi.model.stats.api.averagepriceovertime

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AveragePricePerMonthRepository : JpaRepository<AveragePricePerMonth, Long>
