package nl.dirtya.flatsiteapi.model.stats.api.dto

import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto
import java.math.BigDecimal

class AveragePricePerMonthReadDto(val month: String, val user: SimpleUserReadDto, val price: BigDecimal, val stdDev: BigDecimal, val amount: Int)