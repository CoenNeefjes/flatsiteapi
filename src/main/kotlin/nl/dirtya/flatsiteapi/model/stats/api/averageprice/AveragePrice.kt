package nl.dirtya.flatsiteapi.model.stats.api.averageprice

import nl.dirtya.flatsiteapi.model.user.api.User
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.OneToOne
import javax.persistence.Table

@Entity
@Table(name = "average_price")
class AveragePrice(val averagePrice: Double, val stdDev: Double, val count: Long, @OneToOne var user: User) {

    @Id
    var id: Long = 0;
}