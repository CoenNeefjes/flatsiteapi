package nl.dirtya.flatsiteapi.model.stats.api.averageprice

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface AveragePriceRepository : JpaRepository<AveragePrice, Long>