package nl.dirtya.flatsiteapi.model.stats.controller

import nl.dirtya.flatsiteapi.model.stats.api.dto.AveragePricePerMonthReadDto
import nl.dirtya.flatsiteapi.model.stats.api.dto.AveragePriceReadDto
import nl.dirtya.flatsiteapi.model.stats.api.dto.DryerCountReadDto
import nl.dirtya.flatsiteapi.model.stats.api.dto.ForgottenTransactionDto
import nl.dirtya.flatsiteapi.model.stats.service.StatsService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@Transactional
@RequestMapping("/stats")
class StatsController
@Autowired constructor(val statsService: StatsService) {

    @GetMapping("/average-costs")
    fun getAverageCookingPrice(): List<AveragePriceReadDto> {
        return statsService.getAverageCookingPriceForAllUsers()
    }

    @GetMapping("/average-costs-per-month")
    fun getAverageCookingPricePerMonth(): List<AveragePricePerMonthReadDto> {
        return statsService.getAveragePricePerMonth()
    }

    @GetMapping("/forgotten-transactions")
    fun getForgottenTransactions(): List<ForgottenTransactionDto> {
        return statsService.getForgottenTransactions()
    }

    @GetMapping("/dryer-count")
    fun getDryerCount(): List<DryerCountReadDto> {
        return statsService.getDryerCount()
    }
}