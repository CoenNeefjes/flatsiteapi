package nl.dirtya.flatsiteapi.model.stats.api.dto

import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class DryerCountReadDto(val user: SimpleUserReadDto, val amount: Int, val eatAmount: Int, val fraction: Double)