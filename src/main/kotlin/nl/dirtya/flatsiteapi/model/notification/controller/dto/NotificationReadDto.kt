package nl.dirtya.flatsiteapi.model.notification.controller.dto

import nl.dirtya.flatsiteapi.model.notification.api.Notification

class NotificationReadDto(notification: Notification) {
    val id = notification.id
    val type = notification.type
    val message = notification.message
}