package nl.dirtya.flatsiteapi.model.notification.api

import au.com.console.jpaspecificationdsl.equal
import nl.dirtya.flatsiteapi.model.user.api.User
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository

@Repository
interface NotificationRepository: JpaRepository<Notification, Long>, JpaSpecificationExecutor<Notification> {

    companion object {

        fun userEquals(input: User): Specification<Notification> {
            return Notification::user.equal(input)
        }

        fun typeEquals(input: NotificationType): Specification<Notification> {
            return Notification::type.equal(input)
        }

    }

}