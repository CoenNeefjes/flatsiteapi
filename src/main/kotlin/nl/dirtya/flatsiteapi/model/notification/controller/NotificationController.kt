package nl.dirtya.flatsiteapi.model.notification.controller

import nl.dirtya.flatsiteapi.model.notification.controller.dto.NotificationReadDto
import nl.dirtya.flatsiteapi.model.notification.service.NotificationService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/notifications")
@Transactional
class NotificationController
@Autowired constructor(val notificationService: NotificationService) {

    @GetMapping("/me")
    fun findAndDeleteMyNotifications(): List<NotificationReadDto> {
        return notificationService.findByMeAndDelete()
    }

}