package nl.dirtya.flatsiteapi.model.notification.api

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.*

@Entity
class Notification(
        @Enumerated(EnumType.STRING) val type: NotificationType,
        val message: String,
        @ManyToOne val user: User
) : Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()
}