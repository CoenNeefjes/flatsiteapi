package nl.dirtya.flatsiteapi.model.notification.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.notification.api.Notification
import nl.dirtya.flatsiteapi.model.notification.api.NotificationRepository
import nl.dirtya.flatsiteapi.model.notification.api.NotificationRepository.Companion.typeEquals
import nl.dirtya.flatsiteapi.model.notification.api.NotificationRepository.Companion.userEquals
import nl.dirtya.flatsiteapi.model.notification.api.NotificationType
import nl.dirtya.flatsiteapi.model.notification.controller.dto.NotificationReadDto
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class NotificationService
@Autowired constructor(val notificationRepository: NotificationRepository, val userService: UserService) {

    fun findById(id: Long): Notification {
        return notificationRepository.findById(id).orElseThrow { NotFoundException("Notification with id $id could not be found") }
    }

    fun findByUserAndType(user: User, type: NotificationType): List<Notification> {
        return notificationRepository.findAll(userEquals(user).and(typeEquals(type)))
    }

    fun findByMe(): List<Notification> {
        return notificationRepository.findAll(userEquals(userService.getCurrentUser()))
    }

    fun findByMeAndDelete(): List<NotificationReadDto> {
        val notifications = findByMe()
        val result = notifications.map(::NotificationReadDto)
        deleteAll(notifications)
        return result
    }

    fun create(user: User, type: NotificationType, message: String): Notification {
        return notificationRepository.save(Notification(type, message, user))
    }

    /**
     * Creates a notification, but only if there is not yet a notification of the given type present
     * for the specified user
     *
     * @return The created notification. Returns null if nothing was created
     */
    fun createSingleton(user: User, type: NotificationType, message: String): Notification? {
        if (findByUserAndType(user, type).isEmpty()) {
            return create(user, type, message)
        }
        return null
    }

    fun delete(notification: Notification) {
        notificationRepository.delete(notification)
    }

    fun deleteAll(notifications: List<Notification>) {
        notificationRepository.deleteAll(notifications)
    }

}