package nl.dirtya.flatsiteapi.model.notification.api

enum class NotificationType {
    BEER_FRIDGE,
    COOKING_EVENT_FORGOT_DRYERS
}