package nl.dirtya.flatsiteapi.model.printing.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository

@Repository
interface PrintingRepository : JpaRepository<PrintingJob, Long>, JpaSpecificationExecutor<PrintingJob> {
    @Query("SELECT COUNT(p.id) FROM PrintingJob p WHERE p.issuer.id = :userId")
    fun countPrintingJobsWhereOwnerIdEquals(@Param("userId") userId: Long): Int?
}