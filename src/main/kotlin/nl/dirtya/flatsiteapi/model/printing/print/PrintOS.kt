package nl.dirtya.flatsiteapi.model.printing.print

import org.springframework.web.multipart.MultipartFile

interface PrintOS {
    fun print(file: MultipartFile, amount: Int)
}