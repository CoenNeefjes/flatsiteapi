package nl.dirtya.flatsiteapi.model.printing.service

import nl.dirtya.flatsiteapi.model.printing.api.PrintingJob
import nl.dirtya.flatsiteapi.model.printing.api.PrintingRepository
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service
class PrintingOverviewService
@Autowired constructor(
    private val printingRepository: PrintingRepository,
    private val userService: UserService
) {
    fun findAll(pageable: Pageable): Page<PrintingJob> {
        return printingRepository.findAll(pageable)
    }

    fun getOverview(activeOnly: Boolean): Map<User, Int> {
        val users = if (activeOnly) userService.findByActive(true) else userService.findAll()
        return users.associateWith { getCountForUser(it.id) }
    }

    fun getCountForUser(userId: Long): Int {
        return printingRepository.countPrintingJobsWhereOwnerIdEquals(userId) ?: 0
    }
}