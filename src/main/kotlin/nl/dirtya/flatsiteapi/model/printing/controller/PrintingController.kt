package nl.dirtya.flatsiteapi.model.printing.controller

import nl.dirtya.flatsiteapi.model.printing.controller.dto.PrintingJobReadDto
import nl.dirtya.flatsiteapi.model.printing.controller.dto.PrintingStatusReadDto
import nl.dirtya.flatsiteapi.model.printing.service.PrintingOverviewService
import nl.dirtya.flatsiteapi.model.printing.service.PrintingService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.MediaType
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/printing")
@Transactional
class PrintingController
@Autowired constructor(
    private val printingOverviewService: PrintingOverviewService,
    private val printingService: PrintingService
){
    @GetMapping
    fun findAll(pageable: Pageable): Page<PrintingJobReadDto> {
        return printingOverviewService.findAll(pageable).map(::PrintingJobReadDto)
    }

    @GetMapping("/overview")
    fun overview(@RequestParam(name = "activeOnly", required = true) activeOnly: Boolean): List<PrintingStatusReadDto> {
        return printingOverviewService.getOverview(activeOnly).map { PrintingStatusReadDto(it.key, it.value) }
    }

    @PostMapping(consumes = [MediaType.APPLICATION_PDF_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE])
    fun create(@RequestParam("file") file: MultipartFile, @RequestParam("amount") amount: Int): PrintingJobReadDto {
        return PrintingJobReadDto(printingService.print(file, amount))
    }
}