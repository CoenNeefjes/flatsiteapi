package nl.dirtya.flatsiteapi.model.printing.controller.dto

import nl.dirtya.flatsiteapi.model.printing.api.PrintingJob
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class PrintingJobReadDto(printingJob: PrintingJob) {
    val id = printingJob.id
    val date = printingJob.date
    val fileName = printingJob.fileName
    val user = SimpleUserReadDto(printingJob.issuer)
}