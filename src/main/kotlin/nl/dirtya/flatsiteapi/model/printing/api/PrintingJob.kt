package nl.dirtya.flatsiteapi.model.printing.api

import lombok.ToString
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
@ToString
class PrintingJob(val date: LocalDate, val fileName: String, @ManyToOne var issuer: User) : Identifiable() {
    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()
}