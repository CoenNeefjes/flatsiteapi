package nl.dirtya.flatsiteapi.model.printing.print

import nl.dirtya.flatsiteapi.exception.InternalServerException
import nl.dirtya.flatsiteapi.util.FileValidator
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File

@Service
class PrintLP : PrintOS {
    override fun print(file: MultipartFile, amount: Int) {
        FileValidator.validateMultipartFile(file)
        val filePath = saveFileToDisk(file)
        val cmdArray = buildCmdArray(filePath, amount)

        try {
            Runtime.getRuntime().exec(cmdArray)
        } catch (e: Exception) {
            e.printStackTrace()
            throw InternalServerException(e.message ?: "Something went wrong while printing")
        }
    }

    private fun saveFileToDisk(file: MultipartFile): String {
        val tempFile = File("${System.getProperty("java.io.tmpdir")}/${file.originalFilename!!}")
        tempFile.deleteOnExit()
        file.transferTo(tempFile)

        return tempFile.absolutePath
    }

    private fun buildCmdArray(filePath: String, amount: Int): Array<String> {
        return if (amount > 1) {
            arrayOf("lp", "-m", amount.toString(), filePath)
        } else {
            arrayOf("lp", filePath)
        }
    }
}