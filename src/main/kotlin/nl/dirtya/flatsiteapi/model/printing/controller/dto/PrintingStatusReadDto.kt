package nl.dirtya.flatsiteapi.model.printing.controller.dto

import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class PrintingStatusReadDto(user: User, val balance: Int) {
    val user = SimpleUserReadDto(user)
}