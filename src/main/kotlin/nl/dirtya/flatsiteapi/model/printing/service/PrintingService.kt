package nl.dirtya.flatsiteapi.model.printing.service

import nl.dirtya.flatsiteapi.model.printing.api.PrintingJob
import nl.dirtya.flatsiteapi.model.printing.api.PrintingRepository
import nl.dirtya.flatsiteapi.model.printing.print.PrintOS
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.time.LocalDate

@Service
class PrintingService
@Autowired constructor(
    private val printingRepository: PrintingRepository,
    private val printOS: PrintOS,
    private val userService: UserService
){
    fun print(file: MultipartFile, amount: Int = 1): PrintingJob {
        printOS.print(file, amount)

        val currentUser = userService.getCurrentUser()
        val filename = file.originalFilename!!
        return printingRepository.save(PrintingJob(LocalDate.now(), filename, currentUser))
    }
}