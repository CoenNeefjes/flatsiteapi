package nl.dirtya.flatsiteapi.model.roomsoliciting.day.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDay
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDayRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.time.LocalDate
import java.time.temporal.ChronoUnit
import java.util.stream.Collectors
import java.util.stream.IntStream

@Service class RoomSolicitingDayService
@Autowired constructor(
        private val repository: RoomSolicitingDayRepository
){

    fun findById(id: Long): RoomSolicitingDay {
        return repository.findById(id).orElseThrow { NotFoundException("RoomSolicitingDay with id $id could not be found") }
    }

    fun findByIdOptional(id: Long): RoomSolicitingDay? {
        return repository.findById(id).orElse(null)
    }

    fun findByDate(date: LocalDate): RoomSolicitingDay? {
        return repository.findOne(RoomSolicitingDayRepository.dateEquals(date)).orElse(null)
    }

    fun findByDateOrCreate(date: LocalDate): RoomSolicitingDay {
        return findByDate(date) ?: create(date)
    }

    /**
     * Finds all cookingEvent within the specified date range (exclusive of end date)
     */
    fun findAllInRange(start: LocalDate, end: LocalDate): Map<LocalDate, RoomSolicitingDay?> {
        val numOfDaysBetween: Long = ChronoUnit.DAYS.between(start, end)

        if (numOfDaysBetween < 0) {
            throw NotValidException("End date cannot be before start date")
        }

        val dates: Set<LocalDate> = IntStream.iterate(0) { i: Int -> i + 1 }
                .limit(numOfDaysBetween)
                .mapToObj { i: Int -> start.plusDays(i.toLong()) }
                .collect(Collectors.toSet())

        val days = repository.findAll(RoomSolicitingDayRepository.dateIn(dates))

        return dates.associateWith { days.find { x -> x.date == it } }
    }

    fun findAllInRangeOrCreate(start: LocalDate, end: LocalDate): List<RoomSolicitingDay> {
        val optionalDays: Map<LocalDate, RoomSolicitingDay?> = findAllInRange(start, end)
        return optionalDays.map { it.value ?: create(it.key) }
    }

    fun create(date: LocalDate): RoomSolicitingDay {
        return repository.save(RoomSolicitingDay(date))
    }

}