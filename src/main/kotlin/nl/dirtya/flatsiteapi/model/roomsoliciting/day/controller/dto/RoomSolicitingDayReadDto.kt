package nl.dirtya.flatsiteapi.model.roomsoliciting.day.controller.dto

import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller.dto.RoomSolicitingDayAvailabilityReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDay

class RoomSolicitingDayReadDto(day: RoomSolicitingDay) {

    val id = day.id
    val date = day.date
    val appointments = day.appointments.map { RoomSolicitingAppointmentReadDto(it) }
    val availabilities = day.availabilities.map { RoomSolicitingDayAvailabilityReadDto(it) }

}
