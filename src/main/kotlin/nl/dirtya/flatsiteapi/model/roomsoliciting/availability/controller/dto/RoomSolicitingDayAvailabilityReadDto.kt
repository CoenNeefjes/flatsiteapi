package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller.dto

import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api.RoomSolicitingDayAvailability
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class RoomSolicitingDayAvailabilityReadDto(availability: RoomSolicitingDayAvailability) {

    val id = availability.id
    val user = SimpleUserReadDto(availability.user)
    val availabilityType = availability.availabilityType
    val remarks = availability.remarks

}