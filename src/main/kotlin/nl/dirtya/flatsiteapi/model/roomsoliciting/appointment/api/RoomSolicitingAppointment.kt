package nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.api

import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDay
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api.RoomSolicitor
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class RoomSolicitingAppointment(
        @ManyToOne(optional = false) var day: RoomSolicitingDay,
        @ManyToOne(optional = false) val solicitor: RoomSolicitor,
        var remarks: String
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

}