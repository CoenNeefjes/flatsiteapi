package nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller

import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentCreateWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentUpdateWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.service.RoomSolicitingAppointmentService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/room-soliciting-appointments")
@Transactional
class RoomSolicitingAppointmentController
@Autowired constructor(
        private val service: RoomSolicitingAppointmentService
){

    @PostMapping
    fun create(@RequestBody writeDto: RoomSolicitingAppointmentCreateWriteDto): RoomSolicitingAppointmentReadDto {
        return RoomSolicitingAppointmentReadDto(service.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id", required = true) id: Long, writeDto: RoomSolicitingAppointmentUpdateWriteDto): RoomSolicitingAppointmentReadDto {
        return RoomSolicitingAppointmentReadDto(service.update(id, writeDto))
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable(name = "id", required = true) id: Long) {
        service.delete(id)
    }

}