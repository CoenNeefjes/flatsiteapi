package nl.dirtya.flatsiteapi.model.roomsoliciting.day.api

import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.api.RoomSolicitingAppointment
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api.RoomSolicitingDayAvailability
import nl.dirtya.flatsiteapi.util.Identifiable
import java.time.LocalDate
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList

@Entity
class RoomSolicitingDay(val date: LocalDate): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    @OneToMany(mappedBy = "day", cascade = [CascadeType.ALL], orphanRemoval = true)
    val appointments: MutableList<RoomSolicitingAppointment> = ArrayList()

    @OneToMany(mappedBy = "day", cascade = [CascadeType.ALL], orphanRemoval = true)
    val availabilities: MutableList<RoomSolicitingDayAvailability> = ArrayList()

}