package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api

import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDay
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.*

@Entity
class RoomSolicitingDayAvailability(
        @ManyToOne(optional = false) val day: RoomSolicitingDay,
        @ManyToOne(optional = false) val user: User,
        @Enumerated(EnumType.STRING) var availabilityType: RoomSolicitingDayAvailabilityType,
        var remarks: String
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

}