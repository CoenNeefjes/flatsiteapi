package nl.dirtya.flatsiteapi.model.roomsoliciting.review.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.roomsoliciting.review.api.RoomSolicitorReview
import nl.dirtya.flatsiteapi.model.roomsoliciting.review.api.RoomSolicitorReviewRepository
import nl.dirtya.flatsiteapi.model.roomsoliciting.review.controller.dto.RoomSolicitorReviewWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.service.RoomSolicitorService
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class RoomSolicitorReviewService
@Autowired constructor(
        private val repository: RoomSolicitorReviewRepository,
        private val roomSolicitorService: RoomSolicitorService,
        private val userService: UserService
){

    fun findById(id: Long): RoomSolicitorReview {
        return repository.findById(id).orElseThrow { NotFoundException("RoomSolicitorReview with id $id could not be found") }
    }

    fun findBySolicitorId(id: Long): List<RoomSolicitorReview> {
        return roomSolicitorService.findById(id).reviews
    }

    fun create(writeDto: RoomSolicitorReviewWriteDto): RoomSolicitorReview {
        writeDto.assertValid()

        val solicitor = roomSolicitorService.findById(writeDto.solicitor!!)

        val currentUser = userService.getCurrentUser()

        return repository.save(RoomSolicitorReview(solicitor, currentUser, writeDto.review!!, writeDto.score!!))
    }

    fun update(id: Long, writeDto: RoomSolicitorReviewWriteDto): RoomSolicitorReview {
        writeDto.assertValid()

        val review = findById(id)

        val currentUser = userService.getCurrentUser()

        if (review.user != currentUser) {
            throw ForbiddenException("You are not allowed to edit this review")
        }

        review.review = writeDto.review!!
        review.score = writeDto.score!!

        return repository.save(review)
    }

}