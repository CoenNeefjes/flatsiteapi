package nl.dirtya.flatsiteapi.model.roomsoliciting.review.controller.dto

import nl.dirtya.flatsiteapi.model.roomsoliciting.review.api.RoomSolicitorReview
import nl.dirtya.flatsiteapi.model.user.controller.dto.SimpleUserReadDto

class RoomSolicitorReviewReadDto(review: RoomSolicitorReview) {

    val id = review.id
    val user = SimpleUserReadDto(review.user)
    val review = review.review
    val score = review.score

}