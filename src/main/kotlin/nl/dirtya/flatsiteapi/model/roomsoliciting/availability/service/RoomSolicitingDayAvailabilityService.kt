package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.service

import nl.dirtya.flatsiteapi.exception.ForbiddenException
import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api.RoomSolicitingDayAvailability
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api.RoomSolicitingDayAvailabilityRepository
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller.dto.RoomSolicitingDayAvailabilityWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.service.RoomSolicitingDayService
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class RoomSolicitingDayAvailabilityService
@Autowired constructor(
        private val repository: RoomSolicitingDayAvailabilityRepository,
        private val roomSolicitingDayService: RoomSolicitingDayService,
        private val userService: UserService
){

    fun findById(id: Long): RoomSolicitingDayAvailability {
        return repository.findById(id).orElseThrow { NotFoundException("RoomSolicitingDayAvailability with id $id could not be found") }
    }

    fun findByIdOptional(id: Long): RoomSolicitingDayAvailability? {
        return repository.findById(id).orElse(null)
    }

    fun create(writeDto: RoomSolicitingDayAvailabilityWriteDto): RoomSolicitingDayAvailability {
        writeDto.assertValid()

        val day = roomSolicitingDayService.findById(writeDto.day!!)

        val currentUser = userService.getCurrentUser()

        if (day.availabilities.find { it.user.id == currentUser.id } != null) {
            throw NotValidException("Availability for this user is already created")
        }

        return repository.save(RoomSolicitingDayAvailability(day, currentUser, writeDto.availabilityType!!, writeDto.remarks!!))
    }

    fun update(id: Long, writeDto: RoomSolicitingDayAvailabilityWriteDto): RoomSolicitingDayAvailability {
        writeDto.assertValid()

        val availability = findById(id)

        val currentUser = userService.getCurrentUser()

        if (availability.user.id != currentUser.id) {
            throw ForbiddenException("You are not allowed to update the availability of another user")
        }

        availability.availabilityType = writeDto.availabilityType!!
        availability.remarks = writeDto.remarks!!

        return repository.save(availability)
    }

}