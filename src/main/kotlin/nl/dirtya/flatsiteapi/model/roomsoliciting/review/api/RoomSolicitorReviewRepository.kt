package nl.dirtya.flatsiteapi.model.roomsoliciting.review.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoomSolicitorReviewRepository: JpaRepository<RoomSolicitorReview, Long> {

}