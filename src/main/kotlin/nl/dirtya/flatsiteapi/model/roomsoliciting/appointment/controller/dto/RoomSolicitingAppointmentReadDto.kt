package nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto

import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.api.RoomSolicitingAppointment
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.controller.dto.SimpleRoomSolicitingDayReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto.RoomSolicitorReadDto

class RoomSolicitingAppointmentReadDto(appointment: RoomSolicitingAppointment) {

    val id = appointment.id
    val day = SimpleRoomSolicitingDayReadDto(appointment.day)
    val solicitor = RoomSolicitorReadDto(appointment.solicitor)
    val remarks = appointment.remarks

}
