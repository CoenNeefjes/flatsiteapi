package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api

enum class RoomSolicitingDayAvailabilityType {

    FULLY_AVAILABLE,
    NOT_AVAILABLE,
    OTHER

}