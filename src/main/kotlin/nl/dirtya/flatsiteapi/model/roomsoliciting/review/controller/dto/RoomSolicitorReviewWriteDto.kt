package nl.dirtya.flatsiteapi.model.roomsoliciting.review.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils

class RoomSolicitorReviewWriteDto(
        val solicitor: Long?,
        val review: String?,
        val score: Int?
) {

    fun assertValid() {
        ValidationUtils.objectNotNull(solicitor, "Solicitor")
        ValidationUtils.objectNotNull(review, "Review")
        ValidationUtils.objectNotNull(score, "Score")
    }

}