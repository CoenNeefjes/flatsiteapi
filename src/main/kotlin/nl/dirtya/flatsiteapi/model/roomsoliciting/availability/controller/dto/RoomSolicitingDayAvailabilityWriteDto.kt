package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller.dto

import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api.RoomSolicitingDayAvailabilityType
import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull

class RoomSolicitingDayAvailabilityWriteDto(
        val day: Long?,
        val availabilityType: RoomSolicitingDayAvailabilityType?,
        val remarks: String?
) {

    fun assertValid() {
        objectNotNull(day, "Day")
        objectNotNull(availabilityType, "AvailabilityType")
        objectNotNull(remarks, "Remarks")
        if (availabilityType == RoomSolicitingDayAvailabilityType.OTHER && remarks!!.isBlank()) {
            throw NotValidException("Remark is required when availability type is 'other'")
        }
    }

}