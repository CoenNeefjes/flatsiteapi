package nl.dirtya.flatsiteapi.model.roomsoliciting.day.controller.dto

import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDay

class SimpleRoomSolicitingDayReadDto(day: RoomSolicitingDay) {

    val id = day.id
    val date = day.date

}