package nl.dirtya.flatsiteapi.model.roomsoliciting.review.api

import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api.RoomSolicitor
import nl.dirtya.flatsiteapi.model.user.api.User
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.ManyToOne

@Entity
class RoomSolicitorReview(
        @ManyToOne(optional = false) val solicitor: RoomSolicitor,
        @ManyToOne(optional = false) val user: User,
        var review: String,
        var score: Int
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

}