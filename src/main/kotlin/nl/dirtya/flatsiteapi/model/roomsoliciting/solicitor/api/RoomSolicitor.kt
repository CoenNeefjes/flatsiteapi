package nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api

import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.api.RoomSolicitingAppointment
import nl.dirtya.flatsiteapi.model.roomsoliciting.review.api.RoomSolicitorReview
import nl.dirtya.flatsiteapi.util.Identifiable
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList

@Entity
class RoomSolicitor(
        var name: String,
        var email: String,
        var studentYear: Int,
        var age: Int,
        var study: String,
        var remarks: String
): Identifiable() {

    @Id
    @GeneratedValue
    override var id: Long = 0

    override val uuid: String = UUID.randomUUID().toString()

    @OneToMany(mappedBy = "solicitor", cascade = [CascadeType.ALL], orphanRemoval = true)
    val appointments: MutableList<RoomSolicitingAppointment> = ArrayList()

    @OneToMany(mappedBy = "solicitor", cascade = [CascadeType.ALL], orphanRemoval = true)
    val reviews: MutableList<RoomSolicitorReview> = ArrayList()

}