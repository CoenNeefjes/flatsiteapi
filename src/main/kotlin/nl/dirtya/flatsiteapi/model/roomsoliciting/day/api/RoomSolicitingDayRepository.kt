package nl.dirtya.flatsiteapi.model.roomsoliciting.day.api

import au.com.console.jpaspecificationdsl.`in`
import au.com.console.jpaspecificationdsl.equal
import org.springframework.data.jpa.domain.Specification
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Repository
interface RoomSolicitingDayRepository: JpaRepository<RoomSolicitingDay, Long>, JpaSpecificationExecutor<RoomSolicitingDay> {

    companion object {

        fun dateEquals(input: LocalDate?): Specification<RoomSolicitingDay>? = input?.let {
            return RoomSolicitingDay::date.equal(input)
        }

        fun dateIn(input: Set<LocalDate>?): Specification<RoomSolicitingDay>? = input?.let {
            return RoomSolicitingDay::date.`in`(input)
        }

    }

}