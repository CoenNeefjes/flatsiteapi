package nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api.RoomSolicitor
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api.RoomSolicitorRepository
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto.RoomSolicitorWriteDto
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.stereotype.Service

@Service class RoomSolicitorService
@Autowired constructor(
        private val repository: RoomSolicitorRepository
){

    fun findAll(pageable: Pageable): Page<RoomSolicitor> {
        return repository.findAll(pageable)
    }

    fun findById(id: Long): RoomSolicitor {
        return repository.findById(id).orElseThrow { NotFoundException("RoomSolicitor with id $id could not be found") }
    }

    fun findByIdOptional(id: Long): RoomSolicitor? {
        return repository.findById(id).orElse(null)
    }

    fun create(writeDto: RoomSolicitorWriteDto): RoomSolicitor {
        writeDto.assertValid()

        return repository.save(RoomSolicitor(
                writeDto.name!!,
                writeDto.email!!,
                writeDto.studentYear!!,
                writeDto.age!!,
                writeDto.study!!,
                writeDto.remarks!!
        ))
    }

    fun update(id: Long, writeDto: RoomSolicitorWriteDto): RoomSolicitor {
        writeDto.assertValid()

        val solicitor = findById(id)

        solicitor.name = writeDto.name!!
        solicitor.email = writeDto.email!!
        solicitor.studentYear = writeDto.studentYear!!
        solicitor.age = writeDto.age!!
        solicitor.study = writeDto.study!!
        solicitor.remarks = writeDto.remarks!!

        return repository.save(solicitor)
    }

    fun delete(id: Long) {
        repository.delete(findById(id))
    }

}