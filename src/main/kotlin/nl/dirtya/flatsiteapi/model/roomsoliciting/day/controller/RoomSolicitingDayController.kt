package nl.dirtya.flatsiteapi.model.roomsoliciting.day.controller

import nl.dirtya.flatsiteapi.model.roomsoliciting.day.controller.dto.RoomSolicitingDayReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.service.RoomSolicitingDayService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
@RequestMapping("/room-soliciting-days")
@Transactional
class RoomSolicitingDayController
@Autowired constructor(
        private val roomSolicitingDayService: RoomSolicitingDayService
){

    @GetMapping("get-or-create")
    fun getOrCreate(@RequestParam(name = "date", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) date: LocalDate): RoomSolicitingDayReadDto {
        return RoomSolicitingDayReadDto(roomSolicitingDayService.findByDateOrCreate(date))
    }

    @GetMapping("get-or-create/range")
    fun getOrCreateInRange(
            @RequestParam(name = "from", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) from: LocalDate,
            @RequestParam(name = "to", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) to: LocalDate
    ): List<RoomSolicitingDayReadDto> {
        return roomSolicitingDayService.findAllInRangeOrCreate(from, to).map { RoomSolicitingDayReadDto(it) }
    }

}