package nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull

class RoomSolicitorWriteDto(
        val name: String?,
        val email: String?,
        val studentYear: Int?,
        val age: Int?,
        val study: String?,
        val remarks: String?
) {

    fun assertValid() {
        objectNotNull(name, "Name")
        objectNotNull(email, "Email")
        objectNotNull(studentYear, "StudentYear")
        objectNotNull(age, "Age")
        objectNotNull(study, "Study")
        objectNotNull(remarks, "Remarks")
    }

}
