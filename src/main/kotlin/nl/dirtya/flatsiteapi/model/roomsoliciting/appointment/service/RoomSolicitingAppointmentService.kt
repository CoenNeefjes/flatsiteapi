package nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.service

import nl.dirtya.flatsiteapi.exception.NotFoundException
import nl.dirtya.flatsiteapi.exception.NotValidException
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.api.RoomSolicitingAppointment
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.api.RoomSolicitingAppointmentRepository
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentCreateWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto.RoomSolicitingAppointmentUpdateWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.api.RoomSolicitingDay
import nl.dirtya.flatsiteapi.model.roomsoliciting.day.service.RoomSolicitingDayService
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api.RoomSolicitor
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.service.RoomSolicitorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service class RoomSolicitingAppointmentService
@Autowired constructor(
        private val repository: RoomSolicitingAppointmentRepository,
        private val roomSolicitingDayService: RoomSolicitingDayService,
        private val roomSolicitorService: RoomSolicitorService
){

    fun findById(id: Long): RoomSolicitingAppointment {
        return repository.findById(id).orElseThrow { NotFoundException("RoomSolicitingAppointment with id $id could not be found") }
    }

    fun findByIdOptional(id: Long): RoomSolicitingAppointment? {
        return repository.findById(id).orElse(null)
    }

    fun create(writeDto: RoomSolicitingAppointmentCreateWriteDto): RoomSolicitingAppointment {
        writeDto.assertValid()

        val day = roomSolicitingDayService.findById(writeDto.day!!)
        val solicitor = roomSolicitorService.findById(writeDto.solicitor!!)

        return create(day, solicitor, writeDto.remarks!!)
    }

    fun create(day: RoomSolicitingDay, solicitor: RoomSolicitor, remarks: String): RoomSolicitingAppointment {
        if (day.appointments.any { it.solicitor.id == solicitor.id }) {
            throw NotValidException("Solicitor already has an appointment on ${day.date}")
        }

        return repository.save(RoomSolicitingAppointment(day, solicitor, remarks))
    }

    fun update(id: Long, writeDto: RoomSolicitingAppointmentUpdateWriteDto): RoomSolicitingAppointment {
        writeDto.assertValid()

        val appointment = findById(id)

        appointment.remarks = writeDto.remarks!!

        return repository.save(appointment)
    }

    fun delete(id: Long) {
        repository.delete(findById(id))
    }

}