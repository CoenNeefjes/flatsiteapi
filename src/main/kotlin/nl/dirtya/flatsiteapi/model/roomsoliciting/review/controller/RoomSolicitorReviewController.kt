package nl.dirtya.flatsiteapi.model.roomsoliciting.review.controller

import nl.dirtya.flatsiteapi.model.roomsoliciting.review.controller.dto.RoomSolicitorReviewReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.review.controller.dto.RoomSolicitorReviewWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.review.service.RoomSolicitorReviewService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/room-soliciting-reviews")
@Transactional
class RoomSolicitorReviewController
@Autowired constructor(
        private val roomSolicitorReviewService: RoomSolicitorReviewService
){

    @GetMapping("/solicitor/{id}")
    fun getBySolicitor(@PathVariable(name = "id", required = true) id: Long): List<RoomSolicitorReviewReadDto> {
        return roomSolicitorReviewService.findBySolicitorId(id).map { RoomSolicitorReviewReadDto(it) }
    }

    @PostMapping
    fun create(@RequestBody writeDto: RoomSolicitorReviewWriteDto): RoomSolicitorReviewReadDto {
        return RoomSolicitorReviewReadDto(roomSolicitorReviewService.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id", required = true) id: Long, @RequestBody writeDto: RoomSolicitorReviewWriteDto): RoomSolicitorReviewReadDto {
        return RoomSolicitorReviewReadDto(roomSolicitorReviewService.update(id, writeDto))
    }

}