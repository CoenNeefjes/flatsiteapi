package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoomSolicitingDayAvailabilityRepository: JpaRepository<RoomSolicitingDayAvailability, Long> {
}