package nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils

class RoomSolicitingAppointmentUpdateWriteDto(
        val remarks: String?
) {

    fun assertValid() {
        ValidationUtils.objectNotNull(remarks, "Remarks")
    }

}