package nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller

import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto.RoomSolicitorReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto.RoomSolicitorWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.service.RoomSolicitorService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/room-solicitors")
@Transactional
class RoomSolicitorController
@Autowired constructor(
        private val service: RoomSolicitorService
){

    @GetMapping
    fun findAll(pageable: Pageable): Page<RoomSolicitorReadDto> {
        return service.findAll(pageable).map { RoomSolicitorReadDto(it) }
    }

    @GetMapping("/{id}")
    fun findById(@PathVariable(name = "id", required = true) id: Long): RoomSolicitorReadDto {
        return RoomSolicitorReadDto(service.findById(id))
    }

    @PostMapping
    fun create(@RequestBody writeDto: RoomSolicitorWriteDto): RoomSolicitorReadDto {
        return RoomSolicitorReadDto(service.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id", required = true) id: Long, @RequestBody writeDto: RoomSolicitorWriteDto): RoomSolicitorReadDto {
        return RoomSolicitorReadDto(service.update(id, writeDto))
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    fun delete(@PathVariable(name = "id", required = true) id: Long) {
        service.delete(id)
    }

}