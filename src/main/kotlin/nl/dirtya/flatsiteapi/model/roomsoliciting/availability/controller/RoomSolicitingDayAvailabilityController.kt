package nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller

import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller.dto.RoomSolicitingDayAvailabilityReadDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.controller.dto.RoomSolicitingDayAvailabilityWriteDto
import nl.dirtya.flatsiteapi.model.roomsoliciting.availability.service.RoomSolicitingDayAvailabilityService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/room-soliciting-days/availability")
@Transactional
class RoomSolicitingDayAvailabilityController
@Autowired constructor(
        private val service: RoomSolicitingDayAvailabilityService
){

    @PostMapping
    fun create(@RequestBody writeDto: RoomSolicitingDayAvailabilityWriteDto): RoomSolicitingDayAvailabilityReadDto {
        return RoomSolicitingDayAvailabilityReadDto(service.create(writeDto))
    }

    @PutMapping("/{id}")
    fun update(@PathVariable(name = "id") id: Long, @RequestBody writeDto: RoomSolicitingDayAvailabilityWriteDto): RoomSolicitingDayAvailabilityReadDto {
        return RoomSolicitingDayAvailabilityReadDto(service.update(id, writeDto))
    }

}