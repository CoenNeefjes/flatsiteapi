package nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.controller.dto

import nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api.RoomSolicitor

class RoomSolicitorReadDto(solicitor: RoomSolicitor) {

    val id = solicitor.id
    val name = solicitor.name
    val email = solicitor.email
    val studentYear = solicitor.studentYear
    val age = solicitor.age
    val study = solicitor.study
    val remarks = solicitor.remarks
    val averageScore = solicitor.reviews.map { it.score }.average()

}