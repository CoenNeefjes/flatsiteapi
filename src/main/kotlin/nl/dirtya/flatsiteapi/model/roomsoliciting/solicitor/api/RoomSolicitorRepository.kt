package nl.dirtya.flatsiteapi.model.roomsoliciting.solicitor.api

import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface RoomSolicitorRepository: JpaRepository<RoomSolicitor, Long> {
}