package nl.dirtya.flatsiteapi.model.roomsoliciting.appointment.controller.dto

import nl.dirtya.flatsiteapi.util.ValidationUtils.Companion.objectNotNull

class RoomSolicitingAppointmentCreateWriteDto(
        val day: Long?,
        val solicitor: Long?,
        val remarks: String?
) {

    fun assertValid() {
        objectNotNull(day, "Day")
        objectNotNull(solicitor, "Solicitor")
        objectNotNull(remarks, "Remarks")
    }

}