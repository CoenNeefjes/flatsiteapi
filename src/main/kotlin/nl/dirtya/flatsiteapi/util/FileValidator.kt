package nl.dirtya.flatsiteapi.util

import nl.dirtya.flatsiteapi.exception.NotValidException
import org.springframework.web.multipart.MultipartFile

class FileValidator {
    companion object {

        fun validateMultipartFile(file: MultipartFile) {
            if (file.isEmpty) {
                throw NotValidException("File cannot be empty")
            }
            if (file.originalFilename == null) {
                throw NotValidException("File name cannot be null")
            }
            if (file.originalFilename!!.isEmpty()) {
                throw NotValidException("File name cannot be empty")
            }
            if (file.originalFilename!!.split("\\.".toRegex()).size != 2) {
                throw NotValidException("File name can only contain one dot")
            }
        }

    }
}