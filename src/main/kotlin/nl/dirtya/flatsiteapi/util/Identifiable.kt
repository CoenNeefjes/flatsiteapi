package nl.dirtya.flatsiteapi.util

/**
 * Overreaching class that should be extended by all @Entity annotated classes
 *
 * Provides a correct hashCode and Equals implementation
 *
 */

abstract class Identifiable {

    abstract var id: Long
        protected set

    abstract val uuid: String

    override fun hashCode(): Int {
        return this.uuid.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) {
            return true
        }

        if (!this.javaClass.isInstance(other)) {
            return false
        }

        return uuid == (other as Identifiable).uuid
    }
}