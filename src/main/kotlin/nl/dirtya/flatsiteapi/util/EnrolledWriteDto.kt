package nl.dirtya.flatsiteapi.util

interface EnrolledWriteDto {
    val user: Long?
    val amount: Int?

    fun assertValid() {
        ValidationUtils.objectNotNull(user, "User")
        ValidationUtils.integerMinimumNotNull(amount, "Amount", 0)
    }
}