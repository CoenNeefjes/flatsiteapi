package nl.dirtya.flatsiteapi.util

import nl.dirtya.flatsiteapi.exception.NotValidException
import java.math.BigDecimal

class ValidationUtils {

    companion object {

        val guidRegex: Regex = "\"^[0-9a-fA-F]{8}\\\\-[0-9a-fA-F]{4}\\\\-[0-9a-fA-F]{4}\\\\-[0-9a-fA-F]{4}\\\\-[0-9a-fA-F]{12}\$\"".toRegex()
        val emailRegex: Regex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])".toRegex()

        fun stringNotNullEmptyMaxLength(value: String?, name: String, maxLength: Int) {
            stringNotNullOrEmpty(value, name)
            stringLengthMax(value!!, name, maxLength)
        }

        fun stringNotNullEmptyAndMatches(value: String?, name: String, regex: Regex) {
            stringNotNullOrEmpty(value, name)
            stringMatches(value!!, name, regex)
        }

        fun stringNotNullOrEmpty(value: String?, name: String) {
            stringNotNull(value, name)
            stringNotEmpty(value!!, name)
        }

        fun stringNotNull(value: String?, name: String) {
            if (value == null) {
                throw NotValidException("$name cannot be null")
            }
        }

        fun stringNotEmpty(value: String, name: String) {
            if (value.isEmpty()) {
                throw NotValidException("$name cannot be empty")
            }
        }

        fun stringLengthMax(value: String, name: String, maxLength: Int) {
            if (value.length > maxLength) {
                throw NotValidException("$name cannot exceed length of $maxLength")
            }
        }

        fun stringMatches(value: String, name: String, regex: Regex) {
            if (!value.matches(regex)) {
                throw NotValidException("Invalid " + name.lowercase())
            }
        }

        fun doubleNotNullNegativeOrMaximum(value: Double, name: String, maximum: Double) {
            doubleNotNullOrNegative(value, name)
            doubleMaximum(value, name, maximum)
        }

        fun doubleNotNullOrNegative(value: Double?, name: String) {
            objectNotNull(value, name)
            doubleNotNegative(value!!, name)
        }

        fun doubleMaximum(value: Double, name: String, maximum: Double) {
            if (value > maximum) {
                throw NotValidException("$name cannot be greater than $maximum")
            }
        }

        fun doubleNotNegative(value: Double, name: String) {
            if (value < 0) {
                throw NotValidException("$name cannot be negative")
            }
        }

        fun integerMinimumNotNull(value: Int?, name: String, minimum: Int) {
            objectNotNull(value, name)
            integerMinimum(value!!, name, minimum)
        }

        fun integerMinimum(value: Int, name: String, minimum: Int) {
            if (value < minimum) {
                throw NotValidException("$name must be at least $minimum")
            }
        }

        fun integerNotNullOrNegative(value: Int?, name: String) {
            objectNotNull(value, name)
            integerNotNegative(value!!, name)
        }

        fun integerNotNegative(value: Int, name: String) {
            if (value < 0) {
                throw NotValidException("$name cannot be negative")
            }
        }

        fun bigDecimalNotNullOrNegative(value: BigDecimal?, name: String) {
            objectNotNull(value, name)
            bigDecimalNotNegative(value!!, name)
        }

        fun bigDecimalNotNegative(value: BigDecimal, name: String) {
            if (value < BigDecimal.ZERO) {
                throw NotValidException("$name cannot be negative")
            }
        }

        fun collectionNotNullOrEmptyOrContainsNull(value: Collection<Any?>?, name: String) {
            objectNotNull(value, name)
            collectionNotEmpty(value!!, name)
            collectionNotContainsNull(value, name)
        }

        fun collectionNotNullOrContainsNull(value: Collection<Any?>?, name: String) {
            objectNotNull(value, name)
            collectionNotContainsNull(value!!, name)
        }

        fun collectionNotNullOrEmpty(value: Collection<Any?>?, name: String) {
            objectNotNull(value, name)
            collectionNotEmpty(value!!, name)
        }

        fun collectionNotEmpty(value: Collection<Any?>, name: String) {
            if (value.isEmpty()) {
                throw NotValidException("$name cannot be empty")
            }
        }

        fun collectionNotContainsNull(value: Collection<Any?>, name: String) {
            if (value.contains(null)) {
                throw NotValidException("$name cannot contain null")
            }
        }

        fun objectNotNull(value: Any?, name: String) {
            if (value == null) {
                throw NotValidException("$name cannot be null")
            }
        }

        fun floatMinimumNotNull(value: Float?, name: String, minimum: Float) {
            if (value == null) {
                throw NotValidException("$name cannot be null")
            }
            if (value < minimum) {
                throw NotValidException("$name must be at least $minimum")
            }
        }
    }
}