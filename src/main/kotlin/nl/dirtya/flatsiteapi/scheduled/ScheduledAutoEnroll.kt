package nl.dirtya.flatsiteapi.scheduled

import nl.dirtya.flatsiteapi.model.cooking.service.CookingEventService
import nl.dirtya.flatsiteapi.model.notification.api.NotificationType
import nl.dirtya.flatsiteapi.model.notification.service.NotificationService
import nl.dirtya.flatsiteapi.model.user.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Transactional
@Component class ScheduledAutoEnroll
@Autowired constructor(
    private val userService: UserService,
    private val cookingEventService: CookingEventService,
    private val notificationService: NotificationService,
){

    /**
     * Scheduled task that runs every day at 12:00 and enrolls every user with the auto-enroll feature
     * enabled for the cooking event of the day.
     */
    @Scheduled(cron = "0 0 12 * * *")
    fun autoEnroll() {
        val currentDate = LocalDate.now()
        val activeUsers = userService.findByActive(true)
        val cookingEvent = cookingEventService.findByDateOrCreate(currentDate)

        // Get all active users that have autoEnrolled enabled for the current date
        activeUsers.filter { it.autoEnroll.hasValueForDay(currentDate.dayOfWeek) }.forEach {
            // Check if not already enrolled
            if (!cookingEvent.hasEnrollmentForUser(it.id)) {
                // Check if we need to enroll 1 or 0 times
                val amountToEnroll = if (it.autoEnroll.getValueForDay(currentDate.dayOfWeek) == true) 1 else 0
                // Create enrollment
                cookingEventService.enroll(cookingEvent, amountToEnroll, it)
            }
        }
    }

    /**
     * Scheduled task that runs every day at 22:00 and creates a notification for the cook if
     * he forgot to assign dryers for the cooking event
     */
    @Scheduled(cron = "0 0 22 * * *")
    fun dryerNotification() {
        val currentDate = LocalDate.now()
        val cookingEvent = cookingEventService.findByDateOrCreate(currentDate)

        // Check if there was a cook
        if (cookingEvent.cook != null) {
            // Check if no dryer was selected
            if (cookingEvent.dryer1 == null && cookingEvent.dryer2 == null) {
                // Create notification
                val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
                notificationService.create(
                        cookingEvent.cook!!,
                        NotificationType.COOKING_EVENT_FORGOT_DRYERS,
                        "You forgot to set the dryers for ${cookingEvent.date.format(formatter)}",
                )
            }
        }
    }

}