package nl.dirtya.flatsiteapi

import nl.dirtya.flatsiteapi.config.AppProperties
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties(AppProperties::class)
class FlatsiteApiApplication

fun main(args: Array<String>) {
	runApplication<FlatsiteApiApplication>(*args)
}
