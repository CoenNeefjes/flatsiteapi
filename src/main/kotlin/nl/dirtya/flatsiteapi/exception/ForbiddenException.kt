package nl.dirtya.flatsiteapi.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(value = HttpStatus.FORBIDDEN)
class ForbiddenException(message: String) : RuntimeException(message) {
    constructor(): this("Forbidden")
}