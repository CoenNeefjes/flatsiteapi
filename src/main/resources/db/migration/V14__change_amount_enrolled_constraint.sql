-- Use new tables in this migration, since we do not know the name of the constraints we need to drop.
-- We do know for PostgreSql, but not for the H2 in memory database used for testing.

CREATE TABLE cooking_enrolled2 (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    amount INTEGER NOT NULL CONSTRAINT cooking_enrolled_amount_ge_zero_check CHECK(amount >= 0),
    cooking_event_id BIGINT NOT NULL REFERENCES cooking_event(id),
    UNIQUE (user_id, cooking_event_id)
);
INSERT INTO cooking_enrolled2 SELECT * FROM cooking_enrolled;
DROP TABLE cooking_enrolled;
ALTER TABLE cooking_enrolled2 RENAME TO cooking_enrolled;

CREATE TABLE transaction_enrolled2 (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    amount INTEGER NOT NULL CONSTRAINT transaction_enrolled_amount_ge_zero_check CHECK(amount >= 0),
    price numeric(19, 2) NOT NULL,
    transaction_id BIGINT NOT NULL REFERENCES transaction(id),
    UNIQUE (user_id, transaction_id)
);
INSERT INTO transaction_enrolled2 SELECT * FROM transaction_enrolled;
DROP TABLE transaction_enrolled;
ALTER TABLE transaction_enrolled2 RENAME TO transaction_enrolled;