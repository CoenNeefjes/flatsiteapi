create sequence public.hibernate_sequence;

CREATE TABLE user_model (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    username VARCHAR(255) NOT NULL UNIQUE,
    password VARCHAR(255) NOT NULL,
    roles VARCHAR NOT NULL,
    active BOOLEAN NOT NULL,
    image BIGINT
);

-- Add default user with password: password
INSERT INTO user_model(id, uuid, username, password, roles, active, image) VALUES (
    (SELECT nextval('public.hibernate_sequence')),
    '06985ec9-e1f0-497b-8937-685306f19422',
    'Admin',
    '$2a$10$KhAAmYxXS5sYpnXsGX00L.CGVqsSHjcugWht3F44a/e6no7MG5J56',
    'USER,ADMIN',
    TRUE,
    NULL
);

CREATE TABLE transaction (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    date DATE NOT NULL,
    description VARCHAR(255) NOT NULL,
    price numeric(19, 2) NOT NULL,
    owner_id BIGINT NOT NULL REFERENCES user_model(id)
);

CREATE TABLE transaction_enrolled (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    amount INTEGER NOT NULL CHECK(amount > 0),
    price numeric(19, 2) NOT NULL,
    transaction_id BIGINT NOT NULL REFERENCES transaction(id),
    UNIQUE (user_id, transaction_id)
);

CREATE TABLE cooking_event (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    date DATE NOT NULL UNIQUE,
    points_for_cook INTEGER NOT NULL CHECK(points_for_cook > -1),
    cook_id BIGINT REFERENCES user_model(id),
    transaction_id BIGINT REFERENCES transaction(id)
);

CREATE TABLE cooking_enrolled (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    amount INTEGER NOT NULL CHECK(amount > 0),
    cooking_event_id BIGINT NOT NULL REFERENCES cooking_event(id),
    UNIQUE (user_id, cooking_event_id)
);

CREATE TABLE album (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL UNIQUE,
    created_on DATE NOT NULL,
    last_edited DATE NOT NULL,
    editable BOOLEAN NOT NULL
);

INSERT INTO album(id, uuid, name, created_on, last_edited, editable) VALUES (
    (SELECT nextval('public.hibernate_sequence')),
    'eb99291b-b88c-4e01-a835-9aa23c19d8a1',
    'profiel foto''s',
    current_date,
    current_date,
    FALSE
);

INSERT INTO album(id, uuid, name, created_on, last_edited, editable) VALUES (
    (SELECT nextval('public.hibernate_sequence')),
    '9a34c032-87ec-4c2d-86c8-6a9d20ead249',
    'bierlijsten',
    current_date,
    current_date,
    FALSE
);

INSERT INTO album(id, uuid, name, created_on, last_edited, editable) VALUES (
    (SELECT nextval('public.hibernate_sequence')),
    '8abeea71-e4a3-4483-8559-5fc551bfc315',
    'bier inkoop bonnetjes',
    current_date,
    current_date,
    FALSE
);

CREATE TABLE image (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    created_on DATE NOT NULL,
    created_by_id BIGINT NOT NULL REFERENCES user_model(id),
    album_id BIGINT NOT NULL REFERENCES album(id)
);

CREATE TABLE beer_type (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    name VARCHAR(50) NOT NULL UNIQUE,
    beers_per_crate INTEGER NOT NULL CHECK(beers_per_crate > 0)
);

CREATE TABLE beer_input (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    date DATE NOT NULL,
    type VARCHAR(7) NOT NULL,
    note VARCHAR NULL,
    created_by_id BIGINT NOT NULL REFERENCES user_model(id),
    beer_type_id BIGINT NOT NULL REFERENCES beer_type(id),
    image_id BIGINT REFERENCES image(id)
);

CREATE TABLE beer_input_enrolled (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    amount INTEGER NOT NULL CHECK(amount > 0),
    beer_input_id BIGINT NOT NULL REFERENCES beer_input(id)
);

CREATE TABLE vote (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    description VARCHAR(255) NOT NULL,
    created_by_id BIGINT NOT NULL REFERENCES user_model(id),
    created_on DATE NOT NULL
);

CREATE TABLE vote_users_agree (
    vote_id BIGINT NOT NULL REFERENCES vote(id),
    user_model_id BIGINT NOT NULL REFERENCES user_model(id)
);

CREATE TABLE vote_users_disagree (
    vote_id BIGINT NOT NULL REFERENCES vote(id),
    user_model_id BIGINT NOT NULL REFERENCES user_model(id)
);

CREATE TABLE cooking_donation (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    created_on DATE NOT NULL,
    created_by_id BIGINT NOT NULL REFERENCES user_model(id),
    amount INTEGER NOT NULL CHECK(amount > 0),
    recipient_id BIGINT NOT NULL REFERENCES user_model(id)
);
