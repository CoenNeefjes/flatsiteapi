CREATE TABLE notification (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    type VARCHAR(255) NOT NULL,
    message VARCHAR NOT NULL
);