DROP VIEW average_price;

CREATE VIEW average_price AS
    SELECT row_number() over () AS id,
        CAST(ROUND(AVG(t.price / c.points_for_cook::numeric), 2) AS double precision) AS average_price,
        CAST(ROUND(STDDEV_POP(t.price / c.points_for_cook::numeric), 2) AS double precision) AS std_dev,
        COUNT(t.id) AS count,
        u.id AS user_id
    FROM cooking_event c,
         transaction t,
         user_model u
    WHERE c.transaction_id = t.id AND u.id = t.owner_id
    GROUP BY u.username, u.id
    ORDER BY u.id;