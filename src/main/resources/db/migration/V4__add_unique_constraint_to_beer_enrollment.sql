DELETE from beer_input_enrolled;
DELETE from beer_input;
ALTER TABLE beer_input_enrolled ADD CONSTRAINT unique_beer_enrollment UNIQUE (user_id, beer_input_id);