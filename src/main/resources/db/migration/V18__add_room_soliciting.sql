CREATE TABLE room_soliciting_day(
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    date DATE NOT NULL UNIQUE
);

CREATE TABLE room_solicitor(
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    name VARCHAR NOT NULL,
    email VARCHAR NOT NULL,
    student_year INTEGER NOT NULL,
    age INTEGER NOT NULL,
    study VARCHAR NOT NULL,
    remarks VARCHAR NOT NULL
);

CREATE TABLE room_soliciting_appointment(
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    day_id BIGINT NOT NULL REFERENCES room_soliciting_day(id),
    solicitor_id BIGINT NOT NULL REFERENCES room_solicitor(id),
    remarks VARCHAR NOT NULL
);

CREATE TABLE room_soliciting_day_availability(
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    day_id BIGINT NOT NULL REFERENCES room_soliciting_day(id),
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    availability_type VARCHAR NOT NULL,
    remarks VARCHAR NOT NULL
);

CREATE TABLE room_solicitor_review(
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    solicitor_id BIGINT NOT NULL REFERENCES room_solicitor(id),
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    review VARCHAR NOT NULL,
    score INTEGER NOT NULL,
    UNIQUE (solicitor_id, user_id)
);
