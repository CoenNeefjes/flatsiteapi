ALTER TABLE user_model DROP COLUMN auto_enroll;

ALTER TABLE user_model ADD COLUMN monday BOOLEAN;
ALTER TABLE user_model ADD COLUMN tuesday BOOLEAN;
ALTER TABLE user_model ADD COLUMN wednesday BOOLEAN;
ALTER TABLE user_model ADD COLUMN thursday BOOLEAN;
ALTER TABLE user_model ADD COLUMN friday BOOLEAN;
ALTER TABLE user_model ADD COLUMN saturday BOOLEAN;
ALTER TABLE user_model ADD COLUMN sunday BOOLEAN;