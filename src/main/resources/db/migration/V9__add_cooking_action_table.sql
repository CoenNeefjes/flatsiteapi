CREATE TABLE cooking_action (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    cooking_event_id BIGINT NOT NULL REFERENCES cooking_event(id),
    timestamp TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    message VARCHAR NOT NULL
);