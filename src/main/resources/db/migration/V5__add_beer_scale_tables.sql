CREATE TABLE beer_scale_calibration (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    averagereading INTEGER NOT NULL,
    weight decimal(5,2) NOT NULL CHECK(weight >= 0)
);

CREATE TABLE beer_scale_measurement (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    reading INTEGER NOT NULL
);

CREATE TABLE beer_weight (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    weight decimal(11,10) NOT NULL
);

CREATE TABLE beer_scale_regression (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    slope decimal(100,50) NOT NULL,
    intercept decimal(100,50) NOT NULL
);

CREATE TABLE beer_scale_host_name (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    hostname VARCHAR(255) NOT NULL
);