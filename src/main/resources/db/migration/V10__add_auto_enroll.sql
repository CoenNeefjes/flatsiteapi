ALTER TABLE user_model ADD COLUMN auto_enroll BOOLEAN;

UPDATE user_model SET auto_enroll = false;

ALTER TABLE user_model ALTER COLUMN auto_enroll SET NOT NULL;