CREATE VIEW average_price_per_month AS
    SELECT row_number() OVER () AS id,
           date_trunc('month'::text, c.date::timestamp with time zone) AS month,
           round(avg(t.price / c.points_for_cook::numeric), 2)::double precision AS average_price,
           round(stddev_pop(t.price / c.points_for_cook::numeric), 2)::double precision AS std_dev,
           count(t.id) AS count,
           u.id AS user_id
    FROM cooking_event c,
         transaction t,
         user_model u
    WHERE c.transaction_id = t.id AND u.id = t.owner_id
    GROUP BY u.username, u.id, (date_trunc('month'::text, c.date::timestamp with time zone))
    ORDER BY u.id;

