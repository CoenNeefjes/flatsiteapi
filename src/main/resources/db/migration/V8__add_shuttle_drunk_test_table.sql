CREATE TABLE shuttle_drunk_test_result (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    user_id BIGINT NOT NULL REFERENCES user_model(id),
    date DATE NOT NULL,
    score FLOAT NOT NULL
);