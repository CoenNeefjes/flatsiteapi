CREATE TABLE printing_job (
    id BIGINT PRIMARY KEY,
    uuid VARCHAR(36) NOT NULL UNIQUE,
    issuer_id BIGINT NOT NULL REFERENCES user_model(id),
    date DATE NOT NULL,
    file_name VARCHAR(255) NOT NULL
)